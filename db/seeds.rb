# ruby encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

#Seeds for Cities
city1 = City.find_or_create_by(name: "Berlin")
city2 = City.find_or_create_by(name: "Hamburg")

#Seeds for Categories
c1 = Category.find_or_create_by(name: "people")
c2 = Category.find_or_create_by(name: "dating")
c3 = Category.find_or_create_by(name: "course")

i1 = EventInformation.find_or_create_by(title: "smoking")
i2 = EventInformation.find_or_create_by(title: "pets")
i3 = EventInformation.find_or_create_by(title: "sbahn")
i4 = EventInformation.find_or_create_by(title: "parking-places")
i5 = EventInformation.find_or_create_by(title: "wifi")
i6 = EventInformation.find_or_create_by(title: "elevator")

# create drinks
Drink.where(name: 'wine').first_or_create do |drink|
  drink.name = 'wine'
  drink.alcoholic = true
end

Drink.where(name: 'beer').first_or_create do |drink|
  drink.name = 'beer'
  drink.alcoholic = true
end

Drink.where(name: 'spirits').first_or_create do |drink|
  drink.name = 'spirits'
  drink.alcoholic = true
end

Drink.where(name: 'softdrinks').first_or_create do |drink|
  drink.name = 'softdrinks'
  drink.alcoholic = false
end

if Rails.env.development? || Rails.env.staging?
  #Seeds for User
  u1 = User.where(email: 'marcus@wwl.de').first_or_create do |user|
    user.name = 'Marcus'
    user.password = 'password'
    user.city = 'Trebbin'
    user.interests = 'Volleyball, Kochen, Programmieren'
    user.job = 'Developer (Software)'
    user.phone = '015141891232'
    user.relationship = :married
    user.skip_confirmation!
  end
  # unless u1.addresses.any?
  #   u1.addresses.create!({firstname: "Marcus", lastname: "Janke", salutation: "m",addition: "xxx", city: "Trebbin", country: "Germany", street: "Weinberg 9", zip:"14959"})
  #   u1.invoice_address = address
  #   u1.save
  # end
  # unless u1.payment_data.present?
  #   payment_data = u1.build_payment_data({
  #     owner: 'Marcus Janke',
  #     bank: 'Berliner Sparkasse',
  #     iban: 'DE04170550504135140876',
  #     bic: 'WELADED1LOS',
  #   })
  #   u1.payment_data = payment_data
  #   u1.save
  # end

  u2 = User.where(email: 'jp@web.de').first_or_create do |user|
    user.name = 'Jackson'
    user.password = 'password'
    user.city = 'Berlin, Marzahn'
    user.interests = 'Fußball, Kochen, Schlafen'
    user.job = 'Chef'
    user.phone = '015141891232'
    user.relationship = :single
    user.skip_confirmation!
  end
  # unless u2.addresses.any?
  #   address = u2.addresses.create!({firstname: "Jackson", lastname: "Philipps", salutation: "m", city: "Berlin", country: "Germany", street: "Köpenicker Straße 255", zip:"12555"})
  #   u2.invoice_address = address
  #   u2.save
  # end
  # unless u2.payment_data.present?
  #   payment_data = u2.build_payment_data({
  #     owner: 'Marcus Janke',
  #     bank: 'Berliner Sparkasse',
  #     iban: 'DE05170550504135140876',
  #     bic: 'WELADED1BXX',
  #   })
  #   u2.payment_data = payment_data
  #   u2.save
  # end

  u3 = User.where(email: 'user3@web.de').first_or_create do |user|
    user.name = "Martina"
    user.password = "password"
    user.skip_confirmation!
  end
  u4 = User.where(email: 'user4@web.de').first_or_create do |user|
    user.name = "Peter"
    user.password = "password"
    user.skip_confirmation!
  end
  u5 = User.where(email: 'user5@web.de').first_or_create do |user|
    user.name = "Gundula"
    user.password = "password"
    user.skip_confirmation!
  end
  u6 = User.where(email: 'user6@web.de').first_or_create do |user|
    user.name = "Chantalle"
    user.password = "password"
    user.skip_confirmation!
  end
  u7 = User.where(email: 'user7@web.de').first_or_create do |user|
    user.name = "Armin"
    user.password = "password"
    user.skip_confirmation!
  end

  a1 = BackendUser.where(email: 'admin@wonderweblabs.com').first_or_create do |user|
    user.password = 'password'
    user.access_rights << :manage_all
  end

  #Seeds for Events
  e1 = Event.create!({
    sex_direction: 'mf',
    owner: u1,
    title: "Rosenkohlsuppe mit einer crémigen Sauce Bernaise",
    description: "Hmm das wird eine ganz eine leckere Suppe!",
    price: 2170,
    address_city: "Berlin",
    address_zip: "12345",
    address_street: "Köpenicker Landstraße 254b",
    address_district: "Plänterwald",
    address_addition: "im Hinterhof",
    max_guest_count: 8,
    category: c1,
    state: 'approved',
    fake_latitude: 52.12345,
    fake_longitude: 13.123456,
    latitude: 52.5232435,
    longitude: 13.43145,
    drinks: [Drink.first, Drink.last],
  })

  e2 = Event.create!({
    sex_direction: 'mf',
    owner: u1,
    title: "Blumenkohlsuppe mit roter Beete, auf einem Spiegel von Spargelwasser, gedünstet in Maronenbutter",
    description: "Hmm das wird auch eine ganz eine leckere Suppe!",
    price: 2150,
    address_city: "Berlin",
    address_zip: "14959",
    address_street: "Weinberg 6",
    address_addition: "im Hinterhof",
    address_district: "Plänterwald",
    max_guest_count: 7,
    self_participation: true,
    category: c2,
    state: 'approved',
    fake_latitude: 52.12345,
    fake_longitude: 13.123456,
    latitude: 52.5232435,
    longitude: 13.43145,
    drinks: [Drink.first, Drink.last],
  })

  e3 = Event.create!({
    sex_direction: 'mf',
    owner: u2,
    title: "Gulasch",
    description: "Hmm das wird ein toller Gulpopo",
    price: 2150,
    address_city: "Berlin",
    address_zip: "14959",
    address_street: "Weinberg 6",
    address_district: "Plänterwald",
    address_addition: "im Hinterhof",
    max_guest_count: 8,
    category: c3,
    state: 'approved',
    fake_latitude: 52.12345,
    fake_longitude: 13.123456,
    latitude: 52.5232435,
    longitude: 13.43145,
    drinks: [Drink.first, Drink.last],
  })

  e4 = Event.create!({
    sex_direction: 'mf',
    owner: u2,
    title: "Ia-free-cut-the mit Brombeeren und Lauchpésto",
    description: "Hmm das wird ein ganz ein leckeres Eichen!",
    price: 1450,
    address_city: "Berlin",
    address_zip: "12345",
    address_street: "Köpenicker Landstraße 254b",
    address_district: "Plänterwald",
    address_addition: "im Hinterhof",
    max_guest_count: 5,
    category: c1,
    state: 'approved',
    fake_latitude: 52.12345,
    fake_longitude: 13.123456,
    latitude: 52.5232435,
    longitude: 13.43145,
    drinks: [Drink.first, Drink.last],
  })

  e5 = Event.create!({
    sex_direction: 'mf',
    owner: u2,
    title: "Frikatzeé",
    description: "Hmm das wird sicherlich nicht so toll, is das 1. mal dass ich koche!!",
    price: 550,
    address_city: "Berlin",
    address_zip: "12345",
    address_street: "Köpenicker Landstraße 254b",
    address_district: "Treptow-Köpenick",
    address_addition: "im Hinterhof",
    max_guest_count: 7,
    self_participation: true,
    category: c2,
    state: 'approved',
    fake_latitude: 52.12345,
    fake_longitude: 13.123456,
    latitude: 52.5232435,
    longitude: 13.43145,
    drinks: [Drink.first, Drink.last],
  })

  #Seeds for Relation::EventEventInformation
  ei01 = Relation::EventEventInformation.find_or_create_by(event_id: e1.id, event_information_id: i1.id)
  ei02 = Relation::EventEventInformation.find_or_create_by(event_id: e1.id, event_information_id: i3.id)
  ei03 = Relation::EventEventInformation.find_or_create_by(event_id: e1.id, event_information_id: i6.id)
  ei04 = Relation::EventEventInformation.find_or_create_by(event_id: e2.id, event_information_id: i2.id)
  ei05 = Relation::EventEventInformation.find_or_create_by(event_id: e2.id, event_information_id: i4.id)
  ei06 = Relation::EventEventInformation.find_or_create_by(event_id: e2.id, event_information_id: i5.id)

  #Seeds for EventAppointments
  ea01 = EventAppointment.create!({ event: e1, start_time: Time.now+1.hour })
  ea02 = EventAppointment.create!({ event: e2, start_time: Time.now+1.hour })
  ea03 = EventAppointment.create!({ event: e3, start_time: Time.now+1.hour })
  ea04 = EventAppointment.create!({ event: e4, start_time: Time.now+1.hour })
  ea05 = EventAppointment.create!({ event: e5, start_time: Time.now+1.hour })

  ea1 = EventAppointment.create!({ event: e1, start_time: Time.now+1.day })
  ea2 = EventAppointment.create!({ event: e2, start_time: Time.now+1.day })
  ea3 = EventAppointment.create!({ event: e3, start_time: Time.now+1.day })
  ea4 = EventAppointment.create!({ event: e4, start_time: Time.now+1.day })
  ea5 = EventAppointment.create!({ event: e5, start_time: Time.now+1.day })

  ea6 = EventAppointment.create!({ event: e1, start_time: Time.now+2.days })
  ea7 = EventAppointment.create!({ event: e2, start_time: Time.now+2.days })
  ea8 = EventAppointment.create!({ event: e3, start_time: Time.now+2.days })
  ea9 = EventAppointment.create!({ event: e4, start_time: Time.now+2.days })
  ea10 = EventAppointment.create!({ event: e5, start_time: Time.now+2.days })

  ea11 = EventAppointment.create!({ event: e1, start_time: Time.now+3.days })
  ea12 = EventAppointment.create!({ event: e2, start_time: Time.now+3.days })
  ea13 = EventAppointment.create!({ event: e3, start_time: Time.now+3.days })
  ea14 = EventAppointment.create!({ event: e4, start_time: Time.now+3.days })
  ea15 = EventAppointment.create!({ event: e5, start_time: Time.now+3.days })

  # p01 = Participation.create!(user: u3, event_appointment: ea01, male_count: 1, female_count:2, guests_count: 4)
  # p02 = Participation.create!(user: u4, event_appointment: ea01, male_count: 1, female_count:2, guests_count: 4)
  # p03 = Participation.create!(user: u5, event_appointment: ea01, male_count: 1, female_count:2, guests_count: 4)
  # p04 = Participation.create!(user: u6, event_appointment: ea01, male_count: 1, female_count:2, guests_count: 4)
  # p05 = Participation.create!(user: u7, event_appointment: ea01, male_count: 1, female_count:2, guests_count: 4)
  # p06 = Participation.create!(user: u3, event_appointment: ea02, male_count: 1, female_count:2, guests_count: 4)
  # p07 = Participation.create!(user: u4, event_appointment: ea02, male_count: 1, female_count:2, guests_count: 4)
  # p08 = Participation.create!(user: u5, event_appointment: ea02, male_count: 1, female_count:2, guests_count: 4)

  r1 = EventRating.create!(user: u3, event_appointment: ea01, total: 4, meal: 5, location: 3, atmosphere: 4, price_performance_ratio:4, message: "Das war wirklich ein hervorragendes Essen, vielen Dank dafür!")
  r2 = EventRating.create!(user: u4, event_appointment: ea01, total: 4, meal: 3, location: 3, atmosphere: 4, price_performance_ratio:4, message: "Das war ganz gut! Weiter so... ")
  r3 = EventRating.create!(user: u5, event_appointment: ea01, total: 5, meal: 5, location: 5, atmosphere: 5, price_performance_ratio:4, message: "Ich bin begeistert, bis zum nächsten Mal mein Freund!")
  r4 = EventRating.create!(user: u3, event_appointment: ea02, total: 3, meal: 4, location: 3, atmosphere: 3, price_performance_ratio:3, message: "Also insgesamt ganz OK.")
  r5 = EventRating.create!(user: u4, event_appointment: ea02, total: 1, meal: 1, location: 2, atmosphere: 2, price_performance_ratio:1, message: "Nein danke, nie wieder! Pfuiii")

  be1 = BlogEntry.where(name: 'Erster Blogeintrag').first_or_create do |blog_entry|
    blog_entry.alias = 'erster-blogeintrag'
    blog_entry.date = Time.now-3.months
    blog_entry.teaser_text = 'Das ist unser erster Blogeintrag. Wir sind sehr stolz unsere neue Plattform <<foodoo>> vorzustellen!'
  end

  be2 = BlogEntry.where(name: 'Zweiter Blogeintrag').first_or_create do |blog_entry|
    blog_entry.alias = 'zweiter-blogeintrag'
    blog_entry.date = Time.now-60.days
    blog_entry.teaser_text = 'Das ist unser zweiter Blogeintrag. Wir sind sehr stolz unsere neue Plattform <<foodoo>> vorzustellen!'
  end

  be3 = BlogEntry.where(name: 'Dritter Blogeintrag').first_or_create do |blog_entry|
    blog_entry.alias = 'dritter-blogeintrag'
    blog_entry.date = Time.now-40.days
    blog_entry.teaser_text = 'Das ist unser dritter Blogeintrag. Wir sind sehr stolz unsere neue Plattform <<foodoo>> vorzustellen!'
  end

  be4 = BlogEntry.where(name: 'Vierter Blogeintrag').first_or_create do |blog_entry|
    blog_entry.alias = 'vierter-blogeintrag'
    blog_entry.date = Time.now-10.days
    blog_entry.teaser_text = 'Das ist unser vierter Blogeintrag. Wir sind sehr stolz unsere neue Plattform <<foodoo>> vorzustellen!'
  end

  be5 = BlogEntry.where(name: 'Fünfter Blogeintrag').first_or_create do |blog_entry|
    blog_entry.alias = 'fuenfter-blogeintrag'
    blog_entry.date = Time.now-1.day
    blog_entry.teaser_text = 'Das ist unser fünfter Blogeintrag. Wir sind sehr stolz unsere neue Plattform <<foodoo>> vorzustellen!'
  end

  be6 = BlogEntry.where(name: 'Sechster Blogeintrag').first_or_create do |blog_entry|
    blog_entry.alias = 'sechster-blogeintrag'
    blog_entry.date = Time.now
    blog_entry.teaser_text = 'Das ist unser sechster Blogeintrag. Wir sind sehr stolz unsere neue Plattform <<foodoo>> vorzustellen!'
  end
end
