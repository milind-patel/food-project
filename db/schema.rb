# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140715131243) do

  create_table "account_balances", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "account_balances", ["user_id"], name: "index_account_balances_on_user_id", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.integer  "user_id",             limit: 4
    t.string   "city",                limit: 255
    t.string   "country",             limit: 255
    t.string   "zip",                 limit: 255
    t.string   "salutation",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "encrypted_addition",  limit: 255
    t.string   "encrypted_street",    limit: 255
    t.string   "encrypted_company",   limit: 255
    t.string   "encrypted_firstname", limit: 255
    t.string   "encrypted_lastname",  limit: 255
  end

  create_table "backend_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "password_salt",          limit: 255
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "failed_attempts",        limit: 4,   default: 0,  null: false
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "access_rights",          limit: 4
  end

  add_index "backend_users", ["email"], name: "index_backend_users_on_email", unique: true, using: :btree
  add_index "backend_users", ["reset_password_token"], name: "index_backend_users_on_reset_password_token", unique: true, using: :btree
  add_index "backend_users", ["unlock_token"], name: "index_backend_users_on_unlock_token", unique: true, using: :btree

  create_table "blog_elements", force: :cascade do |t|
    t.string   "headline",           limit: 255
    t.text     "text",               limit: 65535
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "type",               limit: 255
    t.integer  "position",           limit: 4
    t.integer  "blog_entry_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "blog_entries", force: :cascade do |t|
    t.string   "name",                      limit: 255
    t.text     "teaser_text",               limit: 65535
    t.string   "teaser_image_file_name",    limit: 255
    t.string   "teaser_image_content_type", limit: 255
    t.integer  "teaser_image_file_size",    limit: 4
    t.datetime "teaser_image_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "alias",                     limit: 255
    t.date     "date"
    t.boolean  "published",                 limit: 1
  end

  add_index "blog_entries", ["alias", "date"], name: "index_blog_entries_on_alias_and_date", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.integer  "kind",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",       limit: 255, null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "csvs", force: :cascade do |t|
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.string   "kind",              limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "drinks", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.boolean  "alcoholic",  limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_appointments", force: :cascade do |t|
    t.datetime "start_time"
    t.integer  "event_id",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.integer  "notified_for_rating", limit: 4,   default: 0
    t.string   "state",               limit: 255
  end

  add_index "event_appointments", ["event_id"], name: "index_event_appointments_on_event_id", using: :btree

  create_table "event_informations", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_ratings", force: :cascade do |t|
    t.integer  "user_id",                 limit: 4
    t.integer  "event_appointment_id",    limit: 4
    t.integer  "total",                   limit: 4
    t.integer  "meal",                    limit: 4
    t.integer  "location",                limit: 4
    t.integer  "price_performance_ratio", limit: 4
    t.integer  "atmosphere",              limit: 4
    t.text     "message",                 limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "event_ratings", ["event_appointment_id"], name: "index_event_ratings_on_event_appointment_id", using: :btree
  add_index "event_ratings", ["user_id"], name: "index_event_ratings_on_user_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.integer  "owner_id",                  limit: 4
    t.string   "title",                     limit: 255
    t.text     "description",               limit: 65535
    t.integer  "courses_count",             limit: 4
    t.integer  "max_f_guest_count",         limit: 4
    t.integer  "max_guest_count",           limit: 4
    t.integer  "max_m_guest_count",         limit: 4
    t.integer  "min_m_guest_count",         limit: 4
    t.integer  "min_guest_count",           limit: 4
    t.integer  "min_f_guest_count",         limit: 4
    t.integer  "category_id",               limit: 4
    t.integer  "duration",                  limit: 4
    t.integer  "repetition",                limit: 4
    t.string   "address_city",              limit: 255
    t.string   "address_zip",               limit: 255
    t.string   "address_street",            limit: 255
    t.string   "address_addition",          limit: 255
    t.string   "ring_at",                   limit: 255
    t.string   "floor",                     limit: 255
    t.string   "state",                     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.integer  "price",                     limit: 4,                               default: 0
    t.decimal  "latitude",                                precision: 15, scale: 10
    t.decimal  "longitude",                               precision: 15, scale: 10
    t.decimal  "fake_latitude",                           precision: 15, scale: 10
    t.decimal  "fake_longitude",                          precision: 15, scale: 10
    t.string   "sex_direction",             limit: 255
    t.boolean  "self_participation",        limit: 1
    t.boolean  "agb_confirmation",          limit: 1,                               default: false
    t.boolean  "debit_authorization",       limit: 1,                               default: false
    t.string   "person_name",               limit: 255
    t.string   "phone",                     limit: 255
    t.string   "address_district",          limit: 255
    t.boolean  "open_end",                  limit: 1
    t.float    "participation_until_hours", limit: 24,                              default: 0.0
  end

  add_index "events", ["category_id"], name: "index_events_on_category_id", using: :btree
  add_index "events", ["owner_id"], name: "index_events_on_owner_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.integer  "imaged_id",         limit: 4
    t.string   "imaged_type",       limit: 255
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.integer  "amount",            limit: 4,   default: 0
    t.string   "number",            limit: 255
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.datetime "date"
    t.string   "firstname",         limit: 255
    t.string   "lastname",          limit: 255
    t.string   "salutation",        limit: 255
    t.string   "email",             limit: 255
    t.string   "company",           limit: 255
    t.string   "city",              limit: 255
    t.string   "country",           limit: 255
    t.string   "street",            limit: 255
    t.string   "addition",          limit: 255
    t.string   "zip",               limit: 255
    t.boolean  "generated",         limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.boolean  "payed",             limit: 1,   default: false
    t.string   "access_key",        limit: 255
    t.integer  "before_tax_amount", limit: 4
    t.boolean  "exported",          limit: 1,   default: false
  end

  create_table "languages", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "iso_639_1",  limit: 255
    t.string   "iso_639_2",  limit: 255
    t.string   "iso_639_3",  limit: 255
  end

  add_index "languages", ["iso_639_1"], name: "index_languages_on_iso_639_1", using: :btree
  add_index "languages", ["iso_639_2"], name: "index_languages_on_iso_639_2", using: :btree
  add_index "languages", ["iso_639_3"], name: "index_languages_on_iso_639_3", using: :btree

  create_table "newsletter_logs", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "newsletter_subscriptions", force: :cascade do |t|
    t.integer  "city_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.string   "email",      limit: 255,                null: false
    t.string   "name",       limit: 255
    t.boolean  "active",     limit: 1,   default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "newsletter_subscriptions", ["active"], name: "index_newsletter_subscriptions_on_active", using: :btree
  add_index "newsletter_subscriptions", ["email"], name: "index_newsletter_subscriptions_on_email", using: :btree
  add_index "newsletter_subscriptions", ["updated_at"], name: "index_newsletter_subscriptions_on_updated_at", using: :btree

  create_table "participations", force: :cascade do |t|
    t.integer  "user_id",              limit: 4
    t.integer  "event_appointment_id", limit: 4
    t.string   "state",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.integer  "male_count",           limit: 4
    t.integer  "female_count",         limit: 4
    t.integer  "guests_count",         limit: 4
  end

  add_index "participations", ["event_appointment_id"], name: "index_participations_on_event_appointment_id", using: :btree
  add_index "participations", ["user_id"], name: "index_participations_on_user_id", using: :btree

  create_table "payment_data", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "encrypted_iban",  limit: 255
    t.string   "encrypted_bic",   limit: 255
    t.string   "encrypted_bank",  limit: 255
    t.string   "encrypted_owner", limit: 255
  end

  add_index "payment_data", ["user_id"], name: "index_payment_data_on_user_id", using: :btree

  create_table "relation_event_drinks", force: :cascade do |t|
    t.integer  "event_id",   limit: 4
    t.integer  "drink_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "relation_event_event_informations", force: :cascade do |t|
    t.integer  "event_id",             limit: 4
    t.integer  "event_information_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "relation_user_languages", force: :cascade do |t|
    t.integer  "strength",    limit: 4
    t.integer  "user_id",     limit: 4
    t.integer  "language_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "relation_user_languages", ["language_id"], name: "index_relation_user_languages_on_language_id", using: :btree
  add_index "relation_user_languages", ["user_id"], name: "index_relation_user_languages_on_user_id", using: :btree

  create_table "transaction_bases", force: :cascade do |t|
    t.text     "comment",              limit: 65535
    t.integer  "value",                limit: 4
    t.integer  "account_balance_id",   limit: 4
    t.integer  "event_appointment_id", limit: 4
    t.string   "type",                 limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "invoice_id",           limit: 4
    t.datetime "deleted_at"
  end

  add_index "transaction_bases", ["account_balance_id"], name: "index_transaction_bases_on_account_balance_id", using: :btree
  add_index "transaction_bases", ["event_appointment_id"], name: "index_transaction_bases_on_event_appointment_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                                     limit: 255,                            default: "",   null: false
    t.string   "encrypted_password",                        limit: 255,                            default: "",   null: false
    t.string   "password_salt",                             limit: 255
    t.string   "reset_password_token",                      limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                             limit: 4,                              default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",                        limit: 255
    t.string   "last_sign_in_ip",                           limit: 255
    t.string   "confirmation_token",                        limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",                         limit: 255
    t.integer  "failed_attempts",                           limit: 4,                              default: 0,    null: false
    t.string   "unlock_token",                              limit: 255
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "date_of_birth"
    t.string   "job",                                       limit: 255
    t.string   "phone",                                     limit: 255
    t.string   "relationship",                              limit: 255
    t.text     "interests",                                 limit: 65535
    t.text     "description",                               limit: 65535
    t.integer  "invoice_address_id",                        limit: 4
    t.datetime "deleted_at"
    t.string   "fb_uid",                                    limit: 255
    t.integer  "main_image_id",                             limit: 4
    t.string   "gender",                                    limit: 255
    t.string   "name",                                      limit: 255
    t.string   "city",                                      limit: 255
    t.decimal  "aggregated_rating_total",                                 precision: 10, scale: 8, default: 0.0
    t.decimal  "aggregated_rating_meal",                                  precision: 10, scale: 8, default: 0.0
    t.decimal  "aggregated_rating_atmosphere",                            precision: 10, scale: 8, default: 0.0
    t.decimal  "aggregated_rating_location",                              precision: 10, scale: 8, default: 0.0
    t.decimal  "aggregated_rating_price_performance_ratio",               precision: 10, scale: 8, default: 0.0
    t.integer  "aggregated_ratings_count",                  limit: 4,                              default: 0
    t.text     "facebook_oauth_data",                       limit: 65535
    t.boolean  "newsletter_subscription",                   limit: 1,                              default: true
    t.integer  "newsletter_city_id",                        limit: 4
    t.string   "encrypted_phone",                           limit: 255
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["fb_uid"], name: "index_users_on_fb_uid", using: :btree
  add_index "users", ["main_image_id"], name: "index_users_on_main_image_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
