class AddLastPaticipationTimeFieldToEvent < ActiveRecord::Migration
  def change
    add_column :events, :participation_until, :float
  end
end
