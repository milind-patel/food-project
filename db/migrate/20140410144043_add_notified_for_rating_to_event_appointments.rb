class AddNotifiedForRatingToEventAppointments < ActiveRecord::Migration
  def change
    add_column :event_appointments, :notified_for_rating, :integer, default: 0
  end
end
