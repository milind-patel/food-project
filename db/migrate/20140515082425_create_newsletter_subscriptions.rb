class CreateNewsletterSubscriptions < ActiveRecord::Migration
  def change
    create_table :newsletter_subscriptions do |t|
      t.references :city
      t.references :user

      t.string :email, null: false
      t.string :name
      t.boolean :active, default: true
      t.timestamps
    end
    add_index :newsletter_subscriptions, :email
    add_index :newsletter_subscriptions, :active
    add_index :newsletter_subscriptions, :updated_at
  end
end
