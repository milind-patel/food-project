class RemoveUnencryptedFieldsFromAddressess < ActiveRecord::Migration
  def up
    migrate_address_data
    remove_column :addresses, :firstname
    remove_column :addresses, :lastname
    remove_column :addresses, :company
    remove_column :addresses, :addition
    remove_column :addresses, :street
  end

  def down
    add_column :addresses, :company, :string
    add_column :addresses, :firstname, :string
    add_column :addresses, :lastname, :string
    add_column :addresses, :addition, :string
    add_column :addresses, :street, :string
    migrate_address_data_back
  end


  def migrate_address_data
    Address.all.each do |address|
      address.addition  = address.unsafe_addition
      address.street    = address.unsafe_street
      address.company   = address.unsafe_company
      address.firstname = address.unsafe_firstname
      address.lastname  = address.unsafe_lastname
      address.save
    end
  end

  def migrate_address_data_back
    Address.all.each do |address|
      address.unsafe_addition = address.addition
      address.unsafe_street = address.street
      address.unsafe_company = address.company
      address.unsafe_firstname = address.firstname
      address.unsafe_lastname = address.lastname
      address.save
    end
  end
end