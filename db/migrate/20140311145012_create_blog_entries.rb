class CreateBlogEntries < ActiveRecord::Migration
  def change
    create_table :blog_entries do |t|
      # main attributes
      t.string :name
      t.text :teaser_text
      t.attachment :teaser_image

      # timestamps
      t.timestamps
      t.datetime :deleted_at
    end
  end
end
