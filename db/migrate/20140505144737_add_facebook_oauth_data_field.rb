class AddFacebookOauthDataField < ActiveRecord::Migration
  def change
    add_column :users, :facebook_oauth_data, :text
  end
end
