class CreateRelationEventEventInformations < ActiveRecord::Migration
  def change
    create_table :relation_event_event_informations do |t|
      #main data
      t.references :event
      t.references :event_information

      #timestamps
      t.timestamps
    end
  end
end
