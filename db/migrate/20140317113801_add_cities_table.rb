class AddCitiesTable < ActiveRecord::Migration

  def change
    create_table :cities do |t|
      t.string :name

      #timestamps
      t.datetime :deleted_at
      t.timestamps
    end
  end

end
