class AddPeopleCountToParticipations < ActiveRecord::Migration
  def change
    add_column :participations, :people_count, :integer
  end
end
