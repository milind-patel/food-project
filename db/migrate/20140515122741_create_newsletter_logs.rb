class CreateNewsletterLogs < ActiveRecord::Migration
  def change
    create_table :newsletter_logs do |t|
      t.timestamps
    end
  end
end
