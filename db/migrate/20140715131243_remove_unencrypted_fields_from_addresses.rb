class RemoveUnencryptedFieldsFromAddresses < ActiveRecord::Migration
  def up
    migrate_payment_data
    remove_column :payment_data, :iban
    remove_column :payment_data, :bic
    remove_column :payment_data, :bank
    remove_column :payment_data, :owner
  end

  def down
    add_column :payment_data, :iban, :string
    add_column :payment_data, :bic, :string
    add_column :payment_data, :bank, :string
    add_column :payment_data, :owner, :string
    migrate_payment_data_back
  end

  def migrate_payment_data
    PaymentData.all.each do |pd|
      pd.iban   = pd.unsafe_iban
      pd.bic    = pd.unsafe_bic
      pd.bank   = pd.unsafe_bank
      pd.owner  = pd.unsafe_owner
      pd.save
    end
  end

  def migrate_payment_data_back
    PaymentData.all.each do |pd|
      pd.unsafe_iban   = pd.iban
      pd.unsafe_bic    = pd.bic
      pd.unsafe_bank   = pd.bank
      pd.unsafe_owner  = pd.owner
      pd.save
    end
  end
end