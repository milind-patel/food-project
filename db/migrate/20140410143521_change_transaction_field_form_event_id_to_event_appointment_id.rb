class ChangeTransactionFieldFormEventIdToEventAppointmentId < ActiveRecord::Migration
  def change
    rename_column :transaction_bases, :event_id, :event_appointment_id
  end
end
