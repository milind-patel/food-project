class AddMaleCountAndFemaleCountToParticipations < ActiveRecord::Migration
  def change
    add_column :participations, :male_count, :integer
    add_column :participations, :female_count, :integer
    remove_column :participations, :people_count
  end
end
