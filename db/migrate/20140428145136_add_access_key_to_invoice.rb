class AddAccessKeyToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :access_key, :string
  end
end
