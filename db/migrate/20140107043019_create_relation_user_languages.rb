class CreateRelationUserLanguages < ActiveRecord::Migration
  def change
    create_table :relation_user_languages do |t|
      t.integer :strength
      t.references :user, index: true
      t.references :language, index: true

      t.timestamps
    end
  end
end
