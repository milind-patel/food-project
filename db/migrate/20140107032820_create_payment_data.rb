class CreatePaymentData < ActiveRecord::Migration
  def change
    create_table :payment_data do |t|
      t.string :iban
      t.string :bic
      t.string :bank
      t.string :owner
      t.references :user, index: true

      t.timestamps
    end
  end
end
