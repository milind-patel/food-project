class FixMainImageColumnForUsers < ActiveRecord::Migration
  def up
    remove_column :users, :main_image

    add_column :users, :main_image_id, :integer
    add_index :users, :main_image_id
  end

  def down
    remove_index :users, :main_image_id
    remove_column :users, :main_image_id

    add_column :users, :main_image, :integer
  end
end
