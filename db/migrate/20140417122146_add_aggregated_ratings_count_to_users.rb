class AddAggregatedRatingsCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :aggregated_ratings_count, :integer, default: 0
  end
end
