class AddEncryptedFieldsToPaymentData < ActiveRecord::Migration

  def up
    add_column :payment_data, :encrypted_iban, :string
    add_column :payment_data, :encrypted_bic, :string
    add_column :payment_data, :encrypted_bank, :string
    add_column :payment_data, :encrypted_owner, :string
    migrate_payment_data
  end

  def down
    remove_column :payment_data, :encrypted_iban
    remove_column :payment_data, :encrypted_bic
    remove_column :payment_data, :encrypted_bank
    remove_column :payment_data, :encrypted_owner
  end

  def migrate_payment_data
    PaymentData.all.each do |pd|
      pd.iban_e = pd.iban
      pd.bic_e = pd.bic
      pd.bank_e = pd.bank
      pd.owner_e = pd.owner
      pd.save
    end
  end

end
