class AddAggregatedRatingsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :aggregated_rating_total, :decimal, precision: 10, scale: 8, default: 0.0
    add_column :users, :aggregated_rating_meal, :decimal, precision: 10, scale: 8, default: 0.0
    add_column :users, :aggregated_rating_atmosphere, :decimal, precision: 10, scale: 8, default: 0.0
    add_column :users, :aggregated_rating_location, :decimal, precision: 10, scale: 8, default: 0.0
    add_column :users, :aggregated_rating_price_performance_ratio, :decimal, precision: 10, scale: 8, default: 0.0
  end
end
