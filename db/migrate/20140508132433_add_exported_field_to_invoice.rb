class AddExportedFieldToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :exported, :boolean, default: false
  end
end
