class CreateParticipations < ActiveRecord::Migration
  def change
    create_table :participations do |t|
      #main data
      t.references :user, index: true
      t.references :event_appointment, index: true

      #state machine
      t.string :state

      #timestamps
      t.timestamps
    end
  end
end
