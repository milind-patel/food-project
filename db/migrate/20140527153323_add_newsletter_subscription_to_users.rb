class AddNewsletterSubscriptionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :newsletter_subscription, :boolean, default: true
  end
end
