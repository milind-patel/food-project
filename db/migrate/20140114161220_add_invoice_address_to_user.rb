class AddInvoiceAddressToUser < ActiveRecord::Migration
  def up
    add_column :users, :invoice_address_id, :integer, index: true
  end

  def down
    remove_column :users, :invoice_address_id
  end
end
