class ChangeTransactionsTable < ActiveRecord::Migration
  def change
    rename_table 'transactions', 'transaction_bases'
  end
end
