class AddDeletedAtFieldToParticipations < ActiveRecord::Migration
  def up
    add_column :participations, :deleted_at, :datetime
  end

  def down
    remove_column :participations, :deleted_at, :datetime
  end
end
