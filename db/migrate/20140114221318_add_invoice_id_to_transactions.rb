class AddInvoiceIdToTransactions < ActiveRecord::Migration
  def up
    add_column :transactions, :invoice_id, :integer, index: true
  end

  def down
    remove_column :transactions, :invoice_id
  end
end
