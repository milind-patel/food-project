class AddPersonNameFieldToEvents < ActiveRecord::Migration
  def change
    add_column :events, :person_name, :string
  end
end
