class CreateEventInformations < ActiveRecord::Migration
  def change
    create_table :event_informations do |t|
      #main data
      t.string :title

      #timestamps
      t.timestamps
    end
  end
end
