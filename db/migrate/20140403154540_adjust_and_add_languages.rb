class AdjustAndAddLanguages < ActiveRecord::Migration
  def up
    add_column :languages, :iso_639_1, :string
    add_column :languages, :iso_639_2, :string
    add_column :languages, :iso_639_3, :string
    add_index :languages, :iso_639_1
    add_index :languages, :iso_639_2
    add_index :languages, :iso_639_3
    load_and_create_languages
  end

  def down
    remove_index :languages, :iso_639_1
    remove_index :languages, :iso_639_2
    remove_index :languages, :iso_639_3
    remove_column :languages, :iso_639_1
    remove_column :languages, :iso_639_2
    remove_column :languages, :iso_639_3
  end


  # --------------------------------------

  def load_and_create_languages
    Language.delete_all

    file_path = Rails.root.join('lib', 'iso_languages.csv')
    if File.exists?(file_path)
      languages = []

      f = File.new(file_path)
      f.each_line do |content|
        lang = create_value_hash_from_line(content)
        if lang
          languages << lang
          puts "Language added: #{lang[:iso_639_1]} - #{lang[:name]}"
        end
      end

      ActiveRecord::Base.transaction do
        Language.create(languages)
      end
    else
      raise "languages loading file in lib/iso_languages.csv not found."
    end
  end

  def create_value_hash_from_line(line)
    split = line.split(/[|]/)
    unless split[2].blank?
      {
        :iso_639_1 => split[2],
        :iso_639_2 => split[0],
        :iso_639_3 => split[1],
        :title     => split[3]
      }
    end
  end

end
