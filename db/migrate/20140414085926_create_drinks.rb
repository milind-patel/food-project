class CreateDrinks < ActiveRecord::Migration
  def change
    create_table :drinks do |t|
      t.string :name
      t.boolean :alcoholic

      t.timestamps
    end
  end
end
