class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :user
      t.integer :amount
      t.string :number
      t.attachment :file
      t.datetime :date

      t.string :firstname
      t.string :lastname
      t.string :salutation
      t.string :email
      t.string :company
      t.string :city
      t.string :country
      t.string :street
      t.string :addition
      t.string :zip

      t.boolean :generated

      t.timestamps
    end
  end
end
