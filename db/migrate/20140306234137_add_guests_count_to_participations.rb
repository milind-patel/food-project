class AddGuestsCountToParticipations < ActiveRecord::Migration
  def change
    add_column :participations, :guests_count, :integer
  end
end
