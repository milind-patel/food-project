class ChangePriceToIntegerForEvents < ActiveRecord::Migration
  def up
    remove_column :events, :price
    add_column :events, :price, :integer, default: 0
  end

  def down
    remove_column :events, :price
    add_column :events, :price, :float
  end
end
