class CreateEventRatings < ActiveRecord::Migration
  def change
    create_table :event_ratings do |t|
      #foreign keys
      t.references :user, index: true
      t.references :event_appointment, index: true

      #main data
      t.integer :total
      t.integer :meal
      t.integer :location
      t.integer :price_performance_ratio
      t.integer :atmosphere

      t.text :message

      #timestamps
      t.timestamps

      #acts as paranoid
      t.datetime :deleted_at
    end
  end
end
