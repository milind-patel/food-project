class AddStateFieldToEventAppointments < ActiveRecord::Migration
  def change
    add_column :event_appointments, :state, :string
  end
end
