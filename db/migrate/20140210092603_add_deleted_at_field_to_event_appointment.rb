class AddDeletedAtFieldToEventAppointment < ActiveRecord::Migration
  def up
    add_column :event_appointments, :deleted_at, :datetime
  end

  def down

    remove_column :event_appointments, :deleted_at
  end
end
