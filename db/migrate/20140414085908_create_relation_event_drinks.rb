class CreateRelationEventDrinks < ActiveRecord::Migration
  def change
    create_table :relation_event_drinks do |t|
      t.references :event
      t.references :drink

      t.timestamps
    end
  end
end
