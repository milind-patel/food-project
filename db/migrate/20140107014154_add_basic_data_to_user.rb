class AddBasicDataToUser < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.date :date_of_birth
      t.string :firstname
      t.string :job
      t.string :lastname
      t.string :phone
      t.string :relationship
      t.text :interests
      t.text :description
    end
  end
end
