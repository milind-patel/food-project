class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.text :comment
      t.integer :value

      t.references :account_balance, index: true
      t.references :event, index: true

      t.string :type

      t.timestamps
    end
  end
end
