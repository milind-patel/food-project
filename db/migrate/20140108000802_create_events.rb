class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      # owner user
      t.references :owner, index: true

      #main data
      t.string :title
      t.text :description
      t.integer :courses_count
      t.integer :max_f_guest_count
      t.integer :max_guest_count
      t.integer :max_m_guest_count
      t.integer :min_m_guest_count
      t.integer :min_guest_count
      t.integer :min_f_guest_count
      t.float :price
      t.references :category, index: true

      #time attributes
      t.integer :duration
      t.datetime :start_time
      t.integer :repetition

      #address
      t.string :address_city
      t.string :address_zip
      t.string :address_street
      t.string :address_addition
      t.string :ring_at
      t.string :floor

      #state machine
      t.string :state

      t.timestamps
    end
  end
end
