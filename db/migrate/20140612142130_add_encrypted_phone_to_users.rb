class AddEncryptedPhoneToUsers < ActiveRecord::Migration
  def up
    add_column :users, :encrypted_phone, :string
    self.migrate_user_phone_numbers
  end

  def down
    remove_column :users, :encrypted_phone
  end

  def migrate_user_phone_numbers
    User.all.each do |user|
      user.phone_e = user.phone
      user.save
    end
  end
end
