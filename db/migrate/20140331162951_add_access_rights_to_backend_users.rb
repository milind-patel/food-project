class AddAccessRightsToBackendUsers < ActiveRecord::Migration
  def change
    add_column :backend_users, :access_rights, :integer
  end
end
