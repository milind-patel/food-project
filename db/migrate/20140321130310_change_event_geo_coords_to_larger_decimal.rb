class ChangeEventGeoCoordsToLargerDecimal < ActiveRecord::Migration
  def up
    change_column :events, :latitude, :decimal, precision: 15, scale: 10
    change_column :events, :longitude, :decimal, precision: 15, scale: 10
    change_column :events, :fake_latitude, :decimal, precision: 15, scale: 10
    change_column :events, :fake_longitude, :decimal, precision: 15, scale: 10
  end

  def down
    change_column :events, :latitude, :float
    change_column :events, :longitude, :float
    change_column :events, :fake_latitude, :float
    change_column :events, :fake_longitude, :float
  end
end