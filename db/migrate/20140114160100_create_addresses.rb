class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :user
      t.string :addition
      t.string :city
      t.string :country
      t.string :street
      t.string :zip

      t.string :company
      t.string :firstname
      t.string :lastname
      t.string :salutation

      t.timestamps
    end
  end
end
