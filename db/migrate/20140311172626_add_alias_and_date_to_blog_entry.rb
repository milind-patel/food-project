class AddAliasAndDateToBlogEntry < ActiveRecord::Migration
  def up
    add_column :blog_entries, :alias, :string
    add_column :blog_entries, :date, :date
    add_column :blog_entries, :published, :boolean
    add_index :blog_entries, [:alias, :date], unique: true
  end

  def down
    remove_index :blog_entries, [:alias, :date]
    remove_column :blog_entries, :alias
    remove_column :blog_entries, :date
    remove_column :blog_entries, :published
  end
end
