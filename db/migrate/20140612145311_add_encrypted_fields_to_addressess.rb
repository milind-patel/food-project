class AddEncryptedFieldsToAddressess < ActiveRecord::Migration

  def up
    add_column :addresses, :encrypted_addition, :string
    add_column :addresses, :encrypted_street, :string
    add_column :addresses, :encrypted_company, :string
    add_column :addresses, :encrypted_firstname, :string
    add_column :addresses, :encrypted_lastname, :string
    migrate_address_data
  end

  def down
    remove_column :addresses, :encrypted_addition
    remove_column :addresses, :encrypted_street
    remove_column :addresses, :encrypted_company
    remove_column :addresses, :encrypted_firstname
    remove_column :addresses, :encrypted_lastname
  end

  def migrate_address_data
    Address.all.each do |address|
      address.addition_e = address.addition
      address.street_e = address.street
      address.company_e = address.company
      address.firstname_e = address.firstname
      address.lastname_e = address.lastname
      address.save
    end
  end

end
