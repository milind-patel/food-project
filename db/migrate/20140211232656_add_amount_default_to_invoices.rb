class AddAmountDefaultToInvoices < ActiveRecord::Migration
  def change
    change_column :invoices, :amount, :integer, default: 0
  end
end
