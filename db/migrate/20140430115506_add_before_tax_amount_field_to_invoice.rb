class AddBeforeTaxAmountFieldToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :before_tax_amount, :integer
  end
end
