class CreateEventAppointments < ActiveRecord::Migration
  def change
    create_table :event_appointments do |t|
      #main data
      t.datetime :start_time

      #references
      t.references :event, index: true

      #timestamps
      t.timestamps
    end
  end
end
