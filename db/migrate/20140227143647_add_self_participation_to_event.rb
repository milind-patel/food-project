class AddSelfParticipationToEvent < ActiveRecord::Migration
  def change
    add_column :events, :self_participation, :boolean
  end
end
