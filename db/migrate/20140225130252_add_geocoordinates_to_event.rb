class AddGeocoordinatesToEvent < ActiveRecord::Migration
  def up
    add_column :events, :latitude, :float
    add_column :events, :longitude, :float
    add_column :events, :fake_latitude, :float
    add_column :events, :fake_longitude, :float
  end

  def down
    remove_column :events, :latitude
    remove_column :events, :longitude
    remove_column :events, :fake_latitude
    remove_column :events, :fake_longitude
  end
end
