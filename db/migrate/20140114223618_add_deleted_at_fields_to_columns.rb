class AddDeletedAtFieldsToColumns < ActiveRecord::Migration
  def up
    add_column :users, :deleted_at, :datetime
    add_column :payment_data, :deleted_at, :datetime
    add_column :addresses, :deleted_at, :datetime
    add_column :account_balances, :deleted_at, :datetime
    add_column :transactions, :deleted_at, :datetime
    add_column :invoices, :deleted_at, :datetime
    add_column :events, :deleted_at, :datetime
  end

  def down
    remove_column :users, :deleted_at
    remove_column :payment_data, :deleted_at
    remove_column :addresses, :deleted_at
    remove_column :account_balances, :deleted_at
    remove_column :transactions, :deleted_at
    remove_column :invoices, :deleted_at
    remove_column :events, :deleted_at
  end
end
