class CreateAccountBalances < ActiveRecord::Migration
  def change
    create_table :account_balances do |t|
      t.references :user, index: true

      t.timestamps
    end
  end
end
