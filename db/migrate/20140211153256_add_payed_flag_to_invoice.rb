class AddPayedFlagToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :payed, :boolean, default: false
  end
end
