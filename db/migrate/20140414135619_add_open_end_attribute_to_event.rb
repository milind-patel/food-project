class AddOpenEndAttributeToEvent < ActiveRecord::Migration
  def change
    add_column :events, :open_end, :boolean
  end
end
