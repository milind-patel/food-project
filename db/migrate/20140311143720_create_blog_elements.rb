class CreateBlogElements < ActiveRecord::Migration
  def change
    create_table :blog_elements do |t|
      # main attributes
      t.string :headline
      t.text :text
      t.attachment :image
      t.string :type
      t.integer :position

      # references
      t.references :blog_entry

      # timestamps
      t.timestamps
      t.datetime :deleted_at
    end
  end
end
