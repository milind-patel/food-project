class AddNameAndRemoveTitleFromCategories < ActiveRecord::Migration
  def up
    remove_column :categories, :title
    add_column :categories, :name, :string, null: false
  end

  def down
    remove_column :categories, :name
    add_column :categories, :title, :string
  end
end
