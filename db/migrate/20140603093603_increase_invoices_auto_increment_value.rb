class IncreaseInvoicesAutoIncrementValue < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE invoices AUTO_INCREMENT = 1359;
    SQL
  end
end
