class CreateCsvs < ActiveRecord::Migration
  def change
    create_table :csvs do |t|
      t.attachment :file
      t.string :kind

      t.timestamps
    end
  end
end
