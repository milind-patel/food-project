class ChangeEventParticipationUntilFieldName < ActiveRecord::Migration
  def change
    remove_column :events, :participation_until
    add_column :events, :participation_until_hours, :float, default: 0
  end
end
