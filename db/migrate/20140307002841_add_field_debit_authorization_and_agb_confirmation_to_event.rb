class AddFieldDebitAuthorizationAndAgbConfirmationToEvent < ActiveRecord::Migration
  def change
    add_column :events, :agb_confirmation, :boolean, default: false
    add_column :events, :debit_authorization, :boolean, default: false
  end
end
