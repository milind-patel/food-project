class AddMainImageIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :main_image, :integer
  end
end
