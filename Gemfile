source 'https://rubygems.org'

gem 'rails', '4.2.0'
gem 'mysql2', '~> 0.3.18'
gem 'thin', '1.6.3'
gem 'dalli', '2.7.2'

# monitoring
gem 'newrelic_rpm'
gem 'rollbar'

# assets
gem 'therubyracer', platforms: :ruby
gem 'sass-rails', '~> 5.0.1'
gem 'coffee-rails', '~> 4.1.0'
gem 'uglifier', '>= 2.7.0'
gem 'jquery-rails', '4.0.3'
gem 'jquery-ui-rails', '5.0.3'
gem 'compass-rails', '~> 2.0.4'
gem 'less-rails-fontawesome', '~> 0.8.0'
gem 'twitter-bootstrap-rails'
gem 'tinymce-rails'
gem 'tinymce-rails-langs'
gem 'paperclip', '~> 4.2.1'
gem 'jquery-fileupload-rails', '0.4.1'
gem 'spinjs-rails', '~> 1.3'

# views
gem 'turbolinks', '~> 2.5.3'
gem 'html5shiv-rails'
gem 'haml-rails', '~> 0.8.2'
gem 'simple_form', '3.1.0'
gem 'kaminari', '0.16.3'
gem 'icheck-rails'
gem 'active_model_serializers', '~> 0.8.1'
gem 'social-share-button'
gem 'wkhtmltopdf-binary'
gem 'wicked_pdf', '~> 0.10.2'

# view helper
gem 'cells', '~> 3.10.1'
gem 'draper', '~> 1.4.0'
gem 'rails-i18n', '~> 4.0.1'
gem 'momentjs-rails', '~> 2.5.0'
gem 'bootstrap3-datetimepicker-rails', '~> 3.0.0'
gem 'data-confirm-modal', github: 'ifad/data-confirm-modal'
gem 'i18n-country-translations', '~> 1.0.2'
gem 'i18n-language-translations', '~> 0.0.2'
gem 'awesome_print'

# authentication & authorization
gem 'devise', '~> 3.4.1'
gem 'devise-encryptable', '~> 0.2.0'
gem 'omniauth-facebook', '~> 2.0.0'
gem 'cancan', '~> 1.6.10'

# geocoding
gem 'geocoder', '~> 1.2.3'

# mailing
gem 'premailer-rails', '~> 1.7.0'
gem 'nokogiri'
gem 'mail_view'
gem 'gibbon', '~> 1.1.5' #mailchimp api wrapper

# models
gem 'paranoia', '~> 2.1.0'
gem 'active_attr', '~> 0.8.5'
gem 'acts_as_list', '~> 0.3.0'
gem 'bitmask_attributes', '~> 1.0.0'

# state machine
gem 'state_machine', '~> 1.2.0'
gem 'state_machine-audit_trail', '~> 0.1.7'

# async/background jobs
gem 'sidekiq', '~> 3.3.2'
gem 'devise-async', '~> 0.9.0'
gem 'sinatra', '>= 1.3.0', :require => nil
gem 'sidetiq', git: 'https://github.com/wonderweblabs/sidetiq.git'

# command line
gem 'rb-readline'

# encryption
gem "attr_encrypted", "~> 3.0.0"


# sitemap
gem 'sitemap_generator'

# settings
gem 'settingslogic'

# cors
gem 'rack-cors'

# dev
group :development do
  # deployment
  gem 'capistrano', '~> 3.3.5'
  gem 'capistrano-rails', '~> 1.1.2'
  gem 'capistrano-bundler', '~> 1.1.4'
  gem 'capistrano-sidekiq', '~> 0.4.0'
  gem 'capistrano-rbenv', '~> 2.0.3'

  # debugung tools
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'meta_request'

  # security checking via breakeman
  gem "brakeman", :require => false

  # logging
  gem 'quiet_assets'

  # uml generation
  gem 'railroady'
end

# doc
group :doc do
  gem 'sdoc', require: false
end

# testing
group :development, :test do
  gem 'rspec-rails'
  gem 'spork-rails'
  gem 'simplecov', require: false
  gem 'hirb'
  gem 'wirble'
  gem 'factory_girl_rails'
  gem 'database_cleaner', git: 'git@github.com:bmabey/database_cleaner.git'
  gem 'steak'
end

# dev & test
group :development, :test do
  # rails
  gem 'spring'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Call 'console' anywhere in the code to show debugger-console in frontend
  gem 'web-console', '~> 2.0'
end

