lock '3.3.5'

# repository
set :application, 'foodoo'
set :repo_url, '???'
set :scm, :git
set :keep_releases, 3

# User
set :use_sudo, false
ENV['USER'] = %x[git config --global user.name].gsub(' ', '_')

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :format is :pretty
set :format, :pretty

# Default value for :linked_files is []
set :linked_files, [
  'config/application.yml',
  'config/database.yml',
  'config/email.yml',
  'config/secrets.yml',
  'config/thin.yml',
  'config/thin_documents.yml',
  'config/sidekiq.yml',
]

# Default value for linked_dirs is []
set :linked_dirs, [
  'bin',
  'log',
  'public/system',
  'public/assets',
  'public/images',
  'tmp/pids',
  'tmp/cache',
  'tmp/sockets',
  'vendor/bundle',
  'uploads'
]

# rollbar
set :rollbar_token, '???'
set :rollbar_env, Proc.new { fetch :stage }

# deploy
namespace :deploy do
  after 'deploy:finishing', 'thin:restart'
  after 'deploy:finishing', 'sitemap:refresh'
end
