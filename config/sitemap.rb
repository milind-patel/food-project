# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.foodoo.de"
SitemapGenerator::Sitemap.verbose = true

SitemapGenerator::Sitemap.namer = SitemapGenerator::SimpleNamer.new(:sitemap, extension: '.xml')
SitemapGenerator::Sitemap.adapter = Class.new(SitemapGenerator::FileAdapter) do
 def gzip(stream, data); stream.write(data); stream.close end
end.new

SitemapGenerator::Sitemap.create do
  add root_path(category: 'people'), changefreq: 'daily', priority: 1
  add root_path(category: 'dating'), changefreq: 'daily', priority: 1
  add root_path(category: 'course'), changefreq: 'daily', priority: 1

  add blog_entries_path, changefreq: 'daily', priority: 0.9

  add faq_path, changefreq: 'weekly', priority: 0.8
  add how_it_works_path, changefreq: 'monthly', priority: 0.8
  add about_us_path, changefreq: 'monthly', priority: 0.7
  add contact_path, changefreq: 'yearly', priority: 0.7
end
