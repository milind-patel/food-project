set :stage, :staging
server '???', user: 'deploy', roles: %w{web app db}

set :deploy_to, "/opt/foodoo"
set :branch, "develop"
set :thin_config_paths, ['/opt/foodoo/shared/config/thin.yml']

# Rails
set :rails_env, :staging

set :rbenv_path, '/home/deploy/.rbenv'
# config/deploy.rb
set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.2.0'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

# bundler
set :bundle_flags, "--deployment --quiet"

set :sidekiq_options, '-C config/sidekiq.yml'
