# Be sure to restart your server when you modify this file.

# Foodoo::Application.config.session_store :cookie_store, key: '_foodoo_session'
Foodoo::Application.config.session_store ActionDispatch::Session::CacheStore, :expire_after => 48.hours
