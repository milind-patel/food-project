# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.

Rails.application.config.assets.precompile += %w( public/base.js public/base.css public/base_libs.css )
Rails.application.config.assets.precompile += %w( pdf/base.css )
Rails.application.config.assets.precompile += %w( backend/base.js backend/base.css )
Rails.application.config.assets.precompile += %w( backend_auth/base.js backend_auth/base.css )
Rails.application.config.assets.precompile += %w( backend_auth/base.js backend_auth/base.css )

Rails.application.config.assets.precompile += %w( tinymce/skins/custom/* )

Rails.application.config.assets.precompile += %w( progressbar.gif )
Rails.application.config.assets.precompile += %w( loading.gif )
Rails.application.config.assets.precompile += %w( slick.eot slick.ttf )
Rails.application.config.assets.precompile += %w( slick.woff slick.svg )
