require 'sidekiq/web'
require 'sidetiq/web'

Foodoo::Application.routes.draw do
  constraints format: /html|js|json|pdf/ do

    scope module: 'public' do
      get '/agb' => 'pages#agb', as: 'agb'
      get '/kontakt' => 'pages#contact', as: 'contact'
      get '/ueber-uns' => 'pages#about_us', as: 'about_us'
      get '/impressum' => 'pages#imprint', as: 'imprint'
      get '/so-funktionierts' => 'pages#how_it_works', as: 'how_it_works'
      get '/faq' => 'pages#faq', as: 'faq'

      # resources :events
      resources :event_appointments, only: [:show],  path: 'events'
      resources :users, only: [:show]
      resources :blog_entries, only: [:index, :show], path: 'blog' do
        get "/:year/:month/:day/:alias" => "blog_entries#show", as: 'complete', on: :collection
      end
      get '/blog-archiv', to: redirect('/blog')

      resources :errors, only: [:error_404, :error_500]
      resource :newsletter_subscriptions, only: [:create, :edit, :update] do
        get :success
      end

      if Rails.env.development?
        get '/templates' => 'pages#templates'
      end
    end


    # --------------------------------------------
    # intern (internal user area)
    devise_for :users,
      path: '',
      controllers: {
        sessions: 'auth/sessions',
        passwords: 'auth/passwords',
        unlocks: 'auth/unlocks',
        confirmations: 'auth/confirmations',
        registrations: 'auth/registrations',
        omniauth_callbacks: 'auth/omniauth_callbacks'
      },
      path_names: {
        sign_in: 'anmelden',
        sign_out: 'abmelden',
        password: 'neues-passwort',
        confirmation: 'konto-bestaetigen',
        registration: 'registrierung',
        unlock: 'entsperren'
      },
      skip: [:passwords, :confirmations, :unlocks]
    as :user do
      get 'abmelden' => 'devise/sessions#destroy'
      get 'registrierung' => 'auth/registrations#new'

      # passwords
      post 'neues-passwort' => 'auth/passwords#create', as: 'user_password'
      get 'neues-passwort' => 'auth/passwords#new', as: 'new_user_password'
      get 'neues-passwort/neu' => 'auth/passwords#edit', as: 'edit_user_password'
      patch 'neues-passwort/neu' => 'auth/passwords#update'
      put 'neues-passwort/neu' => 'auth/passwords#update'

      # confirmations
      get 'konto-bestaetigen' => 'auth/confirmations#new', as: 'new_user_confirmation'
      get 'konto-bestaetigen/bestaetigen' => 'auth/confirmations#show', as: 'user_confirmation'
      post 'konto-bestaetigen/bestaetigen' => 'auth/confirmations#create'

      # unlocks
      get 'entsperren' => 'auth/unlocks#new', as: 'new_user_unlock'
      get 'entsperren/entsperren' => 'auth/unlocks#show', as: 'user_unlock'
      post 'entsperren/entsperren' => 'auth/unlocks#create'
    end

    namespace :internal, path: 'profil' do
      resources :events, only: [:new, :create] do
        get :back, on: :member
        get :gallery, on: :member
        get :successfully_created, on: :member, path: 'erfolgreich-erstellt'
        resource :event_data, only: [:show], path: 'kontakt-daten'
      end
      resources :event_appointments, only: [:edit, :update] do
        delete 'destroy'
        delete 'uninvite/:participation_id' => :uninvite, as: 'uninvite'
        delete 'not_appear/:participation_id' => :not_appear, as: 'not_appear'
        resource :participation, only: [:show, :new, :create, :destroy], path: 'teilnahme', path_names: { new: 'buchen' }
        resource :rating, only: [:new, :create, :show], path: 'bewertung', path_names: { new: 'bewerten' }
      end
      resources :images
      resource :user, only: [:show, :edit, :update] do
        get :gallery, on: :member
        get :edit_password, to: 'users#edit_password'
        patch :update_password, to: 'users#update_password'
        resources :user_languages, only: [:index, :new, :create, :destroy] do
          put :update_all, on: :collection
        end
      end
      resources :images, only: [:create, :index, :new, :show, :destroy]
    end


    # --------------------------------------------
    # devise & backend
    devise_for :backend_users

    namespace :backend do
      get '/' => 'dashboards#show'

      resource :dashboard, only: :show
      resources :events, only: [:show, :index, :edit, :update] do
        put :decline, on: :member
        put :approve, on: :member
        put :submit, on: :member
        resources :images, only: [:new, :create, :destroy]
      end
      resources :user_exports
      resources :blog_entries, except: [:show], path: 'blog', shallow: true do
        post :toggle_published, on: :member
        resources :blog_elements, except: [:index, :show] do
          put :position_up, on: :member
          put :position_down, on: :member
        end
      end
      resources :cities, except: [:show], path: 'staedte'
      resources :users, only: [:index, :show], shallow: true
      resources :backend_users, except: [:show]
      resources :background_jobs, only: [:index]
    end


    # --------------------------------------------
    # defaults

    get 'errors/error_404'
    get 'errors/oauth_failure'

    root 'public/event_appointments#index'


    # --------------------------------------------
    # sidekiq

    constraints(Constraint::Sidekiq) do
      mount Sidekiq::Web, at: "/backend/sidekiq", as: :sidekiq
    end


    # --------------------------------------------
    # mail testing

    if Rails.env.development?
      mount UserMailerPreview => 'user_mail_view'
      mount EventMailerPreview => 'event_mail_view'
    end


    # --------------------------------------------
    # catch all

    unless Rails.application.config.consider_all_requests_local
      match '*not_found', to: 'errors#error_404', via: [:get]
    end

  end
end
