== README

==== Start Project
* mysql.server start
* /usr/local/opt/memcached/bin/memcached &
* sidekiq -C config/sidekiq.yml
* bundle exec rails server


==== Ruby version

* ruby-2.0.0-p353

==== System dependencies

* memcached (auch in development - für session_store)
* redis

==== Configuration

* database.yml
* application.yml
* sidekiq.yml

==== Services (job queues, cache servers, search engines, etc.)

* upcoming - elasticsearch, aber aktuell noch nicht.

==== Ratings

* ratings werden (via sidekiq) alle 2 Stunden neu berechnet
* Manuell via rake task: ```rake user:reaggregate_ratings```

==== Dev-System starten

* Rails-Server beenden
* application.yml -> asset_host auf 'XYZ.dev.wonderweblabs.com' setzen
* rake assets:clobber 
* rails s -p4000