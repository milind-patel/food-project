require 'spec_helper'

describe TransactionService do
  describe '#create_transaction' do

    subject(:service) {TransactionService.new}

    subject(:user) do
      user = User.where(email: 'marcus@wwl.de').first_or_create do |user|
        user.name = 'Marcus'
        user.password = 'password'
        user.city = 'Trebbin'
        user.interests = 'Volleyball, Kochen, Programmieren'
        user.job = 'Developer (Software)'
        user.relationship = :married
        user.skip_confirmation!
      end

      address = Address.new(
        firstname: 'Test',
        lastname: 'Lastname',
        city: 'My City',
        street: 'Street 1',
        zip: 'Zip',
      )
      if user.invoice_address.nil?
        user.addresses << address
        user.save
        user.invoice_address = address
        user.save
      end

      user.reload
      user
    end

    subject(:category) {Category.find_or_create_by(name: "dating")}

    subject(:event) do
      Event.create!({
        sex_direction: 'mf',
        owner: user,
        title: "Frikatzeé",
        description: "Hmm das wird sicherlich nicht so toll, is das 1. mal dass ich koche!!",
        price: 550,
        address_city: "Berlin",
        address_zip: "12345",
        address_street: "Köpenicker Landstraße 254b",
        address_district: "Treptow-Köpenick",
        address_addition: "im Hinterhof",
        max_guest_count: 7,
        self_participation: true,
        category: category,
        state: :approved,
        fake_latitude: 52.12345,
        fake_longitude: 13.123456,
        latitude: 52.5232435,
        longitude: 13.43145
      })
    end

    subject(:event_appointment) do
      EventAppointment.create!({ event: event, start_time: Time.now + 4.days })
    end

    it 'create a transaction object' do
      Transaction::Base.all.count.should be_eql 0

      Participation.create(
        user: user,
        event_appointment: event_appointment,
        guests_count: 2
      )

      event_appointment.reload

      service.create_transaction(event_appointment)

      Transaction::Base.all.count.should be_eql 1
    end

    it 'calculates the price' do
      participation = Participation.new(
        user: user,
        event_appointment: event_appointment,
        male_count: 2,
        state: :accepted
      ).save

      event_appointment.reload
      event_appointment.recalculate_guests

      service.create_transaction(event_appointment)

      transaction = Transaction::Base.last
      event_appointment.guests_count.should be_eql 3
      puts transaction.value
      transaction.value.should be_eql((-3 * 550 * Settings.event_appointment_fee).to_i)
    end
  end
end
