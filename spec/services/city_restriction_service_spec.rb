require 'spec_helper'

describe CityRestrictionService do
  subject(:service) {CityRestrictionService.new}

  describe '#address_accessable?' do
    it 'return true for accessable address' do
      service.address_accessable?('Hamburg').should == true
      service.address_accessable?('Berlin').should == true
    end

    it 'return false for unaccessable address' do
      service.address_accessable?('Erkner').should == false
    end

    it 'return false for empty address' do
      service.address_accessable?('').should == false
    end

    it 'return false for nil address' do
      service.address_accessable?(nil).should == false
    end
  end
end
