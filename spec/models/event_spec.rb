require 'spec_helper'

describe Event do

  describe 'validations' do
    it 'should check presence of sex_direction' do
      e = build(:event, sex_direction: nil)
      e.valid?.should be_false
    end
    it 'should check if sex_direction is from allowed set' do
      e = build(:event, :mm)
      e.valid?.should be_true
      e = build(:event, :ff)
      e.valid?.should be_true
      e = build(:event)
      e.valid?.should be_true
      e = build(:event, sex_direction: 'aa')
      e.valid?.should be_false
    end
    it 'should check owner presence' do
      e = build(:event)
      e.valid?.should be_true
      e = build(:event, :no_owner)
      e.valid?.should be_false
    end
    it 'should check category presence' do
      e = build(:event)
      e.valid?.should be_true
      e = build(:event, :no_category)
      e.valid?.should be_false
    end
    it 'should validate max_guest_count presence' do
      e = build(:event, max_guest_count: nil)
      e.valid?.should be_false
    end
    it 'should validate max_guest_count larger 0' do
      e = build(:event, max_guest_count: 0)
      e.valid?.should be_false
      e = build(:event, max_guest_count: -1)
      e.valid?.should be_false
    end
    it 'should validate max_guest_count for integer' do
      e = build(:event, max_guest_count: 4.2)
      e.valid?.should be_false
    end
    context 'for dating' do
      context 'on sex_direction mf' do
        it 'check for f equal m' do
          e = build(:event, :dating, sex_direction: 'mf', max_f_guest_count: 10, max_m_guest_count: 0, max_guest_count: 10)
          e.valid?.should be_false
          e = build(:event, :dating, sex_direction: 'mf', max_f_guest_count: 5, max_m_guest_count: 6, max_guest_count: 11)
          e.valid?.should be_false
          e = build(:event, :dating, sex_direction: 'mf', max_f_guest_count: 5, max_m_guest_count: 5, max_guest_count: 10)
          e.valid?.should be_true
        end
      end
      context 'on sex_direction mm' do
        it 'check if m is even' do
          e = build(:event, :dating, sex_direction: 'mm', max_m_guest_count: 9, max_guest_count: 9)
          e.valid?.should be_false
          e = build(:event, :dating, sex_direction: 'mm', max_m_guest_count: 10, max_guest_count: 10)
          e.valid?.should be_true
        end
      end
      context 'on sex_direction ff' do
        it 'check if f is even' do
          e = build(:event, :dating, sex_direction: 'ff', max_f_guest_count: 9, max_guest_count: 9)
          e.valid?.should be_false
          e = build(:event, :dating, sex_direction: 'ff', max_f_guest_count: 10, max_guest_count: 10)
          e.valid?.should be_true
        end
      end
    end
    context 'for dating and self_participation == true' do
      context 'on sex_direction mf' do
        it 'should if f = m+1 for female owner' do
          e = build(:event, :dating, :self_participating, sex_direction: 'mf', max_f_guest_count: 5, max_m_guest_count: 5, max_guest_count: 10)
          e.owner.gender = 'f'
          e.valid?.should be_false
          e = build(:event, :dating, :self_participating, sex_direction: 'mf', max_f_guest_count: 6, max_m_guest_count: 5, max_guest_count: 11)
          e.owner.gender = 'f'
          e.valid?.should be_false
          e = build(:event, :dating, :self_participating, sex_direction: 'mf', max_f_guest_count: 4, max_m_guest_count: 5, max_guest_count: 9)
          e.owner.gender = 'f'
          e.valid?.should be_true
        end
        it 'should if m = f+1 for male owner' do
          e = build(:event, :dating, :self_participating, sex_direction: 'mf', max_f_guest_count: 5, max_m_guest_count: 5, max_guest_count: 10)
          e.owner.gender = 'm'
          e.valid?.should be_false
          e = build(:event, :dating, :self_participating, sex_direction: 'mf', max_f_guest_count: 5, max_m_guest_count: 6, max_guest_count: 11)
          e.owner.gender = 'm'
          e.valid?.should be_false
          e = build(:event, :dating, :self_participating, sex_direction: 'mf', max_f_guest_count: 5, max_m_guest_count: 4, max_guest_count: 9)
          e.owner.gender = 'm'
          e.valid?.should be_true
        end
      end
      context 'on sex_direction mm' do
        it 'should check if owner is male' do
          e = build(:event, :dating, :self_participating, :mm, max_m_guest_count: '9', max_guest_count: 9)
          e.owner.gender = 'f'
          e.valid?.should be_false
          e.owner.gender = 'm'
          e.valid?.should be_true
          e = build(:event, :dating, :self_participating, :mm, max_m_guest_count: '7', max_guest_count: 9)
          e.owner.gender = 'm'
          e.valid?.should be_false
        end
        it 'should check if m is odd' do
          e = build(:event, :dating, :self_participating, :mm, max_m_guest_count: 8, max_guest_count: 8)
          e.owner.gender = 'm'
          e.valid?.should be_false
          e = build(:event, :dating, :self_participating, :mm, max_m_guest_count: 9, max_guest_count: 9)
          e.owner.gender = 'm'
          e.valid?.should be_true
        end
      end
      context 'on sex_direction ff' do
        it 'should check if owner is female' do
          e = build(:event, :dating, :self_participating, :ff, max_f_guest_count: '9', max_guest_count: 9)
          e.owner.gender = 'm'
          e.valid?.should be_false
          e.owner.gender = 'f'
          e.valid?.should be_true
          e = build(:event, :dating, :self_participating, :ff, max_f_guest_count: '7', max_guest_count: 9)
          e.owner.gender = 'f'
          e.valid?.should be_false
        end
        it 'should check if f is odd' do
          e = build(:event, :dating, :self_participating, :ff, max_f_guest_count: 8, max_guest_count: 8)
          e.owner.gender = 'f'
          e.valid?.should be_false
          e = build(:event, :dating, :self_participating, :ff, max_f_guest_count: 9, max_guest_count: 9)
          e.owner.gender = 'f'
          e.valid?.should be_true
        end
      end
    end
  end

  describe '#drinks' do
    it 'returns existing drinks' do
      d = Drink.create(name: 'Test')
      e = build(:event)

      relation = Relation::EventDrink.create(drink: d, event: e)
      e.reload

      e.drinks.should be_include(d)
    end
  end

  describe '#drinks=' do
    it 'add drinks' do
      d = Drink.create(name: 'Test')
      e = build(:event)
      e.drinks = [d]

      e.drinks.should be_include(d)
    end

    it 'add drinks only once' do
      d = Drink.create(name: 'Test')
      e = build(:event)
      e.drinks = [d, d]

      e.drinks.should be_include(d)

      e.drinks.count.should == 1
    end
  end

  describe '#drink_ids' do
    it 'returns existing drinks' do
      d = Drink.create(name: 'Test')
      e = build(:event)

      relation = Relation::EventDrink.create(drink_id: d.id, event: e)
      e.reload
      e.drink_ids.should be_include(d.id)
    end
  end

  describe '#drink_ids=' do
    it 'add drinks' do
      d1 = Drink.create(name: 'Test')
      d2 = Drink.create(name: 'Test2')

      e = build(:event)
      e.drink_ids = [d1.id, d2.id]

      e.drink_ids.should be_include(d1.id)
      e.drink_ids.should be_include(d2.id)
    end

    it 'add nil drinks' do
      d = Drink.create(name: 'Test')

      e = build(:event)
      e.drink_ids = [d.id]
      e.drink_ids.count.should == 1

      e.drink_ids = nil
      e.drink_ids.count.should == 0
    end
  end

  describe '#homo?' do
    it 'should be true if sex_direction is mm' do
      e = build(:event, sex_direction: 'mm')
      e.homo?.should be_true
    end
    it 'should be true if sex_direction is ff' do
      e = build(:event, sex_direction: 'ff')
      e.homo?.should be_true
    end
    it 'should be false if sex_direction is mf' do
      e = build(:event, sex_direction: 'mf')
      e.homo?.should be_false
    end
    it 'should be inverse of #hetero?' do
      e = build(:event, sex_direction: 'mf')
      e.homo?.should be_false
      e.hetero?.should be_true
      e = build(:event, sex_direction: 'ff')
      e.homo?.should be_true
      e.hetero?.should be_false
    end
  end

  describe '#dating?' do
    it 'should be true for dating category' do
      e = build(:event, :course)
      e.dating?.should be_false
      e = build(:event, :dating)
      e.dating?.should be_true
      e = build(:event)
      e.dating?.should be_false
    end
  end

  describe 'validates city_restriction' do
    it 'add error for unaccessable city' do
      e = build(:event)
      e.address_city = 'Erkner'

      e.city_restriction

      e.errors.should have_key(:address_city)
      e.errors.should have_key(:address_street)
      e.errors.should have_key(:address_district)
      e.errors.should have_key(:address_zip)
    end

    it 'add no error for accessable city' do
      e = build(:event)
      e.address_city = 'Berlin'

      e.city_restriction

      e.errors.should_not have_key(:address_city)
      e.errors.should_not have_key(:address_street)
      e.errors.should_not have_key(:address_district)
      e.errors.should_not have_key(:address_zip)
    end
  end
end
