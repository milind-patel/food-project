require 'spec_helper'

describe EventAppointment do

  describe '#close' do
    it 'create transaction after transition' do
      e = create(:event, max_guest_count: 3)
      a = create(:event_appointment, event: e)

      Transaction::Intern.all.count.should be_eql 0
      a.close
      Transaction::Intern.all.count.should be_eql 1
    end

    it 'change state' do
      e = create(:event, max_guest_count: 3)
      a = create(:event_appointment, event: e)

      a.state.should be_eql('created')
      a.close
      a.state.should be_eql('closed')
    end
  end

  describe '#places_left' do
    context 'with homo? true' do
      it 'should return overall places left' do
        e = create(:event, :mm, max_m_guest_count: 6, max_guest_count: 6)
        a = create(:event_appointment, event: e)
        a.places_left.should eq(6)

        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left.should eq(3)

        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left.should eq(0)
      end
    end
    context 'with no guests' do
      it 'should return max_guests' do
        e = create(:event, :mm, max_m_guest_count: 6, max_guest_count: 6)
        a = create(:event_appointment, event: e)
        a.places_left.should eq(6)

        e = create(:event, :ff, max_f_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left.should eq(10)

        e = create(:event, max_guest_count: 9)
        a = create(:event_appointment, event: e)
        a.places_left.should eq(9)

        e = create(:event, :dating, max_m_guest_count: 3, max_f_guest_count: 3, max_guest_count: 6)
        a = create(:event_appointment, event: e)
        a.places_left.should eq(6)
      end
    end
    context 'with plain participations' do
      it 'should return free places' do
        e = create(:event, max_guest_count: 3)
        a = create(:event_appointment, event: e)
        a.places_left.should eq(3)

        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left.should eq(2)

        create(:participation, :with_female_user, event_appointment: a)
        create(:participation, :with_female_user, event_appointment: a)
        a.recalculate_guests.places_left.should eq(0)
      end
      it 'should return free places for dating' do
        e = create(:event, :dating, max_m_guest_count: 3, max_f_guest_count: 3, max_guest_count: 6)
        a = create(:event_appointment, event: e)
        a.places_left.should eq(6)

        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left.should eq(5)

        create(:participation, :with_female_user, event_appointment: a)
        create(:participation, :with_female_user, event_appointment: a)
        a.recalculate_guests.places_left.should eq(3)

        create(:participation, :with_female_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left.should eq(0)
      end
    end
    context 'with guests on participations' do
      it 'should return free places' do
        e = create(:event, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left.should eq(10)

        create(:participation, :with_male_user, male_count: 2, event_appointment: a)
        a.recalculate_guests.places_left.should eq(7)

        create(:participation, :with_female_user, event_appointment: a)
        a.recalculate_guests.places_left.should eq(6)

        create(:participation, :with_user, male_count: 3, female_count: 2, event_appointment: a)
        a.recalculate_guests.places_left.should eq(0)
      end
    end
  end

  describe '#places_left_male' do
    context 'without sex_direction ff' do
      it 'should return zero' do
        e = create(:event, :dating, :ff, max_f_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_male.should eq(0)
        e = create(:event, :mm, max_f_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_male.should eq(10)
      end
    end
    context 'with no guests' do
      it 'should return max_guests_male' do
        e = create(:event, :mm, max_m_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_male.should eq(10)
      end
    end
    context 'with plain participations' do
      it 'should return free places' do
        e = create(:event, :mm, max_m_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_male.should eq(10)

        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left_male.should eq(9)

        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left_male.should eq(7)

        e = create(:event, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_male.should eq(10)

        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left_male.should eq(9)

        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_female_user, event_appointment: a)
        create(:participation, :with_female_user, event_appointment: a)
        create(:participation, :with_female_user, event_appointment: a)
        a.recalculate_guests.places_left_male.should eq(4)
      end
    end
    context 'with guests on participations' do
      it 'should return free places' do
        e = create(:event, :mm, max_m_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_male.should eq(10)

        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left_male.should eq(9)

        create(:participation, :with_male_user, male_count: 3, event_appointment: a)
        a.recalculate_guests.places_left_male.should eq(5)

        e = create(:event, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_male.should eq(10)

        create(:participation, :with_male_user, male_count: 3, female_count: 5, event_appointment: a)
        a.recalculate_guests.places_left_male.should eq(1)
      end
    end
  end

  describe '#places_left_female' do
    context 'without sex_direction mm' do
      it 'should return zero' do
        e = create(:event, :dating, :mm, max_m_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_female.should eq(0)
        e = create(:event, :ff, max_m_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_female.should eq(10)
      end
    end
    context 'with no guests' do
      it 'should return max_guests_female' do
        e = create(:event, :ff, max_f_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_female.should eq(10)
      end
    end
    context 'with plain participations' do
      it 'should return free places' do
        e = create(:event, :ff, max_f_guest_count: 10, max_guest_count: 10)
        a = create(:event_appointment, event: e)
        a.places_left_female.should eq(10)

        create(:participation, :with_female_user, event_appointment: a)
        create(:participation, :with_female_user, event_appointment: a)
        a.recalculate_guests.places_left_female.should eq(8)

        create(:participation, :with_female_user, event_appointment: a)
        a.recalculate_guests.places_left_female.should eq(7)

        e = create(:event, max_guest_count: 12)
        a = create(:event_appointment, event: e)
        a.places_left_female.should eq(12)

        create(:participation, :with_female_user, event_appointment: a)
        a.recalculate_guests.places_left_female.should eq(11)

        create(:participation, :with_female_user, event_appointment: a)
        create(:participation, :with_female_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        create(:participation, :with_male_user, event_appointment: a)
        a.recalculate_guests.places_left_female.should eq(6)
      end
    end
    context 'with guests on participations' do
      it 'should return free places' do
        e = create(:event, :ff, max_f_guest_count: 14, max_guest_count: 14)
        a = create(:event_appointment, event: e)
        a.places_left_female.should eq(14)

        create(:participation, :with_female_user, female_count: 8, event_appointment: a)
        a.recalculate_guests.places_left_female.should eq(5)

        e = create(:event, max_guest_count: 7)
        a = create(:event_appointment, event: e)
        a.places_left_female.should eq(7)

        create(:participation, :with_male_user, male_count: 4, female_count: 2, event_appointment: a)
        a.recalculate_guests.places_left_female.should eq(0)
      end
    end
  end


end
