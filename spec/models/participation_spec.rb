require 'spec_helper'

describe Participation do

  describe 'guest counting' do
    it 'should add user to count' do
      e = create(:event, :dating)
      a = create(:event_appointment, event: e)

      p = build(:participation, :with_male_user, event_appointment: a)
      p.guests_sum.should be(1)
      p.male_guests_sum.should be(1)
      p.female_guests_sum.should be(0)
      p.female_count = 3
      p.female_guests_sum.should be(3)
      p.male_count = 5
      p.male_guests_sum.should be(6)
    end
  end

  describe 'base validations' do
    it 'should check if user is present' do
      e = create(:event)
      a = create(:event_appointment, event: e)

      p = build(:participation, event_appointment: a, user: nil)
      p.valid?.should be_false
      p.user = build(:user)
      p.valid?.should be_true
    end
    it 'should check if event_appointment is present' do
      e = create(:event)
      a = create(:event_appointment, event: e)

      p = build(:participation, :with_user, event_appointment: nil)
      p.valid?.should be_false
      p.event_appointment = a
      p.valid?.should be_true
    end
    it 'should check if event_appointment is not in past' do
      e = create(:event)
      a = create(:event_appointment, event: e)
      a.start_time = Time.zone.now - 1.day

      p = build(:participation, :with_user, event_appointment: a)
      p.valid?.should be_false
      a.start_time = Time.zone.now + 1.day
      p.valid?.should be_true
    end
  end

  context 'on mm event' do
    before(:each) do
      @event = create(:event, :mm)
      @appointment = create(:event_appointment, event: @event)
    end
    it 'should deny female for male event' do
      p = build(:participation, :with_female_user, event_appointment: @appointment)
      p.valid?.should be_false
    end
    it 'should not accept female_count > 0' do
      p = build(:participation, :with_male_user, event_appointment: @appointment)
      p.valid?.should be_true
      p.female_count = 1
      p.valid?.should be_false
    end
    it 'should not be valid for full event' do
      @event.update_attributes(max_m_guest_count: 2, max_guest_count: 2)

      p = build(:participation, :with_male_user, event_appointment: @appointment)
      p.save.should be_true
      p = build(:participation, :with_male_user, event_appointment: @appointment)
      p.save.should be_true
      p = build(:participation, :with_male_user, event_appointment: @appointment)
      p.save.should be_false
    end
    it 'should not be valid for one user too many' do
      @event.update_attributes(max_m_guest_count: 2, max_guest_count: 2)

      p = build(:participation, :with_male_user, male_count: 2, event_appointment: @appointment)
      p.save.should be_false
      p = build(:participation, :with_male_user, male_count: 1, event_appointment: @appointment)
      p.save.should be_true
    end
  end

  context 'on ff event' do
    before(:each) do
      @event = create(:event, :ff)
      @appointment = create(:event_appointment, event: @event)
    end
    it 'should deny male for female event' do
      p = build(:participation, :with_male_user, event_appointment: @appointment)
      p.valid?.should be_false
    end
    it 'should not accept male_count > 0' do
      p = build(:participation, :with_female_user, event_appointment: @appointment)
      p.valid?.should be_true
      p.male_count = 1
      p.valid?.should be_false
    end
    it 'should not be valid for full event' do
      @event.update_attributes(max_f_guest_count: 2, max_guest_count: 2)

      p = build(:participation, :with_female_user, event_appointment: @appointment)
      p.save.should be_true
      p = build(:participation, :with_female_user, event_appointment: @appointment)
      p.save.should be_true
      p = build(:participation, :with_female_user, event_appointment: @appointment)
      p.save.should be_false
    end
    it 'should not be valid for one user too many' do
      @event.update_attributes(max_f_guest_count: 2, max_guest_count: 2)

      p = build(:participation, :with_female_user, female_count: 2, event_appointment: @appointment)
      p.save.should be_false
      p = build(:participation, :with_female_user, female_count: 1, event_appointment: @appointment)
      p.save.should be_true
    end
  end

  context 'on mf event' do
    before(:each) do
      @event = create(:event)
      @appointment = create(:event_appointment, event: @event)
    end
    it 'should not be valid for full event' do
      @event.update_attributes(max_m_guest_count: 2, max_f_guest_count: 2, max_guest_count: 4)

      p = build(:participation, :with_female_user, event_appointment: @appointment)
      p.save.should be_true
      p = build(:participation, :with_female_user, event_appointment: @appointment)
      p.save.should be_true
      p = build(:participation, :with_female_user, event_appointment: @appointment)
      p.save.should be_false

      p = build(:participation, :with_male_user, event_appointment: @appointment)
      p.save.should be_true
      p = build(:participation, :with_male_user, event_appointment: @appointment)
      p.save.should be_true
      p = build(:participation, :with_male_user, event_appointment: @appointment)
      p.save.should be_false
    end
    it 'should not be valid for one user too many' do
      @event.update_attributes(max_guest_count: 4)

      p = build(:participation, :with_female_user, female_count: 2, male_count: 2, event_appointment: @appointment)
      p.save.should be_false
      p = build(:participation, :with_female_user, female_count: 1, male_count: 2, event_appointment: @appointment)
      p.save.should be_true
    end
  end

end
