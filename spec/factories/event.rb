FactoryGirl.define do

  factory :event do
    sequence(:title) { |n| "Event #{n}" }
    association :owner, factory: :user
    max_guest_count 10
    sex_direction 'mf'
    self_participation false

    after(:build) do |user|
      user.category = Category.find_or_create_by(name: 'people') if user.category.nil?
    end

    trait :dating do
      category {Category.find_or_create_by(name: 'dating')}
    end

    trait :course do
      category {Category.find_or_create_by(name: 'course')}
    end

    trait :no_category do
      category nil
      after(:build) do |user|
        user.category = nil
      end
    end

    trait :no_owner do
      owner nil
    end


    trait :mm do
      sex_direction 'mm'
      max_m_guest_count 10
    end

    trait :ff do
      sex_direction 'ff'
      max_f_guest_count 10
    end

    trait :self_participating do
      self_participation true
    end
  end

end