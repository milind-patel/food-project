FactoryGirl.define do

  factory :user do
    sequence(:email) { |n| "user#{n}@example.com" }
    password 'password'
    sequence(:name) { |n| "Peter-#{n}" }
    gender 'm'

    after(:build) do |user|
      user.skip_confirmation!
    end

    after(:create) do |user|
      address = Address.new(
        firstname: 'Test',
        lastname: 'Lastname',
        city: 'My City',
        street: 'Street 1',
        zip: 'Zip',
      )
      user.addresses << address
      user.save
      user.invoice_address = address
      user.save
      user.reload
    end

    trait :female do
      gender 'f'
    end
  end

end