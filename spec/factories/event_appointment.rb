FactoryGirl.define do

  factory :event_appointment do
    start_time (Time.now+1.day)

    trait :with_event do
      association :event, factory: :event, state: 'approved'
    end

  end

end