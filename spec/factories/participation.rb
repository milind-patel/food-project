FactoryGirl.define do

  factory :participation do
    male_count 0
    female_count 0
    guests_count 0

    trait :with_user do
      association :user, factory: :user
    end

    trait :with_male_user do
      association :user, factory: :user, gender: 'm'
    end

    trait :with_female_user do
      association :user, factory: :user, gender: 'f'
    end

    trait :with_event do
      association :event_appointment, factory: :event_appointment
    end
  end

end