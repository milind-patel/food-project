require 'spec_helper'

feature 'Stylesheets' do
  scenario 'are all smaller than the Internet Explorer maximum' do
    stylesheets = []

    City.create!(name: 'Test')

    #### This will examine the HTML at the given URL and check the stylesheets
    #### linked therein. Add similar calls as needed to visit pages that expose
    #### all of your site's CSS
    stylesheets += stylesheets_at_url('/')

    puts '------------'
    stylesheets.each do |stylesheet|
      next if stylesheet.include?('https://fonts.google')
      visit stylesheet
      count = count_selectors(source)

      puts "#{stylesheet}: #{count}"

      count.should be < (4096), "#{stylesheet} contains #{count} selectors, which exceeds Internet Explorer maximum of 4096"
    end
    puts '------------'
  end

  def stylesheets_at_url(url)
    visit url

    page.all(:css, 'link[rel="stylesheet"]', visible: false).map do |node|
      node['href']
    end
  end

  def count_selectors(css)
    css.lines("}").inject(0) { |memo, rule| memo += count_selectors_of_rule(rule) }
  end

  def count_selectors_of_rule(rule)
    rule.partition(/\{/).first.scan(/,/).count.to_i + 1
  end
end