require 'spec_helper'

describe EventAppointmentCloseWorker do
  it 'close events' do
    time = Time.now - Settings.event_appointment_close_time.hours
    e = create(:event, max_guest_count: 3)

    a1 = create(:event_appointment, event: e)
    a1.start_time = Time.now
    a1.save

    a2 = create(:event_appointment, event: e)
    a2.start_time = time - 1.hour
    a2.save

    a1.state.should be_eql('created')
    a2.state.should be_eql('created')

    EventAppointmentCloseWorker.new.perform

    a1.reload
    a2.reload

    a1.state.should be_eql('created')
    a2.state.should be_eql('closed')
  end
end
