class Constraint::Sidekiq

  def self.matches?(request)
    return false unless request.env["warden"].authenticated?(:backend_user)
    ability = Ability.new(request.env['warden'].user(:backend_user))
    ability.can?(:view, :background_jobs)
  end

end