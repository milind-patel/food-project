namespace :languages do

  desc 'Add all Languages to db'
  task :load_and_create => :environment do
    LanguageService.new.load_and_create_languages
  end
end
