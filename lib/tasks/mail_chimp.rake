namespace :mail_chimp do

  desc 'All List-Ids'
  task :lists => :environment do
    api = MailChimpConnectorService.new.api
    response = api.lists.list

    if response.present?
      infos = response['data'].map do |hash|
        {
          id: hash['id'],
          name: hash['name'],
        }
      end

      puts ap(infos)
    else
      puts 'No Lists available'
    end
  end

  desc 'Synchronize database with mailchimp'
  task :sync => :environment do
    MailChimpConnectorService.new.synchronize
  end

end
