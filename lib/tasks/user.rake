namespace :user do

  desc 'Reaggregates all ratings for all user'
  task :reaggregate_ratings => :environment do
    User.reaggregate_ratings_all
  end

  desc 'Register all '
  task :migrator_for_newsletter => :environment do
    users = User.all
    city = City.first
    ActiveRecord::Base.transaction do
      users.each do |user|
        subscription = NewsletterSubscription.find_or_initialize_by(email: user.email)
        subscription.city = city
        subscription.user = user
        subscription.save
      end
    end
  end

  desc 'Checks the newsletter subscriptions for existing users'
  task :check_newsletter_subscriptions => :environment do
    ActiveRecord::Base.transaction do
      User.all.each do |user|
        user.check_newsletter_subscription
      end
    end
  end

  desc 'Sets a default city for users without a newsletter city'
  task :set_default_city => :environment do
    ActiveRecord::Base.transaction do
      User.all.each do |user|
        if user.newsletter_city.nil?
          user.newsletter_city = City.first
          user.save
        end
      end
    end
  end

end