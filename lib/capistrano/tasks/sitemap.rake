namespace :sitemap do
  desc 'refresh sitemap'
  task :refresh do
    on roles(:app) do
      with rails_env: fetch(:rails_env) do
        within current_path do
          if fetch(:rails_env) == 'production'
            execute :bundle, "exec rake sitemap:refresh"
          else
            execute :bundle, "exec rake sitemap:refresh:no_ping"
          end
        end
      end
    end
  end
end