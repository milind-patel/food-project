namespace :users do

  desc 'Update newsletter entries for existing users'
  task :check_newsletter_subscriptions do
    on roles(:app) do
      within current_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, "exec rake user:check_newsletter_subscriptions"
        end
      end
    end
  end

  desc 'Sets a default city for users without a newsletter city'
  task :set_default_city do
    on roles(:app) do
      within current_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, "exec rake user:set_default_city"
        end
      end
    end
  end

end