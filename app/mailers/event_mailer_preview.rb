class EventMailerPreview < MailView

  def created_participation_mail
    prepare_default
    EventMailer.created_participation_mail(@participation)
  end

  def decline_participation_as_owner_mail
    prepare_default
    EventMailer.decline_participation_as_owner_mail(@participation)
  end

  def decline_participation_as_guest_mail
    prepare_default
    EventMailer.decline_participation_as_guest_mail(@participation)
  end

  def event_edited_mail
    prepare_default
    EventMailer.event_edited_mail(@participation)
  end

  def event_appointment_canceled_mail
    prepare_default
    EventMailer.event_appointment_canceled_mail(@appointment)
  end

  def rate_event_mail
    prepare_default
    EventMailer.rate_event_mail(@participation)
  end

  def rated_event_mail
    prepare_default
    EventMailer.rated_event_mail(@rating)
  end

  def created_event_admin_notice
    prepare_default
    EventMailer.created_event_admin_notice(@event)
  end


  private

  def prepare_default
    owner = User.new(
      email: 'sascha+owner@wonderweblabs.com',
      password: 'password',
      name: 'Klaus'
    )
    guest = User.new(
      id: 1,
      email: 'sascha+guest@wonderweblabs.com',
      password: 'password',
      name: 'Hans'
    )

    backend_user = BackendUser.new(
      id: 1,
      email: 'sascha+admin@wonderweblabs.com',
      password: 'password',
    )

    category = Category.find_by(name: "people")
    event = Event.new(
      id: 1,
      sex_direction: 'mf',
      owner: owner,
      title: 'Mein tolles Essen',
      description: '...',
      price: 2100,
      category: category,
      state: 'approved',
    )
    @event = event

    @appointment = EventAppointment.new(
      id: 1,
      event: event,
      start_time: Time.now + 1.hour
    )
    event.event_appointments = [@appointment]

    @participation = Participation.new(
      event_appointment: @appointment,
      user: guest,
      guests_count: 1,
      male_count: 1
    )

    @appointment.participations = [@participation]

    @rating = EventRating.new(
      event_appointment: @appointment,
      user: guest,
      total: 3,
      meal: 3,
      atmosphere: 3,
      location: 3,
      price_performance_ratio: 3,
      message: 'Nachricht',
    )
  end

end
