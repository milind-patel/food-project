class UserMailerPreview < MailView
  def confirmation_instructions
    u = User.find_or_initialize_by_email("peter@pan.de")
    u.password = "00000000"
    u.password_confirmation = "00000000"
    u.skip_confirmation!
    u.save
    UserMailer.confirmation_instructions(u, '12345')
  end

  def reset_password_instructions
    u = User.find_or_initialize_by_email("peter@pan.de")
    u.password = "00000000"
    u.password_confirmation = "00000000"
    u.skip_confirmation!
    u.save
    UserMailer.reset_password_instructions(u, '12345')
  end

  def unlock_instructions
    u = User.find_or_initialize_by_email("peter@pan.de")
    u.password = "00000000"
    u.password_confirmation = "00000000"
    u.skip_confirmation!
    u.save
    UserMailer.unlock_instructions(u, '12345')
  end
end
