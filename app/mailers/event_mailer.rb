# encoding: utf-8
class EventMailer < ActionMailer::Base
  layout 'public_mailer'

  default from: Settings.mailer_from


  # admins get noticed about new event
  # > to admins
  def created_event_admin_notice(event)
    subject = 'Neues Event'
    @event = event
    to = event_create_notification_emails

    if to.any?
      mail(to: to, subject: subject)
    end
  end

  # user books events
  # > to owner
  def created_participation_mail(participation)
    subject = 'Neue Gäste'
    @participation = participation
    @appointment = participation.event_appointment
    @event = participation.event_appointment.event
    to = @event.owner.email

    @guests_count_all = participation.guests_count+1
    @guest_count = participation.guests_count

    mail(to: to, subject: subject)
  end

  # owner declines guest participation
  # > to guest
  def decline_participation_as_owner_mail(participation)
    to = participation.user.email
    subject = 'Eventteilnahme abgelehnt'
    @participation = participation
    @appointment = participation.event_appointment
    @event = participation.event_appointment.event

    mail(to: to, subject: subject)
  end

  # guest declines his participation
  # > to owner
  def decline_participation_as_guest_mail(participation)
    subject = 'Absage Teilnahme Event'
    @participation = participation
    @appointment = participation.event_appointment
    @event = participation.event_appointment.event
    to = @event.owner.email

    @guests_count_all = participation.guests_count+1
    @guest_count = participation.guests_count

    mail(to: to, subject: subject)
  end

  # owner edited event
  # > to participations user
  def event_edited_mail(participation)
    to = participation.user.email
    subject = 'Eventinformationen bearbeitet'
    @participation = participation
    @appointment = participation.event_appointment
    @event = participation.event_appointment.event

    mail(to: to, subject: subject)
  end

  # owner cancels event
  # > to participations user
  def event_appointment_canceled_mail(appointment)
    subject = 'Event abgesagt'
    @appointment = appointment
    @event = appointment.event

    participations = @appointment.participations
    mail_addresses = participations.map do |participation|
      participation.user && participation.user.email
    end

    mail_addresses.each do |address|
      mail(to: address, subject: subject)
    end
  end

  # event is ratable
  # > to guest
  def rate_event_mail(participation)
    to = participation.user.email
    subject = 'Event bewerten'
    @participation = participation
    @appointment = participation.event_appointment
    @event = participation.event_appointment.event

    mail(to: to, subject: subject)
  end

  # event has been rated
  # > to owner
  def rated_event_mail(rating)
    subject = 'Neue Bewertung'
    @rating = rating
    @appointment = rating.event_appointment
    @event = rating.event_appointment.event
    to = @event.owner.email

    mail(to: to, subject: subject)
  end


  private

  def event_create_notification_emails
    users = BackendUser.all.find_all do |element|
      element.access_rights?(:receiving_new_event_email)
    end

    users.map(&:email)
  end

end
