# encoding: utf-8
class TestMailer < ActionMailer::Base
  layout 'public_mailer'

  default from: Settings.mailer_from

  # send invoice to event owner
  def test
    subject = 'Foodoo Testmail'
    to = 'sascha@wonderweblabs.com'
    mail(to: to, subject: subject)
  end
end
