class Backend::BaseController < ApplicationController
  before_filter :authenticate_backend_user!
  before_action :set_meta_tags
  layout 'backend'

  # rescue_from CanCan::AccessDenied do |exception|
  #   render :file => 'public/401.html', :status => :not_authorized, :layout => false
  # end

  def current_user
    current_backend_user
  end


  protected

  def set_meta_tags
    view_config.page_meta_robots = 'noindex, follow'
  end

end
