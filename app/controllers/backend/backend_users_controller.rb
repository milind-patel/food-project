class Backend::BackendUsersController < Backend::BaseController
  before_filter :check_abilities

  def index
    @backend_users = BackendUser.order(email: :asc).page(params[:page])
  end

  def new
    @backend_user = BackendUser.new
  end

  def create
    @backend_user = BackendUser.new(params_for_create)

    if @backend_user.save
      flash[:success] = 'Created backend user'
      redirect_to backend_backend_users_path
    else
      render :new
    end
  end

  def edit
    @backend_user = BackendUser.find(params[:id])
  end

  def update
    @backend_user = BackendUser.find(params[:id])

    if @backend_user.update_attributes(params_for_update)
      flash[:success] = 'Updated backend user'
      redirect_to backend_backend_users_path
    else
      render :edit
    end
  end

  def destroy
    @backend_user = BackendUser.find(params[:id])
    @backend_user.destroy if @backend_user
    redirect_to backend_backend_users_path
  end


  private

  def check_abilities
    authorize! :manage, BackendUser
  end

  def params_for_create
    params.require(:backend_user).permit(
      :email,
      :password,
      :password_confirmation,
      access_rights: []
    )
  end

  def params_for_update
    params.require(:backend_user).permit(
      :email,
      access_rights: []
    )
  end

end