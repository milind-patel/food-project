class Backend::CitiesController < Backend::BaseController
  before_filter :check_abilities

  def index
    @cities = City.all.order("name")
  end

  def new
    @city = City.new
  end

  def create
    @city = City.new(city_params)

    if @city.save
      flash[:success] = "Die Stadt wurde erfolgreich hinzugefügt."
      redirect_to backend_cities_path
    else
      flash.now[:warn] = "Die Stadt konnte nicht gespeichert werden."
      render :new
    end
  end

  def edit
    @city = City.find(params[:id])
  end

  def update
    @city = City.find(params[:id])

    if @city.update_attributes(city_params)
      flash[:success] = "Sie Stadt wurde erfolgreich gespeichert."
      redirect_to backend_cities_path
    else
      flash.now[:warn] = "Die Stadt konnte nicht aktualisiert werden."
      render :edit
    end
  end

  def destroy
    city = City.find params[:id]
    city.destroy if city

    if city.destroyed?
      flash[:success] = "Die Stadt wurde erfolgreich gelöscht."
    else
      flash[:alert] = "Beim Löschen der Stadt ist ein Fehler aufgetreten."
    end

    redirect_to backend_cities_path
  end

  private

  def city_params
    params.require(:city).permit(:name)
  end

  def check_abilities
    authorize! :manage, City
  end

end
