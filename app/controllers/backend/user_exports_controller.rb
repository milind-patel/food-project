class Backend::UserExportsController < Backend::BaseController
  before_filter :check_abilities

  def index
    @csvs = Csv.all.where(kind: 'users').order('created_at')
  end

  def show
    csv = Csv.find(params[:id])

    send_file(csv.file.path, filename: csv.filename)
  end

  def create
    csv_string = service.export

    csv = Csv.create!(
      file: StringIO.new(csv_string),
      kind: 'users'
    )

    send_file(csv.file.path, filename: csv.filename)
  end

  private

  def service
    @service ||= CsvUserExportService.new
  end

  def check_abilities
    authorize! :manage, Csv
  end
end
