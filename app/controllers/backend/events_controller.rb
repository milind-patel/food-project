class Backend::EventsController < Backend::BaseController
  before_filter :check_abilities

  def index
    if [nil, 'waiting_for_approval', 'declined', 'created'].include?(state)
      @events = events_by_state_filter.decorate
    else
      @event_appointments = events_by_state_filter.decorate
    end
  end

  def show
    @event = ::Event.find(params[:id]).decorate
  end

  def edit
    @event = ::Event.find(params[:id]).decorate
  end

  def update
    @event = ::Event.find(params[:id]).decorate

    if @event.update(params_for_update)
      flash[:success] = 'event updated'
      redirect_to backend_event_path(@event)
    else
      flash[:error] = 'could not update event'
      render :edit
    end
  end

  # state machine events
  def decline
    change_state('decline')
  end

  def approve
    change_state('approve')
  end

  def submit
    change_state('submit')
  end

  private

  def change_state(state)
    @event = ::Event.find(params[:id]).decorate

    @event.send(state)

    redirect_to action: :show
  end

  def state
    @state ||= begin
      params[:state] || 'waiting_for_approval'
    end
  end

  def check_abilities
    authorize! :manage, Event
  end

  def events_by_state_filter
    case state
      when 'waiting_for_approval'
        ::Event.where(state: state).order(created_at: :desc)
      when 'approved'
        ::EventAppointment.includes(:event)
          .where(state: 'created', events: { state: 'approved' })
          .where('event_appointments.start_time >= ?', Time.now)
          .order(start_time: :asc)
      when 'expired'
        ::EventAppointment.includes(:event)
          .where(state: 'created', events: { state: 'approved' })
          .where('event_appointments.start_time < ?', Time.now)
          .order(start_time: :desc)
      when 'closed'
        ::EventAppointment.includes(:event)
          .where(state: 'closed', events: { state: 'approved' })
          .where('event_appointments.start_time < ?', Time.now)
          .order(start_time: :desc)
      when 'declined'
        ::Event.where(state: state).order(created_at: :desc)
      when 'created'
        ::Event.where(state: state).order(created_at: :desc)
      else
        ::Event.where(state: 'waiting_for_approval').order(created_at: :desc)
    end
  end

  def params_for_update
    params.require(:event).permit(:title, :description)
  end
end
