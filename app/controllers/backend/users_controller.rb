class Backend::UsersController < Backend::BaseController
  before_filter :check_abilities

  def index
    @users = ::UserDecorator.decorate_collection(User.all)
  end

  def show
    @user = User.find(params[:id]).decorate
  end

  private

  def check_abilities
    authorize! :manage, User
  end

end
