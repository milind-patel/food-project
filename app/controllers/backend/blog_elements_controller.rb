# encoding: utf-8
class Backend::BlogElementsController < Backend::BaseController
  before_filter :check_abilities

  def new
    load_blog_entry
    @blog_element = create_element
  end

  def create
    load_blog_entry
    @blog_element = BlogElement::Base.new(params_for_create)
    @blog_element.blog_entry = @blog_entry

    if @blog_element.save
      flash[:success] = "Der Eintrag wurde erfolgreich gespeichert."
      redirect_to edit_backend_blog_element_path(@blog_element)
    else
      flash.now[:error] = "Der Eintrag konnte nicht gespeichert werden."
      render :new
    end
  end

  def edit
    @blog_element = BlogElement::Base.find(params[:id])
  end

  def update
    @blog_element = BlogElement::Base.find(params[:id])

    if @blog_element.update_attributes(params_for_update)
      flash[:success] = "Der Eintrag wurde erfolgreich gespeichert."
      redirect_to edit_backend_blog_element_path(@blog_element)
    else
      flash[:error] = "Der Eintrag konnte nicht aktualisiert werden."
      render :edit
    end
  end

  def destroy
    @blog_element = BlogElement::Base.find(params[:id])
    blog_entry = @blog_element.blog_entry
    @blog_element.destroy
    flash[:success] = "Der Eintrag wurde erfolgreich gelöscht."
    redirect_to edit_backend_blog_entry_path(blog_entry)
  end

  def position_up
    @blog_element = BlogElement::Base.find(params[:id])
    if @blog_element
      flash[:success] = "Der Eintrag wurde erfolgreich verschoben."
      @blog_element.move_higher
    end
    redirect_to edit_backend_blog_entry_path(@blog_element.blog_entry)
  end

  def position_down
    @blog_element = BlogElement::Base.find(params[:id])
    if @blog_element
      flash[:success] = "Der Eintrag wurde erfolgreich verschoben."
      @blog_element.move_lower
    end
    redirect_to edit_backend_blog_entry_path(@blog_element.blog_entry)
  end


  private

  def create_element
    if params[:element_type] == 'text'
      BlogElement::Text.new blog_entry: @blog_entry
    elsif params[:element_type] == 'image'
      BlogElement::Image.new blog_entry: @blog_entry
    end
  end

  def load_blog_entry
    @blog_entry = BlogEntry.find(params[:blog_entry_id])
  end

  def params_for_create
    params.require(:blog_element).permit(:type, :headline, :text, :image)
  end

  def params_for_update
    params.require(:blog_element).permit(:headline, :text, :image)
  end

  def check_abilities
    authorize! :manage, BlogEntry
  end

end
