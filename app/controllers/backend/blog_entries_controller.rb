# encoding: utf-8
class Backend::BlogEntriesController < Backend::BaseController
  before_filter :check_abilities

  def index
    @blog_entries = BlogEntry.all.order("date desc")
  end

  def new
    @blog_entry = BlogEntry.new
  end

  def create
    @blog_entry = BlogEntry.new(blog_entry_params)

    if @blog_entry.save
      flash[:success] = "Der Blogeintrag wurde erfolgreich gespeichert."
      #TODO: check submit vallue and save, or create new Text/Image element
      if add_element_param.present?
        redirect_to new_backend_blog_entry_blog_element_path(@blog_entry, element_type: add_element_param)
      else
        redirect_to edit_backend_blog_entry_path(@blog_entry)
      end
    else
      flash.now[:alert] = "Beim Speichern des Blogeintrages ist ein Fehler aufgetreten."
      render :new
    end
  end

  def edit
    @blog_entry = BlogEntry.find(params[:id])
  end

  def update
    @blog_entry = BlogEntry.find(params[:id])

    if @blog_entry.update_attributes(blog_entry_params)
      flash[:success] = "Der Blogeintrag wurde erfolgreich gespeichert."
      if add_element_param.present?
        redirect_to new_backend_blog_entry_blog_element_path(@blog_entry, element_type: add_element_param)
      else
        redirect_to edit_backend_blog_entry_path(@blog_entry)
      end
    else
      flash.now[:alert] = "Der Blogeintrag konnte nicht gespeichert werden."
      render :edit
    end
  end

  def destroy
    entry = BlogEntry.find params[:id]
    entry.destroy if entry

    if entry.destroyed?
      flash[:success] = "Der Blogeintrag wurde erfolgreich gelöscht."
    else
      flash[:alert] = "Beim Löschen des Blogeintrages ist ein Fehler aufgetreten."
    end

    redirect_to backend_blog_entries_path
  end

  def toggle_published
    @blog_entry = BlogEntry.find(params[:id])
    is_published = @blog_entry.published
    @blog_entry.published = !is_published

    if @blog_entry.update_attributes(params[:blog_entry])
      flash[:notice] = "Der Blogeintrag ist #{ !is_published ? 'veröffentlicht' : 'nicht mehr veröffentlich'}."
    end

    redirect_to backend_blog_entries_path
  end

  private

  def blog_entry_params
    params.require(:blog_entry).permit(:name, :date, :alias, :teaser_image, :teaser_text, :published)
  end

  def add_element_param
    params[:add_element]
  end

  def check_abilities
    authorize! :manage, BlogEntry
  end

end
