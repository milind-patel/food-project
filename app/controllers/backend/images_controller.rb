class Backend::ImagesController < Backend::BaseController
  before_filter :event

  # def index
  #   @images = current_imaged.images
  # end

  # def show
  #   @image = current_imaged.images.find(params[:id])
  # end

  def new
    @image = Image.new
  end

  def create
    images = params.require(:event).permit(images: [:file])
    images[:images].each do |file|
      if file[:file].present?
        image = Image.new(imaged: event, file: file[:file])
        image.save
      end
    end
    redirect_to backend_event_path(event)
  end

  #
  # def create
  #   @image = Image.new(imaged: current_imaged, file: image_params[:file].first)
  #
  #   if @image.save
  #     respond_to do |format|
  #       format.json {render :json => json_response(@image)}
  #       format.html {redirect_to action: :index}
  #     end
  #   else
  #     respond_to do |format|
  #       format.json {render :json => json_error_response(@image)}
  #       format.html {render :new}
  #     end
  #   end
  # end
  #

  # def update
  #   image = event.images.find(params[:id])
  #   # event.
  #   redirect_to backend_event_path(event)
  # end

  def destroy
    event.images.find(params[:id]).destroy
    redirect_to backend_event_path(event)
  end

  private

  def event
    @event ||= ::Event.find(params[:event_id])
  end

  def json_response(image)
    {
      files:[{
        url: image.file.url,
        thumbnail_url: image.file.url(:upload_thumb_square),
        name: image.file_file_name,
        type: image.file_content_type,
        size: image.file_file_size,
        delete_url: internal_image_path(image),
        delete_type: 'DELETE',
      }]
    }
  end

  def json_error_response(image)
    {
      files:[{
        name: image.file_file_name,
        size: image.file_file_size,
        error: image.errors.full_messages.to_sentence
      }]
    }
  end
end
