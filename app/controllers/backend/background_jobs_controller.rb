class Backend::BackgroundJobsController < Backend::BaseController
  before_filter :check_abilities

  def index; end

  private

  def check_abilities
    authorize! :view, :background_jobs
  end

end