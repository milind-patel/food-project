class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session

  before_filter :fill_view_config
  before_filter :authenticate

  # returning a basic view config instance
  def view_config
    @view_config ||= ViewConfig.new
  end
  helper_method :view_config

  def after_sign_in_path_for(resource)
    location = stored_location_for(resource)

    if location.blank? || location == root_path
      if resource.class == BackendUser
        location = backend_dashboard_path
      elsif resource.class == User
        location = internal_user_path
      end
    end

    location
  end

  def newsletter_cookie_service
    @newsletter_cookie_service ||= NewsletterCookieService.new(cookies)
  end

  protected

  def authenticate
    if Rails.env.staging?
      authenticate_or_request_with_http_basic do |username, password|
        username == "wwl" && password == "foodoo"
      end
    end
  end

  private

  def fill_view_config
    view_config.page_title = I18n::t('public.base.page_title')
    view_config.page_meta_description = I18n::t('public.base.page_meta_description')
    view_config.page_meta_robots = 'index, follow'
    view_config.controller_name = self.class.name.gsub('Controller','').parameterize
    view_config.action_name = self.action_name
  end

end
