class ErrorsController < ApplicationController
  layout "public"

  def error_404
    view_config.page_title = I18n::t('public.errors.error_404.page_title')
    @not_found_path = params[:not_found]
  end

  def oauth_failure
    view_config.page_title = I18n::t('public.errors.oauth_failure.page_title')
  end

end