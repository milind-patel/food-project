class Internal::ImagesController < Internal::BaseController
  skip_before_filter :check_profile_completion, only: [:new, :create]

  def index
    @images = current_imaged.images
  end

  def show
    @image = current_imaged.images.find(params[:id])
  end

  def new
    @image = Image.new

    if params[:modal].present?
      render :new, layout: false
    end
  end

  def create
    @image = Image.new(imaged: current_imaged, file: image_params[:file].first)

    if @image.save
      respond_to do |format|
        format.json {render :json => json_response(@image)}
        format.html {redirect_to action: :index}
      end
    else
      respond_to do |format|
        format.json {render :json => json_error_response(@image)}
        format.html {render :new}
      end
    end
  end

  def destroy
    current_imaged.images.find(params[:id]).destroy

    respond_to do |format|
      format.json {render :json => {success: true}}
      format.html {redirect_to action: :index}
    end
  end

  private

  def current_imaged
    params[:event_id].present? ? Event.find(params[:event_id]) : current_user
  end

  def image_params
    params.require(:image).permit(
      :file => [],
    )
  end

  def json_response(image)
    {
      files:[{
        url: image.file.url,
        thumbnail_url: image.file.url(:upload_thumb_square),
        name: image.file_file_name,
        type: image.file_content_type,
        size: image.file_file_size,
        delete_url: internal_image_path(image),
        delete_type: 'DELETE',
      }]
    }
  end

  def json_error_response(image)
    {
      files:[{
        name: image.file_file_name,
        size: image.file_file_size,
        error: image.errors.full_messages.to_sentence
      }]
    }
  end
end
