class Internal::EventsController < Internal::BaseController
  def new
    @event = current_event.decorate

    view_config.page_title = t('internal.event.create.page_title')
  end

  def create
    @event = current_event.decorate

    @event.update_attributes(event_params)

    if @event.save
      @event.next

      if @event.created?
        @event.submit
        session[:current_event] = nil
        redirect_to(action: :successfully_created, id: @event)
      else
        render :new
      end
    else
      render :new
    end
  end

  def back
    current_event.back

    redirect_to action: :new
  end

  def gallery
    @event = Event.find_by_id(params[:id]) || current_event
    @event = @event.decorate

    render layout: false
  end

  def successfully_created
    @event_appointment = current_user.events.find(params[:id]).decorate.event_appointment
    view_config.page_title = I18n::t('internal.event.created.page_title')
  rescue ActiveRecord::RecordNotFound
    redirect_to root_path
  end

  private

  def current_event
    @current_event ||= begin
      if session[:current_event].present?
        begin
          @current_event = current_user.events.find(session[:current_event].to_i)
        rescue ActiveRecord::RecordNotFound
          @current_event = Event.create(owner: current_user)
        end
      else
        @current_event = Event.create(owner: current_user)
      end

      session[:current_event] = @current_event.id

      @current_event
    end
  end

  def event_informations_params
    params.require(:event_event_informations).permit(
      event_information_ids: []
    )
  end

  def event_params
    params.require(:event).permit(
      :title,
      :description,
      :courses_count,
      :max_f_guest_count,
      :max_guest_count,
      :max_m_guest_count,
      :min_m_guest_count,
      :min_guest_count,
      :min_f_guest_count,
      :float_price,
      :category_id,
      :duration,
      :start_time,
      :repetition,
      :address_city,
      :address_zip,
      :address_street,
      :address_district,
      :address_addition,
      :ring_at,
      :floor,
      :sex_direction,
      :agb_confirmation,
      :debit_authorization,
      :self_participation,
      :person_name,
      :phone,
      :latitude,
      :longitude,
      :participation_until_hours,
      :own_max_guest_count,
      open_ends: [],
      drink_ids: [],
      event_information_ids: [],
      event_appointment: [
        :start_time_time,
        :start_time_date,
      ],
      event_appointments_attributes: [
        :start_time_time,
        :start_time_date,
        :id,
        :_destroy,
      ]
    )
  end
end
