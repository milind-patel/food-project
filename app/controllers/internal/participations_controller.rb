# encoding: utf-8
class Internal::ParticipationsController < Internal::BaseController
  before_filter :load_event_appointment
  before_filter :load_participation, except: [:show]
  before_filter :check_if_owner

  def new
    if params[:as_modal].present? && params[:as_modal] == 'true'
      render 'new_modal', layout: false
    else
      render
    end
  end

  def create
    @participation.attributes = create_params(params)

    if @participation.save
      ParticipationMailingWorker.perform_async(@participation.id, :create)
      redirect_to internal_event_appointment_participation_path(@event_appointment)
    else
      render :new
    end
  end

  def show
    view_config.page_title = I18n::t('public.participations.show.page_title')
    @participation = @event_appointment.participations.where(user: current_user).active_ones.first
  end

  def destroy
    unless @event_appointment.past?
      ParticipationMailingWorker.perform_async(@participation.id, :decline)
      @participation.cancel
    end

    redirect_to :back
  end


  private

  def load_event_appointment
    @event_appointment = EventAppointment.find(params[:event_appointment_id])
    @event_appointment.event = @event_appointment.event.decorate
    @event_appointment = @event_appointment.decorate
  end

  def load_participation
    @participation = Participation.find_or_initialize_by(user_id: current_user.id, event_appointment_id: @event_appointment.id, state: :accepted)
  end

  def create_params(params)
    if @event_appointment.event.dating?
      params.require(:participation).permit(:male_count, :female_count)
    else
      params.require(:participation).permit(:guests_count)
    end
  end

  def check_event_appointment_booked_up
    if @event_appointment.places_left <= 0
      redirect_to event_appointment_path(@event_appointment)
    end
  end

  def check_if_owner
    if @event_appointment.event.owner?(current_user)
      redirect_to event_appointment_path(@event_appointment)
    end
  end

end
