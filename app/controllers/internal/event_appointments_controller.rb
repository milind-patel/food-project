class Internal::EventAppointmentsController < Internal::BaseController
  before_filter :event_appointment
  before_filter :is_editable

  def edit
    @event = event_appointment.event.decorate
    @participations = event_appointment.participations
  end

  def update
    @event = event_appointment.event

    if @event.update_attributes(event_update_params)
      notify_guests_for_change(@event)
      redirect_to event_appointment_path(event_appointment)
    else
      render :edit
    end
  end

  def uninvite
    participation = @event_appointment.participations.find(params[:participation_id])

    unless @event_appointment.past?
      ParticipationMailingWorker.perform_async(participation.id, :uninvite)
      participation.uninvite
    end

    redirect_to :back
  end

  def not_appear
    participation = @event_appointment.participations.find(params[:participation_id])

    if mark_as_not_appeared_possible?
      participation.not_appear
    end

    redirect_to :back
  end

  def destroy
    unless event_appointment.past?
      if event_appointment.cancel
        EventAppointmentMailingWorker.perform_async(event_appointment.id, :canceled)
      end
    end

    redirect_to :back
  end

  private

  def event_appointment
    @event_appointment ||= begin
      event_appointment = EventAppointment.find(params[:id] || params[:event_appointment_id])

      if event_appointment.event.owner?(current_user)
        event_appointment
      else
        nil
      end
    end
  end

  def event_update_params
    params.require(:event).permit(
      :description
    )
  end

  def is_editable
    if event_appointment.closed?
      redirect_to event_appointment_path(@event_appointment)
    end
  end

  def notify_guests_for_change(event)
    event.event_appointments.map(&:participations).flatten.uniq.each do |participation|
      ParticipationMailingWorker.perform_async(participation.id, :edit)
    end
  rescue
    nil
  end
end
