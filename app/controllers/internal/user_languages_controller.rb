class Internal::UserLanguagesController < Internal::BaseController
  skip_before_filter :check_profile_completion

  def index
    @languages = Relation::UserLanguageDecorator.decorate_collection(current_user.decorate.languages)

    respond_to do |format|
      format.html { render layout: false }
    end
  end

  def new
    @language = Relation::UserLanguage.new
    @iso_639_1_options = create_iso_639_1_options

    respond_to do |format|
      format.html { render layout: false }
    end
  end

  def create
    language = Language.find_by_iso_639_1(params[:relation_user_language][:iso_639_1])
    if language
      current_user.languages.build(language: language, strength: 0)
      current_user.save
    end

    respond_to do |format|
      format.json { render json: { status: :ok } ,status: :ok }
    end
  end

  def update_all
    attributes = params[:user] && params[:user][:languages_attributes]

    if attributes.present?
      current_user.languages_attributes = attributes

      current_user.save!
    end

    respond_to do |format|
      format.json { render json: { status: :ok } ,status: :ok }
    end
  end

  def destroy
    language = current_user.languages.find(params[:id])
    language.destroy if language

    respond_to do |format|
      format.json { render json: { status: :ok } ,status: :ok }
    end
  end


  private

  def create_iso_639_1_options
    current_language_ids = current_user.languages.map(&:language_id)

    result = Language.all.inject({}) do |result, language|
      unless current_language_ids.include?(language.id)
        result[I18n.t(language.iso_639_1, scope: :languages)] = language.iso_639_1
      end
      result
    end

    result.sort_by { |k,v| k.to_s }
  end

end
