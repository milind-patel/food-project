class Internal::BaseController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_profile_completion

  layout 'public'

  protected

  def check_profile_completion
    unless current_user.profile_completed?
      flash.keep
      flash.alert = I18n::t('public.flash.profile_incomplete')

      redirect_to edit_internal_user_path
    end
  end

end
