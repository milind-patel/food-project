class Internal::UsersController < Internal::BaseController
  skip_before_filter :check_profile_completion, only: [:edit, :update, :gallery]
  before_filter :user

  def show
    view_config.page_title = t('internal.profile.show.page_title', name: @user.decorate.name)
    @user = @user.decorate
  end

  def edit
    return_back_path = params[:return_back]

    if return_back_path.present?
      store_location_for(current_user, return_back_path)
    end

    view_config.page_title = t('internal.profile.edit.page_title')
    @user.check_profile = true
    @user.valid?
    @user = @user.decorate
  end

  def update
    @user = @user.decorate
    @languages = Relation::UserLanguageDecorator.decorate_collection(user.languages)
    if user.update_attributes(user_params)
      user.check_newsletter_subscription
      user.check_profile = true

      if user.valid?
        location = stored_location_for(current_user)
        if location.present?
          redirect_to location
        else
          # redirect_to user
          redirect_to internal_user_path
        end
      else
        redirect_to edit_internal_user_path
      end
    else
      render :edit
    end
  end

  def update_password
    if user.update_with_password(edit_password_params)
      sign_in(user, bypass: true)
      redirect_to user
    else
      render :edit_password
    end
  end

  def gallery
    @user = user.decorate

    render layout: false
  end

  private

  def user
    @user ||= current_user
  end

  def edit_password_params
    params.require(:user).permit(
      :current_password,
      :password,
      :password_confirmation,
    )
  end

  def user_params
    params.require(:user).permit(
      :date_of_birth,
      :name,
      :job,
      :city,
      :phone,
      :relationship,
      :interests,
      :description,
      :main_image_id,
      :gender,
      :newsletter_subscription,
      :newsletter_city_id,
      languages_attributes: [
        :id,
        :strength,
        :language_id
      ]
    )
  end
end
