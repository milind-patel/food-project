class Internal::EventDataController < Internal::BaseController

  def show
    participated_events = current_user.decorate.participated_events
    event_appointment = participated_events.detect{|ea| ea.event.id.to_s == params[:event_id]}
    @event = event_appointment.event if event_appointment

    if @event.nil?
      redirect_to(errors_error_404_path(not_found: request.original_url))
    end

    render layout: false

  end
  
end