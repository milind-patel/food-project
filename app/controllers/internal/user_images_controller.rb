class Internal::UserImagesController < Internal::BaseController
  before_filter :images
  before_filter :main_image

  def index; end

  def create
    @image = Image.new(image_params)
    @image.imaged = current_user

    if @image.save
      redirect_to action: :index
    else
      render :index
    end
  end

  private

  def image_params
    params.require(:image).permit(
      :file
    )
  end

  def user
    @user ||= current_user
  end

  def images
    @images = user.images
  end

  def main_image
    @main_image ||= user.main_image
  end
end
