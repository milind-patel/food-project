# encoding: utf-8
class Internal::RatingsController < Internal::BaseController
  before_filter :load_data
  before_filter :check_if_rated, except: [:show]

  def show
    @rating = EventRating.find_by(event_appointment_id: @event_appointment.id, user_id: current_user.id)
  end

  def new
    @rating = EventRating.new
  end

  def create
    @rating = EventRating.new(params_for_create)
    @rating.user = current_user
    @rating.event_appointment = @event_appointment

    if @rating.save
      RatingMailingWorker.perform_async(@rating.id, :create)

      # TODO: reaggreagte, if user has only a few ratings

      redirect_to internal_event_appointment_rating_path(@event_appointment)
    else
      render :new
    end
  end


  private

  def params_for_create
    params.require(:event_rating).permit(
      :total,
      :meal,
      :atmosphere,
      :location,
      :price_performance_ratio,
      :message
    )
  end

  def load_data
    participation = current_user.participations.where(event_appointment_id: params[:event_appointment_id]).first
    @event_appointment = EventAppointmentDecorator.decorate(participation.event_appointment) if participation
    redirect_to errors_error_404_path and return if @event_appointment.nil?

    @event = EventDecorator.decorate(@event_appointment.event)
    @owner = UserDecorator.decorate(@event.owner)
    @price = (@event.price/100).to_i

    ratings = @event_appointment.decorate.event_ratings
    @total_rating = @event_appointment.calc_percentage(ratings, :total)

    @rating_collection = {
      "keine Angabe" => 0,
      "1" => 1,
      "2" => 2,
      "3" => 3,
      "4" => 4,
      "5" => 5
    }
  end

  def check_if_rated
    @rating = EventRating.find_by(event_appointment_id: @event_appointment.id, user_id: current_user.id)
    redirect_to internal_event_appointment_rating_path(@event_appointment) if @rating
  end

end