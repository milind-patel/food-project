class Public::PagesController < Public::BaseController

  def about_us
    view_config.page_title = I18n::t('public.about_us.page_title')
  end

  def agb
    view_config.page_title = I18n::t('public.agb.page_title')
  end

  def contact
    view_config.page_title = I18n::t('public.contact.page_title')
  end

  def how_it_works
    view_config.page_title = I18n::t('public.how_it_works.page_title')
    view_config.page_meta_description = I18n::t('public.how_it_works.page_meta_description')
  end

  def imprint
    view_config.page_title = I18n::t('public.imprint.page_title')
  end

  def templates

  end

  def faq
    view_config.page_title = I18n::t('public.faq.page_title')
  end

end