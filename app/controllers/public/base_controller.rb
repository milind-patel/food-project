class Public::BaseController < ApplicationController
  layout 'public'

  def newsletter_service
    @newsletter_service ||= NewsletterService.new(cookies, session, current_user)
  end
  helper_method :newsletter_service

end
