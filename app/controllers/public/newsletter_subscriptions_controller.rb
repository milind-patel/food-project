class Public::NewsletterSubscriptionsController < Public::BaseController

  def create
    @newsletter_subscription = NewsletterSubscription.new(newsletter_subscription_params)

    if @newsletter_subscription.save
      newsletter_cookie_service.set_cookie(@newsletter_subscription.email, true)

      render json: { success: true }, status: :ok
    else
      render json: {
          success: false,
          errors: @newsletter_subscription.errors
        }, status: :ok
    end
  end

  def success
    render layout: false
  end

  def update
    @email = newsletter_subscription_params['email']
    subscription = NewsletterSubscription.find_by(email: @email)

    subscription && subscription.deactivate
  end


  private

  def newsletter_subscription_params
    params.require(:newsletter_subscription).permit(
      :city_id,
      :name,
      :email
    )
  end

end
