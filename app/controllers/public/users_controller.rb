class Public::UsersController < Public::BaseController
  
  def show
    @user = User.find(params[:id]).decorate
    view_config.page_title = t('internal.profile.show.page_title', name: @user.name)
  end

end
