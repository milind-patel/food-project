class Public::EventAppointmentsController < Public::BaseController

  # index filters
  before_filter :index_load_search_request
  before_filter :index_load_categories
  before_filter :index_load_cities

  # show filters
  before_filter :show_filter, only: [:show]

  # actions

  def index
    view_config.page_title = t('public.event_appointments.index.page_title')
    @event_appointments = @search_request.filtered_appointments
    service = EventListViewArrayService.new(self, @event_appointments, params[:page], Settings.view_events_per_page)
    service_view = NewsletterViewCheckService.new(current_user, newsletter_cookie_service)
    @view_array = service.build(service_view.show?)

    # newsletter data
    @newsletter_subscription = NewsletterSubscription.new
    @newsletter_subscription.city = City.first
    @newsletter_cities = City.all

    # meta description switch
    index_meta_tags

    # response handling
    respond_to do |format|
      format.html { render "index_filtered" }
      format.json do
        render json: {
          html: build_event_appointments_json(@view_array),
          more_elements: @search_request.more_appointments?,
        }
      end
    end
  end

  def show
    if @event_appointment.event.state == "waiting_for_approval"
      flash.now[:alert] = t('public.event_appointments.show.flash.not_approved').html_safe
    end

    if @event_appointment.event.state == "declined"
      flash.now[:error] = t('public.event_appointments.show.flash.declined')
    end

    if !flash.now[:error].present? && @event_appointment.canceled?
      if @event_appointment.event.owner == current_user
        flash.now[:error] = t('public.event_appointments.show.flash.canceled')
      else
        flash.now[:error] = t('public.event_appointments.show.flash.canceled_user')
      end
    elsif !flash.now[:error].present? && @event_appointment.closed?
      flash.now[:error] = t('public.event_appointments.show.flash.closed')
    end

    @event = @event_appointment.event.try(:decorate)
    @owner = @event_appointment.event.try(:owner).try(:decorate)

    @description = ActionView::Base.full_sanitizer.sanitize(@event.description)
    @price = (@event.price/100).to_i

    @event_infos = ::EventInformation.all

    view_config.page_title = t('public.event_appointments.show.page_title', name: @event.title)

    respond_to do |format|
      format.html
      format.json do
        render json: @event_appointments
      end
    end
  end



  private

  # -------------------------------------------
  # index methods

  def index_load_search_request
    page = params[:page]
    @search_request = SearchRequest.new(params[:city], params[:category], params[:date])
    @search_request.page(page)
    @search_request.per(Settings.view_events_per_page)

    if params[:city].present? || params[:category].present? || params[:date].present?
      view_config.page_meta_canonical = root_url
    end
  end

  def index_load_categories
    @categories = categories_with_prompt
  end

  def index_load_cities
    @cities = cities_with_prompt
  end

  def categories_with_prompt
    {t("public.categories.default_value") => nil}.merge(
      Category.all.inject({}) do |r, c|
        r[t("public.categories.#{c.name}")] = c.name
        r
      end
    )
  end

  def cities_with_prompt
    City.all.inject({}) do |r, c|
      r[c.name] = c.name.downcase
      r
    end
  end

  def build_event_appointments_json(view_array)
    render_to_string(
      'public/event_appointments/index_filtered_ajax',
      locals: {
        view_array: view_array
      },
      formats: :html,
      layout: false
    )
  end

  def index_meta_tags
    case @search_request.category
      when 'people'
        view_config.page_title = I18n::t('public.base.title.index_people')
        view_config.page_meta_description = I18n::t('public.base.meta_description.index_people')
      when 'dating'
        view_config.page_title = I18n::t('public.base.title.index_dating')
        view_config.page_meta_description = I18n::t('public.base.meta_description.index_dating')
      when 'course'
        view_config.page_title = I18n::t('public.base.title.index_course')
        view_config.page_meta_description = I18n::t('public.base.meta_description.index_course')
      else
        view_config.page_title = I18n::t('public.base.title.index')
        view_config.page_meta_description = I18n::t('public.base.meta_description.index')
    end
  end

  # -------------------------------------------
  # show methods

  def show_filter
    @event_appointment = ::EventAppointment.find_by_id(params[:id]).try(:decorate)
    exists = @event_appointment.nil? || @event_appointment.event.nil? ? false : true

    return if exists && @event_appointment.event.approved?
    return if exists && user_signed_in? && @event_appointment.event.owner.id == current_user.id
    return if exists && backend_user_signed_in? && current_backend_user.cancan?(:manage, Event)

    redirect_to(errors_error_404_path(not_found: request.original_url))
  end

end
