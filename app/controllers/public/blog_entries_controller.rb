class Public::BlogEntriesController < Public::BaseController

  def index
    view_config.page_title = t('public.blog.page_title')
    view_config.page_meta_description = I18n::t('public.blog.page_meta_description')

    # preparation
    @blog_entries_count = BlogEntry.published.count
    per = 5
    page = params[:page].to_i || 1
    page = 1 if page == 0
    @more_entries = per*page < @blog_entries_count ? true : false

    # load entries
    @blog_entries = BlogEntry.published.order("date desc").page(page).per(per)

    # response
    respond_to do |format|
      format.html
      format.json { render json: {
        more_entries: @more_entries,
        entries: build_blog_entries_for_json(@blog_entries)
      }, status: :ok }
    end
  end

  def show
    @blog_entry = BlogEntry.blog_entry_from_params(params)
    view_config.page_title = t(:'public.blog.show.page_title', name: @blog_entry.try(:name))

    if @blog_entry.nil?
      redirect_to blog_entries_path
    end
  end


  private

  def build_blog_entries_for_json(blog_entries)
    render_to_string(
      'public/blog_entries/index_ajax',
      locals: {
        blog_entries: blog_entries
      },
      formats: :html,
      layout: false
    )
  end

end
