class Auth::RegistrationsController < Devise::RegistrationsController
  before_action :set_meta_tags

  layout 'public'

  def new
    build_resource({})
    respond_with self.resource do |format|
      format.html do
        if params[:modal] && params[:modal].eql?('true')
          render 'new_async', layout: false, status: :ok
        else
          render
        end
      end
    end
  end

  protected

  def after_sign_up_path_for(resource)
    internal_user_path
  end

  def set_meta_tags
    view_config.page_meta_robots = 'noindex, follow'
  end
end
