class Auth::SessionsController < Devise::SessionsController
  before_action :set_meta_tags

  layout 'public'

  def new
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)

    if params[:return_back].present?
      store_location_for(resource_class, params[:return_back])
    end

    respond_with(resource, serialize_options(resource)) do |format|
      format.html do

        if params[:modal] && params[:modal].eql?('true')
          render 'new_async', layout: false, status: :ok
        else
          render
        end
      end
    end
  end


  protected

  def set_meta_tags
    view_config.page_meta_robots = 'noindex, follow'
  end

end
