class Auth::PasswordsController < Devise::PasswordsController
  before_action :set_meta_tags

  layout 'public'

  protected

  def set_meta_tags
    view_config.page_meta_robots = 'noindex, follow'
  end

end