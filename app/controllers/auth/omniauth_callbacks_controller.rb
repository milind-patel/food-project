class Auth::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  before_action :set_meta_tags

  def facebook
    # check for necessary data
    if fb_data_complete?(request.env['omniauth.auth']) == false
      redirect_to(errors_oauth_failure_path) and return
    end

    # get the user
    @user = User.find_for_facebook_oauth(request.env['omniauth.auth'])

    # sign_in or/and sign_up
    if @user.present? && @user.persisted?
      @user.confirm!
      sign_in_and_redirect @user, event: :authentication
      set_flash_message(:notice, :success, kind: 'Facebook') if is_navigational_format?
    else
      flash[:alert] = I18n::t('devise.failure.omniauth_account_exists')
      # session['devise.facebook_data'] = nil# = request.env['omniauth.auth']
      redirect_to new_user_registration_url
    end
  end


  protected

  def set_meta_tags
    view_config.page_meta_robots = 'noindex, follow'
  end

  def fb_data_complete?(auth_data)
    return false if auth_data.uid.blank?
    return false if auth_data.info.email.blank?
    true
  rescue
    false
  end

end
