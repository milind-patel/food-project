class Auth::ConfirmationsController < Devise::ConfirmationsController
  before_action :set_meta_tags

  layout 'public'

  protected

  def after_confirmation_path_for(resource_name, resource)
    internal_user_path
  end

  def set_meta_tags
    view_config.page_meta_robots = 'noindex, follow'
  end
end
