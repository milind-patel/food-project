class Relation::UserLanguageDecorator < Draper::Decorator
  delegate_all

  def options_collection
    @options_collection ||= object.available_strengths.inject({}) do |result, strength|
      result[I18n::t("activerecord.attributes.relation.user_language.strengths.#{strength}")] = strength
      result
    end
  end

  def localized_title
    I18n::t(object.language.iso_639_1, scope: :languages)
  end

end