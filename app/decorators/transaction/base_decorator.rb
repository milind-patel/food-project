class Transaction::BaseDecorator < Draper::Decorator
  delegate_all

  def date
    object.updated_at.strftime("%d.%m.%Y")
  end

  def event_title
    if event.nil?
      '-'
    else
      helpers.truncate(event.title)
    end
  end

  def event
    event_appointment && event_appointment.event
  end

  def value_with_currency
    helpers.number_to_currency(value.to_f / 100)
  end

  def inverse_value_with_currency
    helpers.number_to_currency(-1 * float_value)
  end

  def value_status
    value >= 0 ? 'positive' : 'negative'
  end
end
