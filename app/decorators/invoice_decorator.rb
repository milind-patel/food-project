class InvoiceDecorator < Draper::Decorator
  delegate_all

  def salutation
    object.salutation && I18n::t("public.base.#{object.salutation}")
  end

  def date
    object.updated_at.strftime("%d.%m.%Y")
  end

  def date_plus_one
    (object.date+1.day).strftime('%d.%m.%Y')
  end

  def transactions
    object.transactions.decorate
  end

  def payments
    object.payments.decorate
  end

  def user
    object.user.decorate
  end

  def amount_with_currency
    value = as_float(amount)
    helpers.number_to_currency(value)
  end

  def remaining_amount_with_currency
    value = as_float(remaining_amount)
    helpers.number_to_currency(value)
  end

  def payment_status
    payed? ? 'payed' : 'unpayed'
  end

  # invoice pdf methods
  def inverse_amount_after_tax_currency
    as_currency(inverse_amount_after_tax)
  end

  def inverse_amount_after_tax
    -1 * after_tax_amount_as_float
  end

  def inverse_amount_before_tax_currency
    as_currency(inverse_amount_before_tax)
  end

  def inverse_amount_before_tax
    -1 * before_tax_amount_as_float
  end

  def inverse_tax_of_amount_currency
    as_currency(inverse_tax_of_amount)
  end

  def inverse_tax_of_amount
    -1 * tax_of_amount_as_float
  end

  def amount_after_tax_currency
    as_currency(after_tax_amount_as_float)
  end

  def amount_before_tax_currency
    as_currency(before_tax_amount_as_float)
  end

  def tax_of_amount_as_float
    (after_tax_amount_as_float - before_tax_amount_as_float).round(2)
  end

  def after_tax_amount_as_float
    as_float(amount)
  end

  def before_tax_amount_as_float
    as_float(before_tax_amount)
  end

  def tax_of_amount_currency
    as_currency(tax_of_amount_as_float)
  end

  def tax
    "#{object.tax * 100}%"
  end

  private

  def as_currency(value)
    helpers.number_to_currency(value)
  end

  def as_float(value)
    (value.to_f / 100).round(2)
  end
end
