class CategoryDecorator < Draper::Decorator
  delegate_all

  def title
     I18n.t("simple_form.options.event.category.#{name}")
  end
end
