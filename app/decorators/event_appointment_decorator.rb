# encoding: utf-8
class EventAppointmentDecorator < Draper::Decorator
  delegate_all

  def calc_percentage(array, key)
    sum = array.map(&key).inject{ |sum,x| sum + x }

    if array.count < 0 || sum.nil?
      return 0.0
    end

    sum.to_f / array.count.to_f / 5.0
  end

  def formatted_start_time_with_month
    l(start_time.to_time.in_time_zone, :format => '%d. %B %Y')
  end

  def formatted_start_time_with_leading_month
    l(start_time.to_time.in_time_zone, :format => '%A den %d. %B %Y')
  end

  def formatted_start_month
    l(start_time.to_time.in_time_zone, :format => '%b')
  end

  def formatted_start_day
    l(start_time.to_time.in_time_zone, :format => '%d')
  end

  def formatted_start_time_weekday
    l(start_time.to_time.in_time_zone, :format => '%A')
  end

  def formatted_start_time_english
    l(start_time.to_time.in_time_zone, :format => '%Y-%m-%d')
  end

  def formatted_start_time_german
    l(start_time.to_time.in_time_zone, :format => '%d.%m.%Y')
  end

  def formatted_start_time
    l(start_time.to_time.in_time_zone, :format => '%H:%M')
  end

  def start_time_hours
    l(start_time.to_time.in_time_zone, :format => '%H')
  end

  def start_time_minutes
    l(start_time.to_time.in_time_zone, :format => '%M')
  end

  def is_today?
    start_time.to_date == Date.today
  end

  def is_tomorrow?
    start_time.to_date == Date.tomorrow
  end

  def headline_date
    if is_today?
      I18n.t('public.base.today')
    elsif is_tomorrow?
      I18n.t('public.base.tomorrow')
    else
      formatted_start_time_with_leading_month
    end
  end

  def dating?
    object.event.dating?
  end

  def booked_places
    object.event.max_guest_count - object.places_left
  end

  # participation view

  def free_guests_options
    result = { }
    (object.places_left).times do |count|
      price = (object.event.float_price) * (count + 1)
      result[I18n.t('public.event_appointments.select.guests', count: (count+1), price: h.number_to_currency(price))] = count
    end
    result
  end

  def free_male_options
    result = { }
    result[I18n.t('public.event_appointments.select.guests_male', count: 0, price: 0)] = 0 unless helpers.current_user.male?
    male_count = object.places_left_male
    (male_count).times do |count|
      price = (object.event.float_price) * (count + 1)
      value = helpers.current_user.male? ? count : count + 1
      result[I18n.t('public.event_appointments.select.guests_male', count: (count+1), price: h.number_to_currency(price))] = value
    end
    result
  end

  def free_female_options
    result = { }
    result[I18n.t('public.event_appointments.select.guests_female', count: 0, price: 0)] = 0 unless helpers.current_user.female?
    female_count = object.places_left_female
    (female_count).times do |count|
      price = (object.event.float_price) * (count + 1)
      value = helpers.current_user.female? ? count : count + 1
      result[I18n.t('public.event_appointments.select.guests_female', count: (count+1), price: h.number_to_currency(price))] = value
    end
    result
  end

  def sorted_guests_index
    result = sorted_guests
    result ||= []
    guest_count = object.event.max_guest_count || 0

    if result.count <= 7
      result = result.slice(0,6)

      while result.count <= 7 && result.count < guest_count
        result << {
          type: :free
        }
      end
    end

    if result.count > 7
      result = result.slice(0,6)
      result << {
        type: :more
      }
    end

    result
  end

  def sorted_guests
    result = []
    result_guests = []
    object.participations.active_ones.each do |participation|
      image = participation.user.main_image

      if image.present?
        image_url = image.file.url(:image_thumb_small)
      else
        image_url = nil
      end

      result << {
        type: :person,
        image_url: image_url,
        id: participation.user.id,
        name: participation.user.name
      }
      count = participation.male_count.to_i + participation.female_count.to_i
      count = participation.guests_count.to_i if count <= 0
      count.times do
        result_guests << {
          type: :guest,
          image_url: participation.user.main_image.try(:file).try(:url, :image_thumb_small),
          name: "Gast von #{participation.user.name}"
        }
      end
    end

    result + result_guests

  end

  def participation_until
    participation_until_date.to_i
  end

  def participation_until_countdown
    difference = participation_until - Time.now.to_i
    components = []

    if difference > 0
      components << (difference / (24 * 60 * 60)).to_i
      components << ((difference / (60 * 60)) % 24).to_i
      components << ((difference / 60) % 60).to_i
      components << (difference % 60).to_i
    else
      components = [0,0,0,0]
    end

    "%d Tage %02d : %02d : %02d" % components
  end

  def participation_until_date
    object.participation_until_date
  end
end
