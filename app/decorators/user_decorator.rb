class UserDecorator < Draper::Decorator
  delegate_all

  def account_balance
    object.account_balance.decorate
  end

  def invoices
    object.invoices.decorate
  end

  def title
    if object.name.present?
      title = "#{object.name}"
    else
      title = object.email
    end

    title << " (#{age})" if age.present?

    title
  end

  def truncated_title
    if object.name.present?
      title = "#{object.name}"
    else
      title = object.email
    end

    title = title.truncate(16)
    title << " (#{age})" if age.present?

    title
  end

  def languages
    if @decorated_languages.nil?
      @decorated_languages = Relation::UserLanguageDecorator.decorate_collection(object.languages)
      @decorated_languages = @decorated_languages.sort{ |a,b| [a.localized_title] <=> [b.localized_title] }
    end
    @decorated_languages
  end

  def has_events?
    participated_events.count > 0 || organized_events.count > 0
  end

  def organized_events
    r = []
    object.events.each do |e|
      e.event_appointments.uncanceled.each do |ea|
        r << ea
      end
    end
    r.sort{ |e, f| [e.start_time, e.event.title] <=> [f.start_time, f.event.title] }
  end

  def participated_events
    r = []
    object.participations.active_ones.each do |p|
      r << p.event_appointment
    end
    r.sort{ |e, f| [e.start_time, e.event.title] <=> [f.start_time, f.event.title] }
  end

  def upcoming_organized_events
    organized_events.select { |e| e.start_time >= Time.now unless e.nil? }
  end

  def upcoming_participated_events
    participated_events.select { |e| e.start_time >= Time.now unless e.nil? }
  end

  def past_organized_events
    organized_events.select { |e| e.start_time < Time.now unless e.nil? }
  end

  def past_participated_events
    participated_events.select { |e| e.start_time < Time.now unless e.nil? }
  end

  def total_ratings_count
    object.aggregated_ratings_count
  end

  def ratings_description
    ratings_count = object.decorate.total_ratings_count
    if ratings_count == 0
      text = ""
    else
      text = ratings_count == 1? "(1 Bewertung)" : "(#{ratings_count} Bewertungen)"
    end
    text
  end

  def main_image_with_style(style)
    if main_image.present?
      main_image.file.url(style)
    else
      nil
    end
  end

  def profile_image
    main_image_with_style(:show_large)
  end

  def profile_image_edit
    main_image_with_style(:edit_large)
  end

  def profile_image_thumb_small
    main_image_with_style(:image_thumb_small)
  end

  def profile_image_thumb_medium
    main_image_with_style(:image_thumb_medium)
  end

  def newsletter_city_id
    object.newsletter_city_id || City.first.id
  end

  def cities
    @user_newsletter_cities ||= ::City.all.map do |o|
      n = o.name.parameterize
      title = I18n::t("simple_form.labels.user.newsletter_cities_button_titles.#{n}")
      content = "<i class='info-icon icon-#{n}'></i>#{o.name}".html_safe

      [
        content,
        o.id,
      ]
    end
  end

  def relationship
    object.relationship || 'not_specified'
  end

  def possible_relationships
    [
      [I18n::t('internal.profile.show.relationships.not_specified'), 'not_specified'],
      [I18n::t('internal.profile.show.relationships.single'), 'single'],
      [I18n::t('internal.profile.show.relationships.in_an_open_relationship'), 'in_an_open_relationship'],
      [I18n::t('internal.profile.show.relationships.in_a_relationship'), 'in_a_relationship'],
      [I18n::t('internal.profile.show.relationships.married'), 'married'],
    ]
  end

  def age
    if date_of_birth.present?
      now = Time.now.utc.to_date
      (now.year - date_of_birth.year - (date_of_birth.to_date.change(:year => now.year) > now ? 1 : 0)).to_s
    else
      ''
    end
  end

  def city
    object.city || ''
  end

  def language_names
    languages.map{ |language| I18n.t(language.iso_639_1, scope: :languages) }.join(', ')
  end

  def genders
    object.class.genders.map do |g|
      [I18n::t("simple_form.options.user.gender.#{g}"), g]
    end
  end

  def full_error_messages
    errors = []
    object.errors.each do |key, error_string|
      error = ''
      error << I18n::t("activerecord.attributes.user.#{key.to_s}") if key != :main_image
      error << " #{error_string}"
      errors << error
    end
    errors.join('<br/>').html_safe
  end
end
