class AccountBalanceDecorator < Draper::Decorator
  delegate_all

  def balance_with_currency
    helpers.number_to_currency(balance.to_f / 100)
  end

  def balanced?
    object.balance > 0
  end

  def user
    object.user.decorate
  end
end
