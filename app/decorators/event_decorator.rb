class EventDecorator < Draper::Decorator
  delegate_all

  def price_with_currency
    helpers.number_to_currency(price.to_f / 100)
  end

  def formatted_duration
    return if !object.duration.present?

    if object.duration == -1
      "Open end"
    else
      if object.duration == 1
        "1 Stunde"
      else
        "#{object.duration} Stunden"
      end
    end
  end

  def full_city_name
    "#{object.address_city}, #{object.address_district}"
  end

  def full_address
    "#{object.address_street}, #{object.address_zip} #{object.address_city}"
  end

  def truncated_full_city_name
    object.decorate.full_city_name.truncate(25)
  end

  def latitude
    object.latitude || 52.525013
  end

  def longitude
    object.longitude || 13.369494
  end

  def owner
    object.owner.decorate
  end

  def category
    object.category.present? ? object.category.decorate : nil
  end

  def truncated_title
    object.title.try(:truncate, 60)
  end

  def truncated_description
    object.description.try(:truncate, 70, seperator: ' ')
  end

  def html_description
    h.simple_format(object.description)
  end

  def event_appointment
    @event_appointment ||= (object.event_appointment || EventAppointment.new)

    @event_appointment.decorate
  end

  def main_image_path
    if main_image.present?
      main_image.file.url(:index_large)
    else
      nil
    end
  end

  def main_image
    @main_image ||= object.images.first
  end

  def sex_directions
    object.class.sex_directions.map do |key|
      [
        button_text_for_sex_direction(key),
        key,
      ]
    end
  end

  def button_text_for_sex_direction(key)
    if key == "mf"
      '<i class="fa fa-male fa-lg"></i> <i class="fa fa-female fa-lg"></i>'.html_safe
    elsif key == "ff"
      '<i class="fa fa-female fa-lg"></i> <i class="fa fa-female fa-lg"></i>'.html_safe
    elsif key == "mm"
      '<i class="fa fa-male fa-lg"></i> <i class="fa fa-male fa-lg"></i>'.html_safe
    end
  end

  def self_participations
    [
      [I18n::t('simple_form.options.event.self_participation.check'), '1'],
      [I18n::t('simple_form.options.event.self_participation.uncheck'), '0'],
    ]
  end

  def durations
    durations = Array(1..4).map do |value|
      [value.to_s, value]
    end

    durations
  end

  def course_counts
    Array(1..7).map do |v|
      [v.to_s, v]
    end
  end

  def open_ends
    [['Open End', object.open_end]]
  end

  def max_guest_count_options
    (Array(1..10) << 15 << 20)
  end

  def max_guest_counts
    max_guest_count_options.map do |v|
      [v.to_s, v]
    end
  end

  def own_max_guest_count
    if max_guest_count_options.include?(object.max_guest_count)
      ''
    else
      object.max_guest_count
    end
  end

  def categories
    Category.all.map do |o|
      [
        I18n.t("simple_form.options.event.category.#{o.name}"),
        o.id,
      ]
    end
  end

  def informations
    @event_informations ||= ::EventInformation.all.map do |o|
      title = I18n::t("public.event_informations.button_titles.#{o.title}")
      content = "<i class='info-icon icon-#{o.title}'></i>#{title}".html_safe

      [
        content,
        o.id,
      ]
    end
  end

  def possible_drinks
    @possible_drinks ||= Drink.all.map do |drink|
      [
        I18n::t("activerecord.attributes.drink.titles.#{drink.name}"),
        drink.id,
      ]
    end
  end

  def drink_titles
    event.drinks.map do |drink|
      I18n::t("activerecord.attributes.drink.titles.#{drink.name}")
    end.join(', ')
  end

  def repetitions
    Hash['Täglich' => 'daily', 'Wöchentlich' => 'weekly', 'Monatlich' => 'monthly', 'Nein' => :not]
  end

  def participation_until_hours
    object.participation_until_hours.to_i
  end

  def participation_until_hours_steps
    [
      ['0 h', 0],
      ['1 h', 1],
      ['3 h', 3],
      ['6 h', 6],
      ['12 h', 12],
      ['24 h', 24],
      ['48 h', 48],
    ]
  end

  # dummies
  def date
    Date.today
  end

  def repetition_date
    nil
  end

  def bank_account_owner
    nil
  end

  def bank_account_institute
    nil
  end

  def bank_account_iban
    nil
  end

  def bank_account_bic
    nil
  end
end
