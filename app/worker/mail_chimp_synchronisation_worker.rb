class MailChimpSynchronisationWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options queue: :mail_chimp_synchronisation

  recurrence { hourly }

  def perform
    MailChimpConnectorService.new.synchronize
  end
end

