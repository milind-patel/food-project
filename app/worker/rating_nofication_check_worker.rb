class RatingNotificationCheckWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options queue: :rating_notification_check

  recurrence { hourly }

  def perform
    RatingNotificationCheckService.new.run
  end

end

