class EventAppointmentMailingWorker
  include Sidekiq::Worker

  sidekiq_options queue: :mailing

  def perform(id, type)
    event_appointment = EventAppointment.find(id)

    case type.to_sym
      when :canceled then EventMailer.event_appointment_canceled_mail(event_appointment).deliver!
    end
  end
end

