class ParticipationMailingWorker
  include Sidekiq::Worker

  sidekiq_options queue: :mailing


  # accepted types:
  #
  # :create     > new event participation
  # :uninvite   > user got uninvited from the event
  # :decline    > user canceld his participation
  # :edit       > owner edited event
  # :rating     > event rating is possible
  #
  def perform(id, type)
    participation = Participation.find(id)
    case type.to_sym
      when :create    then EventMailer.created_participation_mail(participation).deliver
      when :uninvite  then EventMailer.decline_participation_as_owner_mail(participation).deliver
      when :decline   then EventMailer.decline_participation_as_guest_mail(participation).deliver
      when :edit      then EventMailer.event_edited_mail(participation).deliver
      when :rating    then EventMailer.rate_event_mail(participation).deliver
    end
  end

end
