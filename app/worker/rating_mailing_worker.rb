class RatingMailingWorker
  include Sidekiq::Worker

  sidekiq_options queue: :mailing


  # accepted types:
  #
  # :create     > new event rating
  #
  def perform(id, type)
    rating = EventRating.find(id)
    case type.to_sym
      when :create    then EventMailer.rated_event_mail(rating).deliver
    end
  end

end