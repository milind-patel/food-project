class RatingAggregationWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options queue: :ratings_aggregation

  recurrence { hourly(2) }

  def perform
    User.reaggregate_ratings_all
  end

end