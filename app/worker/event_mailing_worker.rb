class EventMailingWorker
  include Sidekiq::Worker

  sidekiq_options queue: :mailing

  def perform(id, type)
    event = Event.find(id)
    case type.to_sym
      when :created then EventMailer.created_event_admin_notice(event).deliver
    end
  end
end

