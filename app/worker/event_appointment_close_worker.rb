class EventAppointmentCloseWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options queue: :event_appointment_close

  recurrence backfill: false do
    hourly.minute_of_hour(0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55)
  end

  def perform
    EventAppointment.closable.each do |event_appointment|
      event_appointment.close
    end
  end
end

