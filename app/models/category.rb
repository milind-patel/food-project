class Category < ActiveRecord::Base
  has_many :events

  validates_presence_of :name
  validates_uniqueness_of :name

  def dating?
    self.name == 'dating'
  end

  def course?
    self.name == 'course'
  end

  def people?
    self.name == 'people'
  end
end
