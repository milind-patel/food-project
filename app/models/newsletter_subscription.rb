#
# attributes
# city_id, user_id, email, name, active
class NewsletterSubscription < ActiveRecord::Base

  CITY_GROUP_NAME = 'city'
  REGISTERED_GROUP_NAME = 'registered'

  # relations
  belongs_to :city
  belongs_to :user # optional

  # validations
  validates_presence_of :city_id
  validates_presence_of :name
  validates_presence_of :email
  validates_uniqueness_of :email
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

  delegate :name, to: :city, prefix: true, allow_nil: true
  delegate :name, to: :user, prefix: true, allow_nil: true

  # methods
  def self.humanized_group_name_city
    CITY_GROUP_NAME.humanize
  end

  def self.humanized_group_name_registered
    REGISTERED_GROUP_NAME.humanize
  end

  def group_name_city
    CITY_GROUP_NAME
  end

  def group_name_registered
    REGISTERED_GROUP_NAME
  end

  def registered?
    user.present?
  end

  def name
    if user.present?
      user_name
    else
      read_attribute(:name)
    end
  end

  def deactivate
    write_attribute(:active, false)

    save!
  end

  def activate
    write_attribute(:active, true)

    save!
  end
end
