# attributes
# file, imaged, imaged_type, imaged_id

class Image < ActiveRecord::Base
  belongs_to :imaged, polymorphic: true

  has_attached_file :file, :styles => lambda { |f|
    styles = {}

    if f.instance.imaged.class == Event
      styles[:index_large] = '260x190#'
      styles[:show_large] = '400x292#'
      styles[:show_thumb_horizontal] = '120x79#'
      styles[:upload_thumb_square] = '120x120#'
    end

    if f.instance.imaged.class == User
      styles[:image_thumb_small] = '55x60#'
      styles[:image_thumb_medium] = '80x90#'
      styles[:show_large] = '304x391#'
      styles[:show_thumb_vertical] = '93x113#'
      styles[:edit_large] = '390x450#'
      styles[:edit_thumb_vertical] = '110x140#'
      styles[:upload_thumb_square] = '120x120#'
    end

    styles
  }

  validates_presence_of :imaged
  validates_attachment_presence :file
  validates_attachment_content_type :file, :content_type => /image/


end

