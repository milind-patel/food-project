class EventInformation < ActiveRecord::Base
  validates_presence_of :title

  has_many :event_event_informations, class_name: "Relation::EventEventInformation"
  has_many :events, through: :event_event_informations
end
