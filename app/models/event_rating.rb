#database attributes
# total, meal, atmosphere, location, price_performance_ratio, message
class EventRating < ActiveRecord::Base
  acts_as_paranoid

  # relations
  belongs_to :user
  belongs_to :event_appointment

  # validations
  validates_presence_of :user
  validates_presence_of :event_appointment
  validates :total, numericality: { only_integer: true, greater_than: -1, less_than: 6 }, allow_blank: true
  validates :meal, numericality: { only_integer: true, greater_than: -1, less_than: 6 }, allow_blank: true
  validates :atmosphere, numericality: { only_integer: true, greater_than: -1, less_than: 6 }, allow_blank: true
  validates :location, numericality: { only_integer: true, greater_than: -1, less_than: 6 }, allow_blank: true
  validates :price_performance_ratio, numericality: { only_integer: true, greater_than: -1, less_than: 6 }, allow_blank: true

  # callbacks
  after_initialize :nullify_values

  def nullify_values
    self.total = 0 if self.total.nil?
    self.meal = 0 if self.meal.nil?
    self.atmosphere = 0 if self.atmosphere.nil?
    self.location = 0 if self.location.nil?
    self.price_performance_ratio = 0 if self.price_performance_ratio.nil?
  end
end
