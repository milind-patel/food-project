class Drink < ActiveRecord::Base
  validates_uniqueness_of :name

  has_many :events
end
