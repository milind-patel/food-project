# database attributes
# start_time

# attributes
# events, start_time, notified_for_rating:integer
class EventAppointment < ActiveRecord::Base

  attr_accessor :start_time_time, :start_time_date

  # paranoia
  acts_as_paranoid

  # relations
  belongs_to :event
  has_many :participations
  has_many :event_ratings

  # validations
  validates_presence_of :start_time_date
  validates_presence_of :start_time_time

  # scopes
  scope :approved, -> { joins(:event).where('events.state = ?', 'approved') }

  scope :date_ordered, -> { order( start_time: :asc ) }
  scope :type_ordered, -> {
    # FIELD(event_appointments.state, 'created', 'closed'),
    order <<-EOS
      event_appointments.state DESC,
      CASE event_appointments.state WHEN 'created' THEN event_appointments.start_time END ASC,
      CASE WHEN event_appointments.state <> 'created' THEN event_appointments.start_time END DESC
    EOS
  }

  scope :uncanceled, -> {where('event_appointments.state != ?', 'canceled')}

  scope :active, -> { where(state: 'created') }

  scope :with_categories, lambda { |categories_array|
    ids = categories_array.map(&:id)
    joins(:event).where(events: { category_id: ids })
  }

  scope :in_city, lambda { | city |
    joins(:event).where('events.address_city = ?', city)
  }

  scope :later_or_equal, lambda { |time|
    where('start_time >= ?', time)
  }

  scope :closable, lambda {
    time = Time.now - Settings.event_appointment_close_time.hours
    where('start_time <= ? AND state = ?', time, 'created')
  }

  state_machine :state, initial: :created do
    state :created
    state :canceled
    state :closed

    event :cancel do
      transition created: :canceled
    end

    event :close do
      transition created: :closed
    end
  end

  # callbacks
  after_initialize :set_default_state

  # methods

  def set_default_state
    self.state ||= 'created'
  end

  def active?
    self.created?
  end

  def start_time_time=(value)
    if value.present?
      time = Time.parse(value)

      self.start_time = Time.new(
        start_time_date.year,
        start_time_date.month,
        start_time_date.day,
        time.hour,
        time.min
      )
    end
  end

  def start_time_time
    start_time.present? ? Time.parse(self.start_time.to_s) : Time.zone.now
  end

  def start_time_date=(value)
    if value.present?
      date = Date.parse(value)

      self.start_time = Time.new(
        date.year,
        date.month,
        date.day,
        start_time_time.hour,
        start_time_time.min
      )
    end
  end

  def start_time_date
    self.start_time.present? ? Date.parse(self.start_time.to_s) : Date.today.in_time_zone
  end
  # methods

  def recalculate_guests
    reload

    @male_guests_count = 0
    @female_guests_count = 0
    @guests_count = 0

    participations.active_ones.each do |participation|
      @guests_count += 1

      if participation.user.present? && participation.user.male?
        @male_guests_count += 1
      else
        @female_guests_count += 1
      end

      @male_guests_count += participation.male_count.to_i
      @female_guests_count += participation.female_count.to_i
      if participation.male_count.to_i <= 0 && participation.female_count.to_i <= 0
        @guests_count += participation.guests_count.to_i
      else
        @guests_count += participation.male_count.to_i
        @guests_count += participation.female_count.to_i
      end
    end

    self
  end

  def sold?
    participation_past? || places_left <= 0
  end

  def participation_until_date
    start_time - event.participation_until_hours.to_i.hours
  end

  def past?
    start_time.present? && start_time < Time.zone.now
  end

  def closing_possible?
    start_time.present? && (start_time + Settings.event_appointment_close_time) < Time.zone.now
  end

  def participation_past?
    participation_until_date.present? && participation_until_date < Time.zone.now
  end

  def active_participant?(user)
    if user
      participation = user.participations.active_ones.where(event_appointment_id: self.id)
      (!participation.empty? && !self.past?) || self.event.owner == user
    end
  end

  # guest count

  def guests_count
    recalculate_guests if @guests_count.nil?
    @guests_count
  end

  def male_guests_count
    recalculate_guests if @male_guests_count.nil?
    @male_guests_count
  end

  def female_guests_count
    recalculate_guests if @female_guests_count.nil?
    @female_guests_count
  end

  # free places

  def places_left
    event.max_guest_count.to_i - guests_count
  end

  def places_left_male
    if event.dating?
      if event.sex_direction.eql?('ff')
        return 0
      elsif event.sex_direction.eql?('mm') || event.max_m_guest_count.to_i > 0
        return event.max_guest_count.to_i - male_guests_count
      else
        count = event.max_guest_count.to_i
        count += 1 if event.self_participation == true && event.owner.female?
        count = (count/2).to_i
        return count - male_guests_count
      end
    else
      return places_left
    end
  end

  def places_left_female
    if event.dating?
      if event.sex_direction.eql?('mm')
       return 0
      elsif event.sex_direction.eql?('ff') || event.max_f_guest_count.to_i > 0
        return event.max_guest_count.to_i - female_guests_count
      else
        count = event.max_guest_count.to_i
        count += 1 if event.self_participation == true && event.owner.male?
        count = (count/2).to_i
        return count - female_guests_count
      end
    else
      return places_left
    end
  end

  def slug
    event.title.parameterize
  end

  def to_param
    "#{id}-#{slug}"
  end
end
