# attributes
# state, male_count, female_count, guests_count
#
# guests_count is used if m/f distinction does't matter.
class Participation < ActiveRecord::Base

  # paranoia
  acts_as_paranoid

  # relations
  belongs_to :user
  belongs_to :event_appointment

  # scopes
  scope :active_ones, lambda{ where(state: :accepted) }
  scope :canceled_ones, lambda{ where(state: :canceled) }
  scope :uninvited_ones, lambda{ where(state: :uninvited) }
  scope :not_appeared_ones, lambda{ where(state: :not_appeared) }

  # validations
  validates_presence_of :user
  validates_presence_of :event_appointment
  validate :event_appointment_start_time, :on => :create
  validate :gender_check, if: :accepted?
  validate :guest_gender_check, if: :accepted?
  validate :guest_amount_check, if: :accepted?

    # state machine
  state_machine :state, initial: :accepted do
    state :accepted
    state :declined
    state :uninvited
    state :canceled
    state :not_appeared

    event :accept do
      transition declined: :accepted
    end

    event :decline do
      transition accepted: :declined
    end

    event :uninvite do
      transition accepted: :uninvited
    end

    event :cancel do
      transition accepted: :canceled
    end

    event :not_appear do
      transition accepted: :not_appeared
    end
  end

  # callbacks
  after_initialize :set_default_state


  # methods

  def set_default_state
    self.state ||= 'accepted'
  end

  def guests_count
    read_attribute(:guests_count) || 0
  end

  def mark_as_not_appeared_possible?
    event_appointment.created? && accepted?
  end

  def male_count=(count)
    self.guests_count = count.to_i + self.female_count.to_i
    write_attribute(:male_count, count.to_i)
  end

  def female_count=(count)
    self.guests_count = count.to_i + self.male_count.to_i
    write_attribute(:female_count, count.to_i)
  end

  def event_appointment_start_time
    return if event_appointment.nil?
    if event_appointment.start_time < Time.zone.now
      errors.add(:event_appointment, 'is in past.')
    end
  end

  def gender_check
    return if event_appointment.nil?

    if event_appointment.event.sex_direction.eql?('mm') && user.female?
      errors.add(:user, 'is not male.')
    elsif event_appointment.event.sex_direction.eql?('ff') && user.male?
      errors.add(:user, 'is not female.')
    end
  end

  def guest_gender_check
    return if event_appointment.nil?

    if event_appointment.event.sex_direction.eql?('mm') && female_count.to_i > 0
      errors.add(:female_count, 'no female persons allowed.')
    elsif event_appointment.event.sex_direction.eql?('ff') && male_count.to_i > 0
      errors.add(:male_count, 'no male persons allowed.')
    end
  end

  def guest_amount_check
    return if event_appointment.nil?
    return if user.nil?

    if event_appointment.event.sex_direction.eql?('mm')
      if event_appointment.places_left_male - male_guests_sum < 0
        errors.add(:event_appointment, 'is full.')
      end
    elsif event_appointment.event.sex_direction.eql?('ff')
      if event_appointment.places_left_female - female_guests_sum < 0
        errors.add(:event_appointment, 'is full.')
      end
    else
      if event_appointment.places_left_male - male_guests_sum < 0
        errors.add(:event_appointment, 'is full.')
      end
      if event_appointment.places_left_female - female_guests_sum < 0
        errors.add(:event_appointment, 'is full.')
      end
      if event_appointment.places_left - guests_sum < 0
        errors.add(:event_appointment, 'is full.')
      end
    end
  end

  # methods
  def guests_sum
    if male_count.to_i > 0 && female_count.to_i > 0
      male_count.to_i + female_count.to_i + 1
    else
      guests_count + 1
    end
  end

  def male_guests_sum
    if user.male?
      male_count.to_i + 1
    else
      male_count.to_i
    end
  end

  def female_guests_sum
    if user.female?
      female_count.to_i + 1
    else
      female_count.to_i
    end
  end

end
