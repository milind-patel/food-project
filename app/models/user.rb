# attributes
# date_of_birth, name, job, phone, relationship, interests, description
# relation_user_languages, languages, gender
# fb_uid
# main_image_id
# newsletter_subscription, newsletter_city_id
#
# rating attributes:
# aggregated_rating_total, aggregated_rating_meal, aggregated_rating_atmosphere,
# aggregated_rating_location, aggregated_rating_price_performance_ratio,
# aggregated_ratings_count
class User < ActiveRecord::Base

  GENDERS = ['m', 'f']

  # paranoia
  acts_as_paranoid

  # Include default devise modules. Others available are:
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :lockable, :timeoutable, :omniauthable, :async,
         omniauth_providers: [:facebook]

  # relations - payment data, addresses
  has_many :addresses, class_name: 'Address', inverse_of: :user

  # relations
  has_many :events, foreign_key: :owner_id, inverse_of: :owner
  has_many :images, as: :imaged
  has_many :languages, class_name: 'Relation::UserLanguage', inverse_of: :user
  has_many :participations
  has_many :event_ratings

  # newsletter
  belongs_to :newsletter_city, class_name: 'City'

  serialize :facebook_oauth_data

  # filter
  after_initialize :init

  # validations
  validates :gender, inclusion: { in: GENDERS }
  validates_presence_of :name, if: proc { |u| u.check_profile? }
  validates_presence_of :main_image, if: proc { |u| u.check_profile? },
    message: I18n::t('activerecord.errors.models.user.attributes.main_image.blank')
  validates_presence_of :date_of_birth, if: proc { |u| u.check_profile? }
  validate :full_age, if: proc { |u| u.check_profile? }


  # nested attributes
  accepts_nested_attributes_for :languages, update_only: true


  # encryption
  attr_encrypted_options.merge!(algorithm: 'aes-256-cbc', key: Settings.encryption_key_users)
  attr_encrypted :phone_e, attribute: 'encrypted_phone'



  # ------------------------------------------------------------
  # static methods

  def self.find_for_facebook_oauth(auth)
    uid = auth.slice(:uid)[:uid]
    user = where(fb_uid: uid).first_or_initialize.tap do |user|
      infos = auth.info
      gender = auth.extra['raw_info'] && auth.extra['raw_info'].gender

      user.fb_uid = auth.uid if user.fb_uid.blank?
      user.email = infos.email if infos.email.present? && user.email.blank?
      user.password = Devise.friendly_token[0,20] if user.encrypted_password.blank?
      if user.name.blank?
        user.name = [
          infos.first_name,
          infos.last_name,
        ].compact.join(' ')
      end
      user.gender = (gender && gender == 'male' ? 'm' : 'f' ) if user.gender.blank?
      user.facebook_oauth_data = auth
    end

    user.skip_confirmation! unless user.persisted?

    if user.save
      user
    elsif user.errors.messages.count == 1 && user.errors.messages.include?(:email)
      User.unscoped.where(email: auth.info.email).each do |tmp|
        return user if tmp.deleted_at.nil?
      end
      user.save(validate: false)
      user
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end


  # ------------------------------------------------------------
  # methods

  def check_profile?
    @check_profile || false
  end

  def check_profile=(check)
    @check_profile = check
  end

  def profile_completed?
    name.present? && main_image.present?
  end

  def init
    self.gender ||= GENDERS.first
  end

  def main_image
    images.find(main_image_id)

  rescue ActiveRecord::RecordNotFound
    images.first || nil
  end

  def main_image=(image)
    main_image_id = image.id
  end

  def male?
    gender == 'm'
  end

  def female?
    gender == 'f'
  end

  def self.genders
    GENDERS
  end

  # ------------------------------------------------------------
  # aggregated ratings between 0 and 1 in percent

  # should be called async
  def reaggregate_ratings
    service = RatingAggregationService.new(self)
    rating = service.run
    if rating
      self.aggregated_rating_total = rating[:total] if rating[:total].present? && rating[:total].to_f > 0
      self.aggregated_rating_meal = rating[:meal] if rating[:meal].present? && rating[:meal].to_f > 0
      self.aggregated_rating_atmosphere = rating[:atmosphere] if rating[:atmosphere].present? && rating[:atmosphere].to_f > 0
      self.aggregated_rating_location = rating[:location] if rating[:location].present? && rating[:location].to_f > 0
      self.aggregated_rating_price_performance_ratio = rating[:price_performance_ratio] if rating[:price_performance_ratio].present? && rating[:price_performance_ratio].to_f > 0
      self.aggregated_ratings_count = rating[:count] if rating[:count].present? && rating[:count].to_i > 0
      self.save
    end
  end

  def self.reaggregate_ratings_all
    RatingAggregationService.aggregate_all
  end

  def check_newsletter_subscription
    return if self.newsletter_city.nil?
    nl = NewsletterSubscription.find_or_initialize_by(email: self.email)

    nl.user = self
    nl.city = self.newsletter_city
    nl.name = self.name
    nl.active = self.newsletter_subscription
    nl.save
  end


  private

  def full_age
    if self.date_of_birth.present? && self.date_of_birth > (Date.today - 18.years)
      errors.add(:date_of_birth, 'Du musst mindestens 18 Jahre alt sein.')
    end
  end
end
