# database attributes:
#   String:  headline
#   String:  type
#   Text:    text
#   Image:   image

class BlogElement::Base < ActiveRecord::Base
  self.table_name = "blog_elements"

  # scopes
  default_scope -> { order(position: :asc) }

  # relations
  belongs_to :blog_entry, class_name: "BlogEntry", inverse_of: :elements

  # acts as list
  acts_as_list scope: :blog_entry

end