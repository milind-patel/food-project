# database attributes
# company, firstname, lastname, salutation, user, addition, city, country, street, zip

class Address < ActiveRecord::Base

  # paranoia
  acts_as_paranoid

  # relations
  belongs_to :user, class_name: 'User', inverse_of: :addresses

  #encryption
  attr_encrypted_options.merge!(algorithm: 'aes-256-cbc', key: Settings.encryption_key_addresses)
  attr_encrypted :addition_e, attribute: 'encrypted_addition'   # tmp
  attr_encrypted :street_e, attribute: 'encrypted_street'       # tmp
  attr_encrypted :company_e, attribute: 'encrypted_company'     # tmp
  attr_encrypted :firstname_e, attribute: 'encrypted_firstname' # tmp
  attr_encrypted :lastname_e, attribute: 'encrypted_lastname'   # tmp

  attr_encrypted :addition, attribute: 'encrypted_addition'
  attr_encrypted :street, attribute: 'encrypted_street'
  attr_encrypted :company, attribute: 'encrypted_company'
  attr_encrypted :firstname, attribute: 'encrypted_firstname'
  attr_encrypted :lastname, attribute: 'encrypted_lastname'

  # validations
  validates_presence_of :firstname
  # validates_presence_of :lastname # removed to provide simpler address insertion
  validates_presence_of :user
  validates_presence_of :city
  validates_presence_of :street
  validates_presence_of :zip

  validates :salutation, inclusion: {
    in: :valid_salutations,
    message: "%{value} is not a valid salutation",
  }, if: 'present?'

  def valid_salutations
    ['w', 'm', nil]
  end


  # temporary fields

  def unsafe_firstname
    read_attribute(:firstname)
  end
  def unsafe_firstname=(v)
    write_attribute(:firstname, v)
  end

  def unsafe_lastname
    read_attribute(:lastname)
  end
  def unsafe_lastname=(v)
    write_attribute(:lastname, v)
  end

  def unsafe_company
    read_attribute(:company)
  end
  def unsafe_company=(v)
    write_attribute(:company, v)
  end

  def unsafe_addition
    read_attribute(:addition)
  end
  def unsafe_addition=(v)
    write_attribute(:addition, v)
  end

  def unsafe_street
    read_attribute(:street)
  end
  def unsafe_street=(v)
    write_attribute(:street, v)
  end

end
