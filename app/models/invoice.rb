# database attributes
# firstname, lastname, salutation, email, company, city, country, street, addition, zip,
# user_id, amount, number, date, generated, payed

# attributes
# user, transactions

class Invoice < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user, class_name: 'User', inverse_of: :invoices

  has_many :transactions, class_name: 'Transaction::Intern', inverse_of: :invoice
  has_many :payments, class_name: 'Transaction::InvoicePayment', inverse_of: :invoice

  #paperclip relation
  has_attached_file :file

  after_validation :generate_amount
  after_save :generate_number
  before_save :check_payed_status
  before_create :create_access_key

  validates_attachment :file, content_type: { content_type: "application/pdf" }
  validates_presence_of :firstname
  validates_presence_of :email
  validates_presence_of :city
  validates_presence_of :street
  validates_presence_of :zip
  validates_presence_of :user_id
  validates_presence_of :date

  scope :all_unpayed, lambda {
    where(payed: false)
  }

  scope :all_payed, lambda {
    where(payed: true)
  }

  def remaining_amount
    payments.inject(-1 * amount) do |result, payment|
      result -= payment.value
    end
  end

  def create_document
    if self.persisted?
      url = URI.join(document_create_host_url, document_create_path).to_s
      self.file_remote_url = url
      self.save
    end
  end

  def file_remote_url=(url)
    @file_remote_url = url
    self.file = URI.parse(url)
  end

  def accessible?(key)
    self.access_key.eql?(key)
  end

  def amount_after_tax
    self.amount
  end

  def tax
    Settings.invoice_tax
  end

  def filename
    "foodoo-#{self.number}-#{self.date.strftime("%Y%m%d")}"
  end

  def number
    n = read_attribute(:number)
    return nil if n.blank?
    "#{Time.now.strftime('%y')}-#{n}"
  end

  def client_number
    sprintf('%05d', self.user.id)
  end

  private
  def generate_number
    if self.number.nil?
      number = sprintf('%05d', self.id)
      self.update_attribute(:number, number)
    end
  end

  def generate_amount
    self.amount = transactions.inject(0) do |result, transaction|
      result += transaction.value

      result
    end

    self.before_tax_amount = self.amount / (1 + tax)
  end

  def check_payed_status
    self.payed = remaining_amount <= 0

    true
  end

  def update_access_key
    self.access_key = nil
    self.save
  end

  def document_create_host_url
    Settings.documents_create_host
  end

  def document_create_path
    Rails.application.routes.url_helpers.invoice_path(
      id: self.id,
      action: 'show',
      access_key: self.access_key,
      format: :pdf
    )
  end

  def create_access_key
    if self.access_key.blank?
      self.access_key = SecureRandom.hex(64)
    end
  end
end
