# attributes
# iban, bic, bank, owner

# relations
# user

class PaymentData < ActiveRecord::Base

  # paranoia
  acts_as_paranoid

  # relations
  belongs_to :user

  # encryption
  attr_encrypted_options.merge!(algorithm: 'aes-256-cbc', key: Settings.encryption_key_payment_data)
  attr_encrypted :iban_e, attribute: 'encrypted_iban'
  attr_encrypted :bic_e, attribute: 'encrypted_bic'
  attr_encrypted :bank_e, attribute: 'encrypted_bank'
  attr_encrypted :owner_e, attribute: 'encrypted_owner'

  attr_encrypted :iban
  attr_encrypted :bic
  attr_encrypted :bank
  attr_encrypted :owner

  # validations
  validates_presence_of :user
  validates_presence_of :owner
  validates_presence_of :bank
  validates_presence_of :iban

  validates_length_of :iban, {:minimum => 15, :maximum => 31, :allow_blank => false,
    message: "Falsche Länge! Zwischen 15 und 31 Zeichen." }
  validates_length_of :bic, {:minimum => 8, :maximum => 11, :allow_blank => true,
    message: "Falsche Länge! Zwischen 8 und 11 Zeichen." }


  # temporary fields

  def unsafe_owner
    read_attribute(:owner)
  end
  def unsafe_owner=(v)
    write_attribute(:owner, v)
  end

  def unsafe_bank
    read_attribute(:bank)
  end
  def unsafe_bank=(v)
    write_attribute(:bank, v)
  end

  def unsafe_iban
    read_attribute(:iban)
  end
  def unsafe_iban=(v)
    write_attribute(:iban, v)
  end

  def unsafe_bic
    read_attribute(:bic)
  end
  def unsafe_bic=(v)
    write_attribute(:bic, v)
  end


end
