# database attributes:
#   String:  name
#   Text:    teaser_text
#   Image:   teaser_image
#   Date:    date

class BlogEntry < ActiveRecord::Base
  acts_as_paranoid

  has_attached_file :teaser_image, :styles => { :large => "500x500#", :medium => "330x230#", :thumb => "80x80#" }
  has_many :elements, class_name: 'BlogElement::Base', inverse_of: :blog_entry

  #validations
  validates :alias, uniqueness: {scope: :date}, presence: true
  validates :name, presence: true

  #scopes
  scope :published, lambda { where("published = ?", true ) }

  # accepts_nested_attributes_for :elements

  def params_for_show
    year = self.date.strftime("%Y")
    month = self.date.strftime("%m")
    day = self.date.strftime("%d")
    alias_id = self.alias

    {year: year, month: month, day: day, alias: alias_id}
  end

  def self.blog_entry_from_params(params)
    begin
      date = Date.new(params[:year].to_i, params[:month].to_i, params[:day].to_i)
    rescue Exception => e
      blog_entry = nil
    end

    if date
      blog_entry = BlogEntry.find_by(date: date, alias: params[:alias])
    end
  end

end