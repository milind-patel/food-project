# attributes: name, timestamps
class City < ActiveRecord::Base

  acts_as_paranoid

  def self.all_names
    all.map(&:name)
  end
end
