class Transaction::Intern < Transaction::Base
  belongs_to :invoice, class_name: 'Invoice', inverse_of: :transactions
end
