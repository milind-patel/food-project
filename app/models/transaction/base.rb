# database - attributes
# account_balance_id, comment, value, event_id

# attributes
# account_balance, event
class Transaction::Base < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :account_balance, class_name: 'AccountBalance', inverse_of: :transactions
  belongs_to :event_appointment #TODO add validation if the event belongs to the account_balancce.user to check
  #if the event could assign to the transaction

  validates_presence_of :account_balance
  validates_presence_of :value

  def float_value
    self.value = value.to_f / 100
  end

  def float_value=(value)
    self.value = (value.to_f * 100)
  end
end
