class Transaction::InvoicePayment < Transaction::Base
  belongs_to :invoice, class_name: 'Invoice', inverse_of: :payments
end
