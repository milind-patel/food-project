# database attributes
# user_id

class AccountBalance < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user, class_name: 'User', inverse_of: :account_balance

  has_many :transactions, class_name: 'Transaction::Base', inverse_of: :account_balance

  validates_presence_of :user

  scope :all_underbalanced, lambda {
    where(
      "(SELECT SUM(transaction_bases.value) FROM transaction_bases
      WHERE transaction_bases.account_balance_id LIKE account_balances.id) < 0"
    )
  }

  scope :all_underbalanced_without_invoice, lambda {
    where(
      "(SELECT SUM(transaction_bases.value) FROM transaction_bases
      WHERE transaction_bases.account_balance_id LIKE account_balances.id AND
      transaction_bases.invoice_id is null) < 0"
    )
  }

  def balance
    transactions.inject(0) do |result, transaction|
      result = result + transaction.value
    end
  end

  def is_balanced?
    balance >= 0
  end

  def balance_status
    is_balanced? ? 'positive' : 'negative'
  end

  def unpayed_transactions
    self.transactions.where(invoice_id: nil)
  end
end
