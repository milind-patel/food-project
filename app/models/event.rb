# database attributes
# owner, title, description, courses_count, max_f_guest_count
# max_guest_count, max_m_guest_count, min_m_guest_count, min_guest_count
# min_f_guest_count, price, category, duration, repetition
# address_city, address_zip, address_street, address_addition
# latitude, longitude, fake_latitude, fake_longitude
# ring_at, floor, state, sex_direction, self_participation

# attributes
# category, owner, transaction, images

class Event < ActiveRecord::Base
  # SEX_DIRECTIONS = ['hetero', 'homo']
  SEX_DIRECTIONS = ['mf', 'mm', 'ff']

  # paranoid
  acts_as_paranoid

  # relations
  belongs_to :category, class_name: 'Category'
  belongs_to :owner, class_name: 'User', inverse_of: :events

  has_many :images, as: :imaged
  has_many :event_drinks, class_name: "Relation::EventDrink"
  has_many :event_appointments
  has_many :event_event_informations, class_name: "Relation::EventEventInformation"
  has_many :event_informations, through: :event_event_informations

  # ---------------------------------------------------
  # validations
  validates_presence_of :owner

  #before_validation :check_max_guest_even_or_odd

  def max_guest_count_sum
    return if dating? && self_participation

    if sex_direction == 'mf'
      if max_f_guest_count.to_i > 0 || max_m_guest_count.to_i > 0
        sum = max_f_guest_count.to_i + max_m_guest_count.to_i
        if sum != max_guest_count
          errors.add(:max_guest_count, 'is not the result of max_m_guest_count + max_f_guest_count.')

          return false
        end
      end
    elsif sex_direction == 'mm'
      if max_m_guest_count.to_i != max_guest_count
        errors.add(:max_guest_count, 'is not the equal max_m_guest_count.')

        return false
      end
    elsif sex_direction == 'ff'
      if max_f_guest_count.to_i != max_guest_count
        errors.add(:max_guest_count, 'is not the equal max_f_guest_count.')

        return false
      end
    end

    return true
  end

  def dating_arrangement
    return unless dating?

    m = max_m_guest_count.to_i
    f = max_f_guest_count.to_i

    if sex_direction == 'mf'
      if self_participation
        if owner.male? && (m+1 != f)
          errors.add(:max_m_guest_count, 'is not the equal max_f_guest_count-1.')
        elsif owner.female? && (m != f+1)
          errors.add(:max_m_guest_count, 'is not the equal max_f_guest_count+1.')
        end
      elsif !self_participation && m != f
        errors.add(:max_m_guest_count, 'is not the equal max_f_guest_count.')
      end
    elsif sex_direction == 'mm'
      if self_participation && owner.female?
        errors.add(:sex_direction, 'you are not male.')
      elsif self_participation && (m+1)%2 != 0
        errors.add(:max_m_guest_count, 'is not even with you.')
      elsif self_participation && m+1 != max_guest_count
        errors.add(:max_m_guest_count, 'is not equal max_guest_count - 1.')
      elsif !self_participation && m%2 != 0
        errors.add(:max_m_guest_count, 'is not even.')
      elsif !self_participation && m != max_guest_count
        errors.add(:max_m_guest_count, 'is not equal max_guest_count.')
      end
    else
      if self_participation && owner.male?
        errors.add(:sex_direction, 'you are not female.')
      elsif self_participation && (f+1)%2 != 0
        errors.add(:max_f_guest_count, 'is not even with you.')
      elsif self_participation && f+1 != max_guest_count
        errors.add(:max_f_guest_count, 'is not equal max_guest_count - 1.')
      elsif !self_participation && f%2 != 0
        errors.add(:max_f_guest_count, 'is not even.')
      elsif !self_participation && f != max_guest_count
        errors.add(:max_f_guest_count, 'is not equal max_guest_count.')
      end
    end
  end

  # ---------------------------------------------------
  # geocoding
  before_validation :fill_max_counts

  # ---------------------------------------------------
  # nested stuff
  accepts_nested_attributes_for :category
  accepts_nested_attributes_for :event_event_informations
  accepts_nested_attributes_for :owner
  accepts_nested_attributes_for :event_appointments, allow_destroy: true

  bitmask :repetition, :as => [:daily, :weekly, :monthly, :not]
  # ---------------------------------------------------
  # state machine

  state_machine :state, initial: :started do
    state :started

    state :create_1 do
      # presence
      validates_presence_of :title
      validates_presence_of :category_id
      validates_presence_of :sex_direction, :if => :is_dating_dinner?
      validates_inclusion_of :self_participation, in: [true, false], :if => :is_dating_dinner?
      validates_presence_of :max_guest_count
      validates_presence_of :courses_count
      validates_presence_of :duration
      validates_presence_of :description
      validates_presence_of :images
      validates_presence_of :category
      validate :city_restriction

      # format
      validates :sex_direction, inclusion: { in: SEX_DIRECTIONS }, :if => :is_dating_dinner?
      validates :max_guest_count, numericality: { only_integer: true, greater_than: 0 }
      validates :max_guest_count, numericality: { only_integer: true, greater_than: 0 }
      validate :dating_arrangement
    end

    state :create_2 do
      # presence step 1
      validates_presence_of :title
      validates_presence_of :category_id
      validates_presence_of :sex_direction, :if => :is_dating_dinner?
      validates_inclusion_of :self_participation, in: [true, false], :if => :is_dating_dinner?
      validates_presence_of :max_guest_count
      validates_presence_of :courses_count
      validates_presence_of :duration
      validates_presence_of :description
      validates_presence_of :images
      validates_presence_of :category

      # presence step 2
      validates_presence_of :person_name
      validates_presence_of :address_city
      validates_presence_of :address_street
      validates_presence_of :address_district
      validates_presence_of :address_zip
      # validates_presence_of :ring_at
      # validates_presence_of :floor
      validates_presence_of :latitude
      validates_presence_of :longitude
      validates_presence_of :fake_latitude
      validates_presence_of :fake_longitude
      validate :city_restriction

      # format step 1
      validates :sex_direction, inclusion: { in: SEX_DIRECTIONS }, :if => :is_dating_dinner?
      validates :max_guest_count, numericality: { only_integer: true, greater_than: 0 }
      validate :dating_arrangement
    end

    state :created do
      # presence step 1
      validates_presence_of :title
      validates_presence_of :category_id
      validates_presence_of :sex_direction, :if => :is_dating_dinner?
      validates_inclusion_of :self_participation, in: [true, false], :if => :is_dating_dinner?
      validates_presence_of :max_guest_count
      validates_presence_of :courses_count
      validates_presence_of :duration
      validates_presence_of :description
      validates_presence_of :images
      validates_presence_of :category

      # presence step 2
      validates_presence_of :person_name
      validates_presence_of :address_city
      validates_presence_of :address_street
      validates_presence_of :address_zip
      # validates_presence_of :ring_at
      # validates_presence_of :floor
      validates_presence_of :latitude
      validates_presence_of :longitude
      validates_presence_of :fake_latitude
      validates_presence_of :fake_longitude

      # presence step 3
      validates_presence_of :price

      # format step 1
      validates :sex_direction, inclusion: { in: SEX_DIRECTIONS }, :if => :is_dating_dinner?
      validates :max_guest_count, numericality: { only_integer: true, greater_than: 0 }
      validate :dating_arrangement

      #format step 3
      validates_acceptance_of :agb_confirmation, accept: true
      #validates_acceptance_of :debit_authorization, accept: true
    end

    state :approved do
      validates_presence_of :description
    end

    state :created do
      validates_presence_of :description
    end

    state :declined do
      validates_presence_of :description
    end

    state :waiting_for_approval do
      validates_presence_of :description
    end

    #transitions
    event :back do
      transition create_2: :create_1
      transition create_1: :started
    end

    event :next do
      transition started: :create_1
      transition create_1: :create_2
      transition create_2: :created
    end

    event :create do
      transition all: :created
    end

    event :submit do
      transition created: :waiting_for_approval
      transition declined: :waiting_for_approval
    end

    event :approve do
      transition waiting_for_approval: :approved
    end

    event :decline do
      transition waiting_for_approval: :declined
      transition approved: :declined
    end

    after_transition to: :waiting_for_approval, do: :notify_for_new_event
  end

  # callbacks
  after_initialize :set_default_state

  # methods

  def set_default_state
    self.state ||= 'started'
  end

  def drinks
    self.event_drinks.map{|ed| ed.drink }.compact
  end

  def drink_ids
    self.event_drinks.map{|ed| ed.drink_id }.compact
  end

  def drink_ids=(values)
    relations = []

    if values.present?
      values = values.uniq

      values.each do |drink_id|
        unless self.drink_ids.include?(drink_id)
          relations << Relation::EventDrink.new(event: self, drink_id: drink_id)
        end
      end
    end

    self.event_drinks = relations
  end

  def drinks=(values)
    self.drink_ids = values.map(&:id)
  end

  def open_ends=(values)
    self.open_end = values.first
  end

  def open_ends
    [self.open_end]
  end

  def own_max_guest_count=(value)
    self.max_guest_count = value if value.present?
  end

  def event_appointment
    self.event_appointments.first
  end

  def event_appointment=(values)
    appointment = self.event_appointments.first

    if appointment.nil?
      self.event_appointments << EventAppointment.new(values)
    else
      appointment.update_attributes(values)
    end
  end

  def event_information_ids=(value)
    value.reject! { |v| v.empty? }
    self.event_informations = EventInformation.find(value)
  end

  def event_information_ids
    self.event_informations.map(&:id)
  end

  def is_dating_dinner?
    category_id == 2
  end

  # ---------------------------------------------------
  # methods

  def city_restriction
    @city_restriction_service ||= CityRestrictionService.new

    unless @city_restriction_service.address_accessable?(address)
      errors.add(:address_city, 'Die Stadt muss Berlin oder Hamburg sein.')
      errors.add(:address_street, 'Die Straße muss sich in Berlin oder Hamburg befinden.')
      errors.add(:address_district, 'Der Stadtbezirk muss sich in Berlin oder Hamburg befinden.')
      errors.add(:address_zip, 'Die PLZ muss sich in Berlin oder Hamburg befinden.')
    end
  end

  def address
    [
      address_city,
      address_street,
      address_district,
      address_zip,
    ].compact.join(' ')
  end

  def description=(value)
    value = ActionController::Base.helpers.strip_tags(value)

    write_attribute(:description, value)
  end

  def owner?(user)
    user && self.owner.id == user.id
  end

  def attributes
    super.merge(
      'float_price' => self.float_price,
      'drink_ids' => self.drink_ids
    )
  end

  def latitude=(lat)
    lat = lat.to_f
    write_attribute(:latitude, lat)
    fake_lat = lat + (-0.009 + rand(0..0.018))
    write_attribute(:fake_latitude, fake_lat)
    Rails.logger.debug("Lat: #{lat}")
  end

  def longitude=(lng)
    lng = lng.to_f
    write_attribute(:longitude, lng)
    fake_lng = lng + (-0.009 + rand(0..0.018))
    write_attribute(:fake_longitude, fake_lng)
    Rails.logger.debug("Lng: #{lng}")
  end

  def float_price=(value)
    self.price = value.to_i * 100
  end

  def float_price
    self.price.to_f / 100
  end

  def full_address()
    "#{address_street}, #{address_zip} #{address_city}"
  end

  def is_information_set?(event_information)
    self.event_informations.include?(event_information)
  end

  def self.sex_directions
    SEX_DIRECTIONS
  end

  def notify_for_new_event
    EventMailingWorker.perform_async(self.id, :created)
  end


  # genered stuff and guests calculations

  def homo?
    sex_direction.eql?('mm') || sex_direction.eql?('ff') ? true : false
  end

  def hetero?
    homo? ? false : true
  end

  def homo_male?
    homo? && sex_direction.eql?('mm')
  end

  def homo_female?
    homo? && sex_direction.eql?('ff')
  end

  def dating?
    return false if category.nil?
    category.dating?
  end

  def fill_max_counts
    if dating? && max_guest_count.present?
      if self_participation
        count = (max_guest_count + 1) / 2
      else
        count = max_guest_count / 2
      end

      if hetero? && self_participation && owner.male?
        self.max_m_guest_count = count - 1
        self.max_f_guest_count = count
      end

      if hetero? && self_participation && owner.female?
        self.max_m_guest_count = count
        self.max_f_guest_count = count - 1
      end

      if homo_male? && !self_participation && owner.female?
        self.max_m_guest_count = (count * 2)
        self.max_f_guest_count = 0
      end

      if homo_male? && self_participation && owner.male?
        self.max_m_guest_count = (count * 2) - 1
        self.max_f_guest_count = 0
      end

      if homo_female? && self_participation && owner.female?
        self.max_m_guest_count = 0
        self.max_f_guest_count = (count * 2) - 1
      end

      if homo_female? && !self_participation && owner.male?
        self.max_m_guest_count = 0
        self.max_f_guest_count = (count * 2)
      end
    end

    true
  end

end
