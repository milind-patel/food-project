# attributes
# title, relation_user_languages, users

class Language < ActiveRecord::Base
  validates_uniqueness_of :iso_639_1

  has_many :relation_user_languages, class_name: 'Relation::UserLanguage', inverse_of: :language
  has_many :users, through: :relation_user_languages
end
