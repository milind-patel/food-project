class BackendUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  devise :database_authenticatable, :recoverable, :rememberable,
         :trackable, :validatable, :lockable, :timeoutable

  # bitmask
  bitmask :access_rights, as: [
    :manage_all,                # WWL - admin user

    :manage_backend_users,      # backend users management
    :manage_users,              # frontend users management

    :manage_events,             # events management
    :manage_cities,             # WWL - cities management > currently, this should only be possible for wwl
    :manage_blog,               # blog management

    :view_background_jobs,      # WWL - sidekiq view

    :receiving_new_event_email, # receive an email if an event was created
    :manage_csv,                # export users from database
    :receiving_invoice_email,   # receive an email if a new invoice has been generated
  ]

  def can?(resource)
    access_rights?(resource)
  end

  def cancan?(action, resource)
    ability = Ability.new(self)
    ability.can?(action, resource)
  end

end
