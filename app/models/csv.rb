class Csv < ActiveRecord::Base
  has_attached_file :file

  validates_attachment_presence :file

  def filename
    "#{kind}-#{id}-#{formatted_created_at}.csv"
  end

  def formatted_created_at
    created_at.strftime("%Y-%m-%d_%H:%M:%S")
  end
end
