class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities

    user ||= BackendUser.new

    can :manage, BackendUser if user.can?(:manage_backend_users)
    can :manage, User if user.can?(:manage_users)

    can :manage, Event if user.can?(:manage_events)
    can :manage, EventAppointment if user.can?(:manage_events)
    can :manage, City if user.can?(:manage_cities)
    can :manage, BlogEntry if user.can?(:manage_blog)
    can :manage, Csv if user.can?(:manage_csv)

    can :view, :background_jobs if user.can?(:view_background_jobs)

    # admins
    can :manage, :all if user.can?(:manage_all)


  end
end
