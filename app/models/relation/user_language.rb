# attributes
# strength, language, user
#
# - min 0
# - max 4
class Relation::UserLanguage < ActiveRecord::Base
  delegate :title, to: :language

  STRENGTHS = [0, 1, 2, 3, 4]

  # relations
  belongs_to :user, inverse_of: :languages
  belongs_to :language, inverse_of: :relation_user_languages

  # validations
  validates_presence_of :language
  validates_presence_of :strength
  validates_presence_of :user
  validates_uniqueness_of :language_id, scope: :user_id

  # methods

  def available_strengths
    STRENGTHS
  end

  def iso_639_1
    language.nil? ? nil : language.iso_639_1
  end

end
