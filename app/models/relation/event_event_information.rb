class Relation::EventEventInformation < ActiveRecord::Base
  belongs_to :event
  belongs_to :event_information
end
