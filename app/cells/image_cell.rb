class ImageCell < Cell::Rails
  def upload(event = nil)
    if event.present?
      @path_param = {:event_id => event.id}
    else
      @path_param = {}
    end

    render
  end

  def js_templates
    render
  end
end
