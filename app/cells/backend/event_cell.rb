class Backend::EventCell < Cell::Rails
  helper_method :active_class
  helper_method :class_for_state_event
  helper_method :html_options
  helper_method :path_for_state_event

  def state_control(event)
    @state_events = event.state_events

    render
  end

  def state_filter_navigation(current_event_state)
    @active_event_state = current_event_state
    @event_states = [
      :waiting_for_approval,
      :separator,
      :approved,
      :expired,
      :closed,
      :separator,
      :declined,
      :created,
    ]

    render
  end

  #helpers
  def active_class(event_state, active_event_state)
    event_state.to_s == active_event_state.to_s ? 'active' : ''
  end

  def html_options(state_event)
    {
      method: :put,
      class: [
        'btn',
        "btn-#{class_for_state_event(state_event)}",
        'pull-right'
      ]
    }
  end

  def class_for_state_event(state_event)
    @event_classes ||= {
      approve: 'success',
      decline: 'danger',
      submit: 'success',
    }

    @event_classes[state_event]
  end

  def path_for_state_event(state_event)
    {controller: 'backend/events', action: state_event}
  end
end
