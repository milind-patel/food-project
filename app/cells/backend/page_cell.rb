class Backend::PageCell < Cell::Rails
  include ApplicationHelper
  include ActionView::Helpers::UrlHelper
  include Devise::Controllers::Helpers
  include CanCan::ControllerAdditions

  helper_method :current_user
  helper_method :current_page_class

  def navigation(current_user)
    @current_user = current_user

    render
  end

  def flash_messages
    @flash = controller.flash
    render
  end

end
