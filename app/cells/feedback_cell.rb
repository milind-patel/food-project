class FeedbackCell < Cell::Rails
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::OutputSafetyHelper

  append_view_path "app/views" #for using the rating partials

  def event_ratings(event_appointment)
    ratings = event_appointment.decorate.event_ratings

    @total_percentage = event_appointment.calc_percentage(ratings, :total)
    @meal_percentage = event_appointment.calc_percentage(ratings, :meal)
    @location_percentage = event_appointment.calc_percentage(ratings, :location)
    @atmosphere_percentage = event_appointment.calc_percentage(ratings, :atmosphere)
    @price_performance_percentage = event_appointment.calc_percentage(ratings, :price_performance_ratio)

    render view: 'ratings'
  end

  def user_ratings(user)
  	@total_percentage = user.aggregated_rating_total
    @meal_percentage = user.aggregated_rating_meal
    @location_percentage = user.aggregated_rating_location
    @atmosphere_percentage = user.aggregated_rating_atmosphere
    @price_performance_percentage = user.aggregated_rating_price_performance_ratio

  	render view: 'ratings'
  end

  def event_comments(event_appointment)
    @ratings = event_appointment.event_ratings

    render view: 'comments'
  end

  def user_comments(user)
    @ratings = []
    user.organized_events.each do |ea|
      ea.event_ratings.each do |rating|
        @ratings << rating
      end
    end

    render view: 'comments'
  end

  def comment(rating)
    @user = rating.user.try(:decorate)
    @image_url = @user.profile_image_thumb_small
    @comment = simple_format(rating.message)

    render
  end

end