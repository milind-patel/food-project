class SocialCell < Cell::Rails

  def facebook_share(appointment)
    appointment = appointment.decorate
    event = appointment.event.decorate

    url = event_appointment_url(appointment).to_s
    redirect_uri = internal_user_url.to_s
    caption = appointment.event.title
    summary = appointment.event.description
    image = root_url.to_s+event.main_image_path.to_s if event.main_image

    @link = share_with_facebook_url({ caption: caption, link: url, redirect_uri: redirect_uri, description: summary, picture: image })

    render
  end

private
  def share_with_facebook_url(opts)
    url = "https://www.facebook.com/dialog/feed?app_id=" + Settings.facebook_app_key
    parameters = []

    opts.each do |k,v|
      key = "#{k}"

      if v.is_a? Hash
        v.each do |sk, sv|
          parameters << "#{key}[#{sk}]=#{sv}"
        end
      else
        parameters << "#{key}=#{v}"
      end

    end

    url + "&" + parameters.join("&")
  end
end