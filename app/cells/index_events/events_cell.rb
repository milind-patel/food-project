class IndexEvents::EventsCell < Cell::Rails
  include ViewModel
  include Cells::Rails::ActionView

  def show
    @events = model
    render
  end

end