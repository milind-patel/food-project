class IndexEvents::NewsletterCell < Cell::Rails

  def show
    @newsletter_subscription = NewsletterSubscription.new
    @newsletter_subscription.city = City.first
    @cities = City.all
    render
  end

end