class IndexEvents::HeadlineCell < Cell::Rails
  include ViewModel
  include Cells::Rails::ActionView

  def show
    @headline_strings = build_headline_strings(model)
    @model = model
    render
  end

  private

  def build_headline_strings(date)
    if Date.today == date
      [t('public.base.today'), l(date, :format => '%A den %d. %B %Y')]
    elsif (Date.today+1.day) == date
      [t('public.base.tomorrow'), l(date, :format => '%A den %d. %B %Y')]
    else
      [l(date, :format => '%A'), l(date, :format => 'den %d. %B %Y')]
    end
  end

end