class EventInformationCell < Cell::Rails

  def normal(event, event_infos)
    event_infos.inject("") do |result, event_info|
      wrapper_html = {}
      btn_title = t("public.event_informations.button_titles.#{event_info.title}")
      if event.is_information_set?(event_info)
        wrapper_html[:class] ||= ''
        wrapper_html[:class] << ' active'
        content = "<i class='info-icon icon-#{event_info.title}'></i>#{btn_title}".html_safe
      else
        wrapper_html[:class] ||= ''
        content = "<i class='info-icon icon-#{event_info.title}'></i>#{btn_title}".html_safe
      end

      result << default(content, wrapper_html)
      result
    end.html_safe
  end

  def hover(event, event_infos)
    event_infos.inject("") do |result, event_info|
      wrapper_html = {}
      btn_title = t("public.event_informations.button_titles.#{event_info.title}")
      if event.is_information_set?(event_info)
        wrapper_html[:class] ||= ''
        wrapper_html[:class] << ' active'
        content = "<i class='info-icon icon-#{event_info.title}'></i>#{btn_title}".html_safe
      else
        wrapper_html[:class] ||= ''
        wrapper_html[:class] << ' hover'
        content = "<i class='info-icon icon-#{event_info.title}'></i>#{btn_title}".html_safe
      end

      result << default(content, wrapper_html)
      result
    end.html_safe
  end

  def default(content, wrapper_html = {})
    @content = content
    @wrapper_html = wrapper_html
    @wrapper_html[:class] ||= ''
    @wrapper_html[:class] << ' fbtn-square-info'
    render view: 'icon'
  end

end