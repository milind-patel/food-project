class BlogCell < Cell::Rails
  helper ApplicationHelper
  append_view_path "app/views"

  def blog_entry_teaser(blog_entry)
    @blog_entry = blog_entry
    render
  end

  def blog_entry_teaser_show(blog_entry)
    @blog_entry = blog_entry
    render
  end

  def element(element)
    if element.is_a?(BlogElement::Text)
      element_text(element)
    elsif element.is_a?(BlogElement::Image)
      element_image(element)
    end
  end

  def element_text(element)
    @element = element
    render view: 'element_text'
  end

  def element_image(element)
    @element = element
    render view: 'element_image'
  end

end
