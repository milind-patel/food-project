class ButtonCell < Cell::Rails
  def booking(url, wrapper_html = {})
    if wrapper_html[:class].present?
      wrapper_html[:class] << ' booking-btn'
    else
      wrapper_html[:class] = 'booking-btn'
    end

    @url = url
    @wrapper_html = wrapper_html

    render
  end

  def small(content, url, wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' fbtn-small'
    default(content, url, wrapper_html)
  end
  def small_button(content, wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' fbtn-small'
    default_button(content, wrapper_html)
  end

  def medium(content, url, wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' fbtn-larger'
    default(content, url, wrapper_html)
  end
  def medium_button(content, wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' fbtn-larger'
    default_button(content, wrapper_html)
  end

  def large(content, url, wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' fbtn-large'
    default(content, url, wrapper_html)
  end
  def large_button(content, wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' fbtn-large'
    default_button(content, wrapper_html)
  end

  def square(content, url, wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' fbtn-square'
    default(content, url, wrapper_html)
  end
  def square_button(content, wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' fbtn-square'
    default_button(content, wrapper_html)
  end

  def checkbox_round(wrapper_html = {})
    wrapper_html[:class] ||= ''
    wrapper_html[:class] << ' checkbox-round'
    default_button("", wrapper_html)
  end

  def default(content, url, wrapper_html = {})
    @url = url
    @content = content
    @wrapper_html = wrapper_html
    @wrapper_html[:class] ||= ''
    @wrapper_html[:class] << ' fbtn'
    render view: 'button'
  end
  alias :normal :default

  def default_button(content, wrapper_html = {})
    @content = content
    @wrapper_html = wrapper_html
    @wrapper_html[:class] ||= ''
    @wrapper_html[:class] << ' fbtn'
    render view: 'button_button'
  end
  alias :normal_button :default_button


end
