class EventCell < Cell::Rails
  append_view_path "app/views"

  def event_appointment_box(event_appointment, extended_view, show_participations, sold = false)
    @appointment = event_appointment.decorate
    @event = @appointment.event.decorate
    @owner = @appointment.event.owner.decorate if @event.owner

    @state_css_class = sold ? 'sold' : ''
    @state_css_class = 'done' if event_appointment.closed?

    @description = ActionView::Base.full_sanitizer.sanitize(@event.description)
    @price = (@event.price/100).to_i
    accepted_user_ids = @appointment.participations.active_ones.collect {|i| i.user_id}
    @guests = ::User.where(:id => accepted_user_ids)
    @cancellation = false
    @extended_view = extended_view
    @show_participations = show_participations

    @reviews = "#{@owner.ratings_description}"
    @percentage = @owner.aggregated_rating_total

    @contactable = false

    render view: 'box'
  end

  def google_map(event, display_circle = false)
    @latitude = display_circle ? event.fake_latitude : event.latitude
    @longitude = display_circle ? event.fake_longitude : event.longitude
    @display_circle = display_circle

    render
  end

  def participated_event_appointment_box(event_appointment, extended_view)
    @appointment = event_appointment.decorate
    @event = @appointment.event.decorate
    @owner = @appointment.event.owner.decorate if @event.owner

    @description = ActionView::Base.full_sanitizer.sanitize(@event.description)
    @price = (@event.price/100).to_i
    accepted_user_ids = @appointment.participations.active_ones.collect {|i| i.user_id}
    @guests = ::User.where(:id => accepted_user_ids)
    @cancellation = !event_appointment.past?
    @extended_view = extended_view

    @reviews = "#{@owner.ratings_description}"
    @percentage = @owner.aggregated_rating_total

    @contactable = true

    render view: 'box'
  end

  def event_data(event)
    @event = event.decorate

    render
  end

end
