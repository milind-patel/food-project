# encoding: utf-8
class ParticipationCell < Cell::Rails
  include Devise::Controllers::Helpers

  def show(participation)
    @participation = participation
    @event_appointment = participation.event_appointment.decorate

    render
  end

  # states
  # args
  #  @event_appointment
  #  @participation

  def main_content
    if show_message?
      @message = create_message

      render view: 'message'
    else
      render view: 'person_selection'
    end
  end

  def create_message
    if @event_appointment.past?
      return 'Das Event ist bereits abgelaufen.'
    end

    if @event_appointment.participation_past?
      return 'Das Event ist nicht mehr buchbar.'
    end

    if @participation.persisted?
      return 'Du nimmst bereist an dem Event teil.'
    end

    if @event_appointment.places_left <= 0
      return 'Event ist ausgebucht.'
    end

    event = @event_appointment.event
    if event.homo?
      messages = ['Dieses Event kann nicht von dir besucht werden, da']

      if current_user.male? && event.homo_female?
        messages << 'nur Frauen daran teilnehmen dürfen.'
      elsif current_user.female? && event.homo_male?
        messages << 'nur Männer daran teilnehmen dürfen.'
      else
        return nil
      end

      return messages.join(' ')
    end

    return nil
  end

  def head
    render
  end

  def person_selection
    render
  end

  private

  def show_message?
    create_message.present?
  end
end
