class UserCell < Cell::Rails
  include ActionView::Helpers::AssetUrlHelper
  include Devise::Controllers::Helpers
  helper_method :profile_editable?

  def profile(user)
    @user = user.decorate
    @flash = parent_controller.flash

    render
  end

  def profile_editable?(user)
    current_user && user.id == current_user.id
  end

  def edit_languages_index(user_languages)
    @languages = user_languages

    render
  end

  # state
  def main_image
    @image = @user.profile_image || 'user/default-profile.jpg'

    render
  end

  def horizontal_image_slider
    @images = @user.images

    if @images.present?
      render
    end
  end
end
