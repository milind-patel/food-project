class GalleryCell < Cell::Rails
  def show(object, type)
    @object = object

    render view: "#{type.to_s}_show"
  end

  def image_container(image)
    @image = image

    is_main_image = @user.try(:main_image).try(:id) == image.try(:id) ? true : false

    @additional_class = is_main_image ? 'highlight' : ''

    unless is_main_image
      @data_attributes = {
        placement: 'right',
        toggle: 'tooltip',
        title: I18n::t('internal.profile.edit.set_as_main_image'),
        container: 'body'
      }
    else
      @data_attributes = {
        placement: 'right',
        toggle: 'tooltip',
        title: I18n::t('internal.profile.edit.is_main_image'),
        container: 'body'
      }
    end

    @data_attributes_delete = {
      placement: 'left',
      toggle: 'tooltip',
      title: I18n::t('internal.profile.edit.delete_image'),
      container: 'body'
    }

    render
  end

end
