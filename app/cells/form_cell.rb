#
# Cell methods
#
# - button_radios
#
#
class FormCell < Cell::Rails
  include ActionView::Helpers::FormTagHelper
  include Cells::Rails::ActionView

  # ----------------------------------------------
  # button_radios

  public

  def repetition_inputs(form, event_appointment = nil, index = nil)
    @event_appointment = event_appointment
    @form = form
    @index = index

    render
  end

  def add_repetition(form)
    @form = form
    @index = rand(20..99999999999)
    @event_appointment = form.object.event_appointments.build

    render
  end

  # cell
  def radios(form, name, attributes = {})
    @form = form
    @name = name
    @attributes = attributes
    @attributes[:as] = :radio_buttons
    @attributes[:input_html] ||= {}
    @attributes[:input_html][:class] ||= 'fbtn'

    unless @attributes[:show_label].nil?
      @show_label = @attributes[:show_label]
    else
      @show_label = true
    end

    if @show_label == false
      @attributes[:input_html][:class] << ' show-label-false'
    end

    render
  end

  def checkboxes(form, name, attributes = {})
    @form = form
    @name = name
    @attributes = attributes
    @attributes[:input_html] ||= {}
    @attributes[:input_html][:class] ||= ''

    unless @attributes[:show_label].nil?
      @show_label = @attributes[:show_label]
    else
      @show_label = true
    end


    if @show_label == false
      @attributes[:input_html][:class] << ' show-label-false'
    end

    render
  end

  def boolean(form, name, attributes = {})
    @form = form
    @name = name
    @attributes = attributes.merge(as: :boolean)

    render
  end

  # checkbox
  def simple_checkbox(form, name, attributes)
    @form = form
    @name = name
    @attributes = attributes
    @attributes[:as] = :check_boxes

    render
  end

  def custom_checkboxes(type, name, collection, value)
    buttons = ''
    collection.each do |k,v|
      active_class = v.eql?(value) ? 'active' : ''
      buttons << render_cell(:button, type, class: active_class, data: { value: v })
    end
    buttons.html_safe
  end

  #radios
  def simple_radio(form, name, attributes)
    @error = @form.error(@name)
    @form = form
    @name = name
    @show_label = attributes[:label] == false ? false : true
    @attributes = attributes
    @css_class = "event_#{@name}"

    if @error.present?
      @css_class << " has-error"
    end

    render
  end

  def custom_radios(type, name, collection, value)
    buttons = ''
    collection.each do |k,v|
      active_class = v.eql?(value) ? 'active' : ''
      buttons << render_cell(:button, type, k, '#', class: active_class, data: { value: v })
    end
    buttons.html_safe
  end

end
