class EventAppointmentCell < Cell::Rails

  def city_dropdown(search_attribute, data)
    @id = "city-dropdown"
    @strong_text = t('public.cities.strong_text')
    @search_attribute = search_attribute
    @data = data

    if @search_attribute.present?
      @button_text = @search_attribute.humanize
    else
      @button_default_text = City.first.name
    end

    render view: 'button-dropdown'
  end

  def category_dropdown(search_attribute, data)
    @id = "category-dropdown"
    @strong_text = t('public.categories.strong_text')
    @search_attribute = search_attribute
    @data = data

    if @search_attribute.present?
      @button_text = t("public.categories.#{@search_attribute}")
    else
      @button_default_text = t('public.categories.default_value')
    end

    render view: 'button-dropdown'
  end

  def dates_dropdown(search_attribute, data)
    @id = "dates-dropdown"
    @strong_text = t('public.dates.strong_text')
    @search_attribute = search_attribute
    @data = data

    if @search_attribute.present?
      @button_text = l(@search_attribute.to_time, :format => '%d. %B %Y')
    else
      @button_default_text = t('public.dates.default_value')
    end

    render view: 'button-dropdown'
  end

  def button_dropdown(search_attribute, data, id, label)
    @id = id
    @strong_text = label
    @search_attribute = search_attribute
    @data = data

    if @search_attribute.present?
      @button_text = l(@search_attribute.to_time, :format => '%d. %B %Y')
    else
      @button_default_text = t('public.dates.default_value')
    end

    render view: 'button-dropdown'
  end

  def participations(participations)
    @participations = participations

    if @participations.count > 0
      render
    end
  end

  def participation(participation)
    @event_appointment = participation.event_appointment
    @participation = participation

    render
  end
end
