module ApplicationHelper
  def current_page_class(path)
    current_page?(path) ? 'active' : ''
  end

  def mail_to_email
    Settings.mailer_to
  end

  def facebook_url
    "https://www.facebook.com/pages/Foodoode/1485041008385191"
  end

  def twitter_url
    "https://twitter.com/foodoo_DE"
  end

  def google_plus_url
    "https://plus.google.com/104211048612947730406"
  end


  def absolute_attachment_url(attachment_name, attachment_style = :medium)
    "#{request.protocol}#{request.host_with_port}#{attachment_name.url(attachment_style)}"
  end

end
