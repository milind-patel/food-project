# ready function
$(document).on 'page:change', ->

  if $('.blog-entry .teaser-text').length > 0
    $('.blog-entry .teaser-text').dotdotdot(
      ellipsis: '... ',
      wrap: 'word',
      fallbackToLetter: true,
      after: null,
      watch: false
    )

  # ---------------------------------
  # endless loading
  if $('body.public-blogentries').length > 0
    loader = new Foodoo.BlogEntriesLoader('#blog-entries', 'indexBlogEntriesSpinner')

    $('#more-blog-entries-button').on 'click', (event) ->
      event.preventDefault()
      loader.loadNextPage()