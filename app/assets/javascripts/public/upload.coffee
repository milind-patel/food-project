# namespace
window.Foodoo or= {}

# order form handler
class Foodoo.Upload
  constructor: (@callback)->
    self = this
    $('#image-upload-btn-ready').fadeOut()

    $('#image_uploaded_file').attr('name','upload[uploaded_file]')
    $('#fileupload').fileupload({autoUpload: true, dropZone: $(this)})
      .bind('fileuploadsubmit', (e, data)->
        $('.template-upload').find('.spinner').css('opacity', '1')
      )
      .bind('fileuploaddone', (e, data)->
        $('#image-upload-btn-ready').fadeIn()
      )

    $('#image-upload-modal').on 'hidden.bs.modal', (e)->
      $('#image-upload-btn-ready').fadeOut()

      downloads = $('.template-download')
      if downloads.length > 0
        downloads.remove()
        url = $(this).data('ajax-reload-url')
        container = $(this).data('ajax-reload-container')

        $.ajax(
          type: 'GET'
          url: url
          dataType: 'html'
        ).done((response_data) ->
          $("#{container}").html(response_data)
          self.callback()
        )


  # if galleryType == 'vertical'
  #   gallery = new VerticleGallery(container, "vertical")
  # else if galleryType == 'horizontal'
  #   gallery = new HorizontalGallery(container, "horizontal")

  # if gallery != null
  #   gallery.start()
