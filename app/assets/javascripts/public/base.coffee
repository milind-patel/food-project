#
#= require plugins
#= require turbolinks
#= require jquery
#= require jquery_ujs
#= require jquery-ui
#= require jquery-fileupload
#= require jquery.spin
#= require jquery.icheck
#= require jquery.dotdotdot
#= require twitter/bootstrap
#= require bootstrap
#= require moment
#= require bootstrap-datetimepicker
#= require slick.min
#= require placeholder
#= require nprogress

#= require moment/de
#= require locales/bootstrap-datetimepicker.de.js

#= require parameterize
#= require tinymce-jquery
#= require purl
#= require bootstrap-select
#
#= require_self
#
#= require_tree ./class
#= require public/google_maps
#= require public/event_appointment_maps
#= require public/gallery
#= require public/horizontal_gallery
#= require public/verticle_gallery
#= require public/form_elements
#= require public/event_appointments
#= require public/upload
#= require public/events
#= require public/user
#= require public/blog
#= require social-share-button
#

# nprogress hooks
$(document).on 'page:fetch', -> NProgress.start()
$(document).on 'page:change', -> NProgress.done()
$(document).on 'page:restore', -> NProgress.remove()

# ready function
$(document).on 'page:change', ->

  NProgress.done()

  $('form').on 'submit', (event) ->
    NProgress.start()

  # footer scroll to top
  $('footer li.top a').click ->
    $('body,html').animate(
      scrollTop: 0
    , 300)

  # login modal
  $('body').delegate '.link-login-popup', 'click', (event)->
    event.preventDefault()
    href = $(this).attr('href')

    $.ajax(
      url: href
      dataType: 'html'
      type: 'GET'
      data: { modal: true }
    ).done((response_data) ->
      $('#login-modal').html(response_data)
      $('#login-modal').modal('show')
    ).fail((response_data) ->
      location.href = href
    )
  $('#login-modal').delegate '.create a', 'click', (event)->
    event.preventDefault()
    href = $(this).attr('href')
    request = $.ajax(
      url: "#{href}?modal=true"
      dataType: 'html'
      type: 'GET'
    ).done((response_data) ->
      $('#login-modal')
      $('#login-modal').html(response_data)
    ).fail((response_data) ->
      location.href = href
    )

  eventAppointmentParticipation = new Foodoo.EventAppointmentParticipation
  eventAppointmentParticipation.bindButtonHandler()
  eventAppointmentParticipation.bindDropdownHandler()

  eventCountdown = new Foodoo.Event.Countdown

  $('body').delegate '#contact-btn', 'click', (event)->
    event.preventDefault()
    href = $(this).attr('href')
    console.log href

    $.ajax(
      url: href
      dataType: 'html'
      type: 'GET'
      data: { modal: true }
    ).done((response_data) ->
      $('#contact-modal').html(response_data)
      $('#contact-modal').modal('show')
    ).fail((response_data) ->
      location.href = href
    )

  $('[data-toggle="tooltip"]').tooltip
    animated: 'fade',
    placement: 'right',

  # selectpicker
  $('.selectpicker').selectpicker()

  $('.text-wrapper h2').dotdotdot(
    ellipsis: '...',
    wrap: 'letter',
    fallbackToLetter: true,
    tolerance: 5
  )

  $('.pictures').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    fade: true,
    speed: 4000,
    cssEase: 'ease-in'
  })

  # popup link
  $("a[popup=true]").on "click", (e) ->
    window.open $(this).attr("href"), "Popup", "height=600, width=900"
    e.preventDefault()



