# ready function
$(document).on 'page:change', ->

  if $('.internal-users.show').length > 0 || $('.public-users.show').length > 0
    userProfile = new Foodoo.UserProfileShow()
    userProfile.run()
    new Foodoo.UserDescriptionHandler()


  if $('.internal-users.edit').length > 0
    userProfile = new Foodoo.UserProfileEdit()
    userProfile.run()

    form = $('#edit_user_form')
    languages_handler = new Foodoo.User.LanguagesEdit(form)

  # $('#contact-modal').modal(
  #   backdrop: true
  #   keyboard: true
  #   show: true
  # )

    # $('#add-language-button').on 'click', (event) ->
    #   event.preventDefault()
    #   url = $(this).attr('href')

    #   current_languages = []
    #   $('.languages .language').each ->
    #     language_id = $(this).find('input.language-id').val()
    #     language_strength = $(this).find('select.language-strength').val()
    #     current_languages.push({
    #       language_id: language_id
    #       language_strength: language_strength
    #     })

    #   $.ajax(
    #     url: "#{url}?modal=true"
    #     dataType: 'html'
    #     type: 'GET'
    #     data: { existing_languages: current_languages }
    #   ).done((response_data) ->
    #     modal = $('#add-language-modal')
    #     modal.html(response_data)
    #     modal.modal(
    #       backdrop: true
    #       keyboard: true
    #       show: true
    #     )
    #     # _this.modal.find('button.selectpicker').focus()

    #     modal.find('#create-btn').on 'click', (event) ->
    #       event.preventDefault()
    #       url = modal.find('form').attr('action')
    #       method = modal.find('form').attr('method')
    #       data = modal.find('form').serialize()
    #       # _this.showSpinner()
    #       # $.ajax(
    #       #   url: url
    #       #   data: data
    #       #   dataType: 'json'
    #       #   type: method.toUpperCase()
    #       # ).done((response_data) ->
    #       #   $('.languages').html($(response_data.content).find('.languages').html())
    #       #   modal.modal('hide')
    #       # )

    #   )
