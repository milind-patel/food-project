# ready function
$(document).on 'page:change', ->

  # ----------------------------------------
  # radio buttons

  button_radio_button_set = (sender, value)->
    sender.parents('.button-radios').find('.radios input').prop('checked', false)
    sender.parents('.button-radios').find('.buttons a').removeClass('active')
    sender.parents('.button-radios').find(".radios input[value='#{value}']").prop('checked', true)
    sender.parents('.button-radios').find(".buttons a[data-value='#{value}']").addClass('active')

  $('.button-radios .buttons').delegate 'a', 'click', (event) ->
    event.preventDefault()
    button_radio_button_set($(this), $(this).data('value'))
  $('.button-radios .radios').delegate 'input:radio', 'change', (event) ->
    button_radio_button_set($(this), $(this).val())