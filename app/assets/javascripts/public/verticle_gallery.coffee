class window.VerticleGallery extends window.Gallery
  previous: (offset, duration=200) ->
    top = parseInt(@inside().css("top"))
    max_top_offset = @images_container().height() - @inside().height()

    if top >= max_top_offset
      if (top - offset) < max_top_offset
        @arrow_hide(@arrow_previous())
        @inside().stop().animate({top:"#{max_top_offset+1}px"}, duration)
      else
        @arrow_show(@arrow_next())
        @inside().stop().animate({top:"-=#{offset}"}, duration)

    else
      @arrow_hide(@arrow_next())
      @inside().stop().animate({top:"-1px"}, duration)

  next: (offset, duration=200) ->
    top = parseInt(@inside().css("top"))

    if top < -1
      @arrow_show(@arrow_previous())
      if (top + offset) >= -1
        @inside().stop().animate({top:"-1px"}, duration)
      else
        @inside().stop().animate({top:"+=#{offset}"}, duration)
    else
      @arrow_hide(@arrow_next())
      @inside().stop().animate({top:"-1px"}, duration)
