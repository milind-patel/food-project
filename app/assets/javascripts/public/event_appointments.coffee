# ready function
$(document).on 'page:change', ->

  # ---------------------------------
  # selectpicker
  $('.selectpicker').each ->
    $(this).selectpicker()

  $('a#city-dropdown').parents('.btn-group').find('.dropdown-menu li').click ->
    selText = $(this).text()
    $('#city').val($(this).data("id"))
    $(this).parents('.btn-group').find('.dropdown-toggle').html('<strong>WO? </strong>' + selText+' <span class="caret"></span>')

  $('a#category-dropdown').parents('.btn-group').find('.dropdown-menu li').click ->
    selText = $(this).text()
    $('#category').val($(this).data("id"))
    $(this).parents('.btn-group').find('.dropdown-toggle').html('<strong>WAS? </strong>' + selText+' <span class="caret"></span>')

  # ---------------------------------
  # filtered events scrolling

  # if $('.container.events.filtered').length != 0
  #   $('html, body').animate
  #     scrollTop: $("#categories-wrapper").offset().top
  #   , 300

  today = new Date();

  $('.dropdown-wrapper [data-toggle="datepicker"]').datetimepicker(
    pick12HourFormat: false
    pickTime: false
    language: 'de'
    format: 'DD.MM.YY'
    minDate: "#{today.getMonth()+1}/#{today.getDate()}/#{today.getFullYear()}"
  )

  $('.dropdown-wrapper [data-toggle="datepicker"]').on 'dp.show', (e)->
    console.log 'HIDE'
    $('a#city-dropdown').parent().removeClass('open')
    $('a#category-dropdown').parent().removeClass('open')


  $('.dropdown-wrapper [data-toggle="datepicker"]').on 'dp.change', (e)->
    date = e.date.format("DD.MM.YYYY")

    $(this).html("<strong>Wann?</strong> #{date}")
    $('#date').val(date)

  # ---------------------------------
  # ajax reload
  loader = new Foodoo.EventAppointmentLoader('#indexEventsContainer', 'indexEventsSpinner')

  $('#more-events-button').on 'click', (event) ->
    event.preventDefault()
    loader.loadNextPage()

  $('.bootstrap-select').on 'hidden.bs.dropdown', (e)->
    calculatePrice()


  # ---------------------------------
  # headline and icons clicking
  $('.icon#meeting-people-icon').click (e) ->
    e.preventDefault()
    selectMeetingPeople()

  $('.text #meeting-people-text').click (e) ->
    e.preventDefault()
    selectMeetingPeople()

  $('.icon#dating-dinner-icon').click (e) ->
    e.preventDefault()
    selectDatingDinner()

  $('.text #dating-dinner-text').click (e) ->
    e.preventDefault()
    selectDatingDinner()

  $('.icon#cooking-course-icon').click (e) ->
    e.preventDefault()
    selectCookingCourse()

  $('.text #cooking-course-text').click (e) ->
    e.preventDefault()
    selectCookingCourse()

  selectMeetingPeople = ->
    $('#category').val("people")
    selText = $('a#category-dropdown').parents('.btn-group').find('.dropdown-menu li:nth-child(2)').text()
    $('a#category-dropdown').parents('.btn-group').find('.dropdown-toggle').html('<strong>WAS? </strong>' + selText+' <span class="caret"></span>')
    $('#new_search_request').submit()

  selectDatingDinner = ->
    $('#category').val("dating")
    selText = $('a#category-dropdown').parents('.btn-group').find('.dropdown-menu li:nth-child(3)').text()
    $('a#category-dropdown').parents('.btn-group').find('.dropdown-toggle').html('<strong>WAS? </strong>' + selText+' <span class="caret"></span>')
    $('#new_search_request').submit()

  selectCookingCourse = ->
    $('#category').val("course")
    selText = $('a#category-dropdown').parents('.btn-group').find('.dropdown-menu li:nth-child(4)').text()
    $('a#category-dropdown').parents('.btn-group').find('.dropdown-toggle').html('<strong>WAS? </strong>' + selText+' <span class="caret"></span>')
    $('#new_search_request').submit()

  calculatePrice = ->
    multiplier = 0

    $('.bootstrap-select').find('.selectpicker .selected').each ->
      multiplier += parseInt($(this).attr('rel'))

    price = parseFloat($('.guest-count').data('price-per-person'))
    sumPrice = (multiplier + 1) * price

    $('.sum-price p').text("#{sumPrice.toFixed(2)}€")

  calculatePrice()
