# ready function
$(document).on 'page:change', ->

  markers = []
  global_map = null

  cleanUpMarker = ->
    for m in markers
      m.setMap(null)
    markers = []

  placeMarker = (map, location) ->
    cleanUpMarker()

    marker = new google.maps.Marker
      map: map
      position: location
      icon: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png'

    setLat(location.lat())
    setLng(location.lng())

    markers.push(marker)

  filterCityDistrict = (result) ->
    for component in result.address_components
      if component.types.indexOf('sublocality') > -1
        setDistrictValue(component.long_name)
        break

  setDistrictValue = (value) ->
    $('input[name="event[address_district]"]').val(value)

  codeAddress = ->
    address = "Deutschland #{$('#event_address_street').val()} #{$('#event_address_zip').val()} #{$('#event_address_city').val()}"
    geocoder = new google.maps.Geocoder()
    geocoder.geocode
      address: address
    , (results, status) ->
      if status is google.maps.GeocoderStatus.OK
        placeMarker(global_map, results[0].geometry.location)
        global_map.setCenter(results[0].geometry.location)
        filterCityDistrict(results[0])

  initMap = (show_circle) ->
    location = new google.maps.LatLng(lat_data_attr($('#google-map')), lng_data_attr($('#google-map')))
    mapOptions = {
      zoom: 13
      center: location
      panControl: false
      zoomControl: true
      scaleControl: true
      mapTypeControl: true
      rotateControl: true
      overviewMapControl: true
      streetViewControl: true
      scrollwheel: true
      draggable: true
      disableDoubleClickZoom: false
    }
    map = new google.maps.Map($('#google-map')[0], mapOptions)

    if show_circle == true
      circle = new google.maps.Circle
        center: location,
        radius: 1000,
        fillColor: "#76b702",
        fillOpacity: 0.4,
        strokeColor: '#76746e'
        strokeOpacity: 0.7,
        strokeWeight: 1,
        map: map
    else
      placeMarker(map, location)

  initMapWithoutCircle = ->
    initMap(false)

  initMapWithCircle = ->
    initMap(true)

  initEventCreateMap = ->
    location = new google.maps.LatLng(lat(),lng())
    mapOptions = {
      zoom: 13
      center: location
      panControl: false
      zoomControl: true
      scaleControl: false
      mapTypeControl: true
      rotateControl: false
      overviewMapControl: false
      streetViewControl: false
      scrollwheel: true
      draggable: true
      disableDoubleClickZoom: false
    }
    map = new google.maps.Map($('#event-create-google-map')[0], mapOptions)

    placeMarker(map, location)

    google.maps.event.addListener map, 'click', (e) ->
      placeMarker(map, e.latLng)

    global_map = map

  lat = ->
    $('#event_latitude').val()
  lng = ->
    $('#event_longitude').val()

  lat_data_attr = (map_sel) ->
    map_sel.data("latitude")

  lng_data_attr = (map_sel) ->
    map_sel.data("longitude")

  setLat = (lat) ->
    $('#event_latitude').val(lat)

  setLng = (lng) ->
    $('#event_longitude').val(lng)


  if $('#google-map.detailed').length > 0
    window.addMapsLoadCallback(initMapWithoutCircle)

  if $('#google-map.undetailed').length > 0
    window.addMapsLoadCallback(initMapWithCircle)

  if $('#event-create-google-map').length > 0
    window.addMapsLoadCallback(initEventCreateMap)

  $('#event_address_city').focusout ->
    codeAddress()
  $('#event_address_street').focusout ->
    codeAddress()
  $('#event_address_zip').focusout ->
    codeAddress()


