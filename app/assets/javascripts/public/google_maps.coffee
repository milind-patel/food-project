map_load_callbacks = []
map_loaded = false

# maps loaded callback
window.mapInitializeCallback = ->
  map_loaded = true
  for fct in map_load_callbacks
    fct()

# maps loaded callback registraion
window.addMapsLoadCallback = (fct)->
  map_load_callbacks.push(fct)
  fct() if map_loaded == true


$ ->
  api_key = $('body').data('google-maps-api-key')
  script = document.createElement('script')
  script.type = 'text/javascript'
  script.src = "https://maps.googleapis.com/maps/api/js?key=#{api_key}&sensor=false&callback=window.mapInitializeCallback"
  document.body.appendChild(script)
