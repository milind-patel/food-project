# namespace
window.Foodoo or= {}

# event description handler
class Foodoo.EventDescriptionHandler

  constructor: ->
    @originalContent = @getTruncationContainer().html()
    @getForcedHeight() # initial calculation
    @dotdotdot()
    @bindEvents()

  dotdotdot: ->
    @getTruncationContainer().dotdotdot
      callback: (isTruncated, orgContent) =>
        if isTruncated == true
          @getReadmoreButton().show()

  bindEvents: ->
    @getReadmoreButton().on "click", (e) =>
      e.preventDefault()
      @showText()
    @getReadlessButton().on "click", (e) =>
      e.preventDefault()
      @hideText()

  hideText: ->
    @getTruncationContainer().css('height', @getForcedHeight())
    @dotdotdot()
    @getReadmoreButton().show()
    @getReadlessButton().hide()

  showText: ->
    @getTruncationContainer().trigger('destroy')
    @getTruncationContainer().css('height': 'auto')
    @getTruncationContainer().html(@originalContent)
    @getReadmoreButton().hide()
    @getReadlessButton().show()

  getTruncationContainer: ->
    @truncationContainer or= $('.description-text-wrapper .description-text')

  getForcedHeight: ->
    @forcedHeight or= @getTruncationContainer().height()

  getReadmoreButton: ->
    @readmoreButton or= $('.description-text-wrapper a.readmore')

  getReadlessButton: ->
    @readlessButton or= $('.description-text-wrapper a.readless')

