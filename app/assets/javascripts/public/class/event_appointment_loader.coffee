# namespace
window.Foodoo or= {}

# order form handler
class Foodoo.EventAppointmentLoader

  constructor: (mainContainer, spinnerId)->
    @setLastLoadedPage(1)
    @setMainContainer($(mainContainer))
    @bindNewsletter()
    @bindNewsletterPopup()

  loadNextPage: ->
    page = @getLastLoadedPage() + 1
    @loadPage(page)

  loadPage: (page)->
    @sendIndexPageRequest(page)

  sendIndexPageRequest: (page)->
    self = this

    @showSpinner()
    @hideMoreButton()

    $.getJSON "/?page=#{page}", self.getFilterParams(),  (response)->
      html = $(response['html'])
      more_elements = response['more_elements']
      self.hideSpinner()

      if more_elements == true
        self.setLastLoadedPage(page)
        self.showMoreButton()

      # filter headline
      last_date = self.getMainContainer().find('.headline.index_show_element').last().data('date')
      first_new = html.first()
      window.html = html
      if first_new.hasClass('headline') && first_new.data('date') == last_date
        html = html.slice(1, html.length)

      # add to dom
      self.appendWithHtml(html)

      # rebind newsletter stuff
      self.bindNewsletter()

  bindNewsletter: ->
    @handler or= new Foodoo.NewsletterLineHandler('.newsletter.index_show_element')
    @handler.restart()

  bindNewsletterPopup: ->
    _this = @
    $('#more-events-button').on 'click', ->
      if $('.newsletter.index_show_element').length > 0
        $('#newsletter-popup-modal').modal()
        @handler_popup or= new Foodoo.NewsletterLineHandler('#newsletter-popup-modal')
        @handler_popup.successCallback(->
          $('#newsletter-popup-modal').modal('hide')
          $('.modal-backdrop').remove()
          if _this.handler?
            _this.handler.remove_newsletter_rows()
        )
        @handler_popup.restart()


  showSpinner: ->
    $(@getMoreButton()).before(
      "<div id='#{@getSpinnerId()}'></div>"
    )

    $("##{@getSpinnerId()}").spin(@getSpinnerOptions())

  hideSpinner: ->
    $("##{@getSpinnerId()}").remove()

  appendWithHtml: (html) ->
    # append
    @getMainContainer().find('.row').append(html)

    # dotdotdot for the headline
    $('.text-wrapper h2').dotdotdot(
      ellipsis: '...',
      wrap: 'letter',
      fallbackToLetter: true,
      tolerance: 5
    )

  addEventAppointmentToHtml: (eventAppointment)->
    self = this

    $(@getLastInsideMainContainer()).find('.row').append(eventAppointment.html)

    if $(@getLastInsideMainContainer()).find('.event_appointment_box').length % 2 == 0
      $(@getLastInsideMainContainer()).append($('<div class="clearfix"></div>'))

  addHeaderForDate: (date, html)->
    $(@getMainContainer()).append(html)
    $(@getMainContainer()).find('.headline-bar').data('date', date)
    $(@getMainContainer()).find('.headline-bar').last().after(
      "<div class='inside'>
        <div class='row'></div>
      </div>"
    )

  getMainContainer: ->
    @mainContainer

  setMainContainer: (container)->
    @mainContainer = container

  setSpinnerId: (spinnerId)->
    @spinnerId = spinnerId

  getSpinnerId: ->
    @spinnerId

  getLastInsideMainContainer: ->
     $(@getMainContainer()).find('.inside').last()

  getHeaders: ->
    $(@getMainContainer()).find('.headline-bar')

  getEventAppointments: ->
    @eventAppointments

  setEventAppointments: (eventAppointments)->
    @eventAppointments = eventAppointments

  getLastLoadedPage: ->
    @lastLaodedPage

  getFilterParams: ->
    $(@getMainContainer()).data()

  setLastLoadedPage: (page)->
    @lastLaodedPage = page

  getSpinnerOptions: ->
    {
      lines: 9     # The number of lines to draw
      length: 15    # The length of each line
      width: 2      # The line thickness
      radius: 9    # The radius of the inner circle
      corners: 1    # Corner roundness (0..1)
      rotate: 0     # The rotation offset
      direction: 1  # 1: clockwise, -1: counterclockwise
      color: '#000' # #rgb or #rrggbb or array of colors
      speed: 1      # Rounds per second
      trail: 31     # Afterglow percentage
      shadow: true  # Whether to render a shadow
      hwaccel: false    # Whether to use hardware acceleration
      className: 'spinner'    # The CSS class to assign to the spinner
      zIndex: 2e9   # The z-index (defaults to 2000000000)
      top: '30px'   # Top position relative to parent in px
      left: 'auto'  # Left position relative to parent in px
    }

  getMoreButton: ->
    '#more-events-button'

  hideMoreButton: ->
    $(@getMoreButton()).fadeOut(50)

  showMoreButton: ->
    $(@getMoreButton()).fadeIn(200)
