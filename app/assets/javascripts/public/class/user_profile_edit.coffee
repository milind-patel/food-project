# namespace
window.Foodoo or= {}

class Foodoo.UserProfileEdit

  constructor: ->

  run: ->
    # gallery = new Foodoo.Gallery.Vertical(@getSliderBox(), @getMainImageBox())
    # @setGalleryHandler(gallery)
    @start_image_gallery()


  # ---------------------------------
  # getter / setter

  getProfileBox: ->
    $('.internal-users.edit .form').first()

  getMainImageBox: ->
    @getProfileBox().find('.main-image')

  getSliderBox: ->
    @getProfileBox().find('.thumbnail-slider-vertical')


  getGalleryHandler: ->
    @galleryHandler or= null

  setGalleryHandler: (handler) ->
    @galleryHandler = handler



  start_image_gallery: ->
    verticleGallery = new VerticleGallery(".edit_user", "vertical")
    verticleGallery.start()

    new Foodoo.Upload ->
      (new Foodoo.UserProfile()).start_image_gallery()

    $('.internal-users.edit').find('.to-main-image').click (e)->
      e.preventDefault()

      tooltip_delete = $('.internal-users.edit #gallery').data('image-delete-tooltip')
      tooltip_is_main_image = $('.internal-users.edit #gallery').data('image-is-main-image-tooltip')
      tooltip_set_main_image = $('.internal-users.edit #gallery').data('image-set-main-image-tooltip')

      if !$(this).hasClass('highlight')
        $('.internal-users.edit').find('.to-main-image').removeClass('highlight')
        $('.internal-users.edit').find('.to-main-image').attr('title', tooltip_set_main_image).tooltip('fixTitle')

        $(this).addClass('highlight')
        $(this).attr('title', tooltip_is_main_image).tooltip('fixTitle').tooltip('show')

        $('.internal-users.edit #checkmark').fadeIn(300, ->
          $(this).fadeOut(1200)
        )

        old_image_id = $('#user_main_image_id').val()
        $('#user_main_image_id').val($(this).parent().data('image-id'))
        $('#user_main_image_id').data('old-image-id', old_image_id)

    $('.internal-users.edit').find('.btn.delete').click (e)->
      e.preventDefault()
      url = $(this).attr('href')
      image_container = $(this).parent()
      $.ajax url,
        type: 'DELETE'
        dataType:"json"
        contentType: 'application/json; charset=utf-8'
        success: (data, textStatus, jqXHR) ->
            image_container.fadeOut(400)
            large_image_url = image_container.find('img').data('url-large')
            main_image = $('.internal-users.edit .main_image')
            if main_image.attr('src') == large_image_url
              main_image.attr('src', '')
