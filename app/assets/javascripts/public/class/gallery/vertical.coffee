# namespace
window.Foodoo or= {}
window.Foodoo.Gallery or= {}

# order form handler
class Foodoo.Gallery.Vertical extends Foodoo.Gallery.Base

  getSlickConfig: ->
    if @slickDefaultConfig?
      @slickDefaultConfig
    else
      {
        accessibility: true
        autoplay: false
        arrows: true
        draggable: true
        infinite: false
        placeholders: false
        slidesToShow: 1
        slidesToScroll: 1
        swipe: true
        touchMove: true
        vertical: true
      }