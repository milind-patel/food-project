# namespace
window.Foodoo or= {}
window.Foodoo.Gallery or= {}

# order form handler
class Foodoo.Gallery.Base

  constructor: (@sliderContainer, @mainImageContainer, @slickDefaultConfig = null) ->
    @createSlickSlider()
    @bindHoverEvents()


  createSlickSlider: ->
    console.log @getSlickConfig()
    console.log @getSliderContainer()
    window.config = @getSlickConfig()
    window.container = @getSliderContainer()
    @getSliderContainer().slick(@getSlickConfig())

  bindHoverEvents: ->
    _this = this
    @getSliderContainer().delegate '.image', 'mouseenter', (event) ->
      url_large = $(this).find('img').data('url-large')
      _this.setMainImage(url_large)

  setMainImage: (url) ->
    @getImageContainer().attr('src', url)



  # ---------------------------------
  # getter / setter

  getSliderContainer: ->
    @sliderContainer

  getImageContainer: ->
    @mainImageContainer

  getSlickConfig: ->
    @slickDefaultConfig or= {}