# namespace
window.Foodoo or= {}
window.Foodoo.Gallery or= {}

# order form handler
class Foodoo.Gallery.Horizontal extends Foodoo.Gallery.Base

  getSlickConfig: ->
    if @slickDefaultConfig?
      @slickDefaultConfig
    else
      {
        accessibility: true
        autoplay: false
        arrows: true
        draggable: true
        infinite: false
        placeholders: false
        slidesToShow: 4
        slidesToScroll: 4
        swipe: true
        touchMove: true
      }