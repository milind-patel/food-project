# namespace
window.Foodoo or= {}

class Foodoo.UserProfileShow

  constructor: ->

  run: ->
    gallery = new Foodoo.Gallery.Horizontal(@getSliderBox(), @getMainImageBox())
    @setGalleryHandler(gallery)
    this.initEventLists()


  # ---------------------------------
  # getter / setter

  getProfileBox: ->
    $('.profile-infos').first()

  getMainImageBox: ->
    @getProfileBox().find('.main-image')

  getSliderBox: ->
    @getProfileBox().find('.thumbnail-slider-horizontal')


  getGalleryHandler: ->
    @galleryHandler or= null

  setGalleryHandler: (handler) ->
    @galleryHandler = handler


  # ----------------------------------
  # events upcoming/past

  initEventLists: ->
    #bind listener
    _this = this
    $('#upcoming_organized_events').on "click", (event) ->
      event.preventDefault()
      _this.showUpcomingOrganizedEvents()
    
    $('#upcoming_participated_events').on "click", (event) -> 
      event.preventDefault()
      _this.showUpcomingParticipatedEvents()

    $('#past_organized_events').on "click", (event) ->
      event.preventDefault()
      _this.showPastOrganizedEvents()

    $('#past_participated_events').on "click", (event) ->
      event.preventDefault()
      _this.showPastParticipatedEvents()

    # show upcoming events
    @showUpcomingOrganizedEvents()
    @showUpcomingParticipatedEvents()

  showUpcomingOrganizedEvents: ->
    $('.past.organized-events').hide()
    $('.upcoming.organized-events').show()
    $('a#past_organized_events').removeClass("active")
    $('a#upcoming_organized_events').addClass("active")

  showUpcomingParticipatedEvents: ->
    $('.past.participated-events').hide()
    $('.upcoming.participated-events').show()
    $('a#past_participated_events').removeClass("active")
    $('a#upcoming_participated_events').addClass("active")

  showPastOrganizedEvents: ->
    $('.upcoming.organized-events').hide()
    $('.past.organized-events').show()
    $('a#upcoming_organized_events').removeClass("active")
    $('a#past_organized_events').addClass("active")

  showPastParticipatedEvents: ->
    $('.upcoming.participated-events').hide()
    $('.past.participated-events').show()
    $('a#upcoming_participated_events').removeClass("active")
    $('a#past_participated_events').addClass("active")

