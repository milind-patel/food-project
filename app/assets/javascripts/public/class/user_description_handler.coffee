# namespace
window.Foodoo or= {}

# event description handler
class Foodoo.UserDescriptionHandler

  constructor: ->
    @originalContent = @getTruncationContainer().html()
    @getForcedHeight() # initial calculation
    @dotdotdot()
    @bindEvents()

  dotdotdot: ->
    @getTruncationContainer().dotdotdot
      callback: (isTruncated, orgContent) =>
        if isTruncated == true
          @getReadmoreButton().show()

  bindEvents: ->
    @getReadmoreButton().on "click", (e) =>
      e.preventDefault()
      @showText()
    @getReadlessButton().on "click", (e) =>
      e.preventDefault()
      @hideText()

  hideText: ->
    @getTruncationContainer().css('height', @getForcedHeight())
    @dotdotdot()
    @getReadmoreButton().show()
    @getReadlessButton().hide()

  showText: ->
    @getTruncationContainer().trigger('destroy')
    @getTruncationContainer().css('height': 'auto')
    @getTruncationContainer().html(@originalContent)
    @getReadmoreButton().hide()
    @getReadlessButton().show()

  getTruncationContainer: ->
    @truncationContainer or= $('.about-me .content')

  getForcedHeight: ->
    @forcedHeight or= @getTruncationContainer().height()

  getReadmoreButton: ->
    @readmoreButton or= $('.about-me a.readmore')

  getReadlessButton: ->
    @readlessButton or= $('.about-me a.readless')
