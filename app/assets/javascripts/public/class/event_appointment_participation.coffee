# namespace
window.Foodoo or= {}

# order form handler
class Foodoo.EventAppointmentParticipation
  bindButtonHandler: ->
    self = this

    $('.load-ajax-popup').on 'click', (event)->
      event.preventDefault()
      href = $(this).attr('href')

      $.ajax(
        url: "#{href}?as_modal=true"
        dataType: 'html'
        type: 'GET'
      ).done( (data)->
        self.modal().html(data)
        self.modal().modal('show')
        self.bindDropdownHandler()
      )

  bindDropdownHandler: ->
    self = @

    @modalSelectPicker().selectpicker()
    @calculatePrice()

    @mainContainer().find('.bootstrap-select').on 'hidden.bs.dropdown', (e)->
      self.calculatePrice()

  mainContainer: ->
    $('#event-participation-booking-box')

  modal: ->
    $('#appointment-modal')

  modalSelectPicker: ->
    @mainContainer().find('.selectpicker')

  calculatePrice: ->
    multiplier = 0

    @modalSelectPicker().find('.selected').each ->
      multiplier += parseInt($(this).attr('rel'))

    price = parseFloat($('.guest-count').data('price-per-person'))
    sumPrice = (multiplier + 1) * price
    cleanedPrice = sumPrice.toFixed(2)

    $('.sum-price p').text("#{cleanedPrice}€")

