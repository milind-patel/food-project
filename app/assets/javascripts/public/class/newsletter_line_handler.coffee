window.Foodoo or= {}

class Foodoo.NewsletterLineHandler

  constructor: (@selector)->

  start: ->
    @unbind()
    @bind_submit_action()
    @bind_city_select_action()

  restart: ->
    @start()

  unbind: ->
    @getRows().find('form').unbind()
    @getAllRows()

  bind_submit_action: ->
    _this = @
    @getRows().find('form').on 'submit', (event) ->
      event.preventDefault()
      $.post($(this).attr('action'), $(this).serialize(), ( (response) ->
        if response.success == true
          _this.successCallbackFunction?()
          _this.show_success_modal()
          _this.remove_newsletter_rows()
        else
          _this.handleErrors(response)
      ), 'json')

  bind_city_select_action: ->
    _this = @
    @getNewsletterSelects().each ->
      $(this).find('.dropdown-menu li').click ->
        selText = $(this).text()
        selId = $(this).data('id')
        $(this).parents(_this.getNewsletterSelectSelector()).find('.content').html(selText)
        $(this).parents('form').find(_this.getNewsletterCityInputSelector()).val(selId)


  # ----------------
  # callback

  successCallback: (func) ->
    @successCallbackFunction = func


  # ----------------
  # errors

  handleErrors: (response) ->
    if response.errors.name?
      @getNameInputs().addClass('has-error')
    else
      @getNameInputs().removeClass('has-error')

    if response.errors.email?
      @getEmailInputs().addClass('has-error')
    else
      @getEmailInputs().removeClass('has-error')

  # ----------------
  # success

  show_success_modal: ->
    _this = @
    $.get(@getNewsletterSuccessUrl(), null, (response) ->
      _this.getNewsletterSuccessModal().html(response)
      _this.getNewsletterSuccessModal().modal('show')
    , 'html')

  remove_newsletter_rows: ->
    @getAllRows().remove()


  # ----------------

  getRows: ->
    @newsletterRows or= $(@selector)

  getAllRows: ->
    @newsletterRows = $(@selector)
    @newsletterRows

  getNameInputs: ->
    @getRows().find('input.newsletter_name')

  getEmailInputs: ->
    @getRows().find('input.newsletter_email')

  getNewsletterSuccessModal: ->
    @newsletterSuccessModal or= $('#newsletter-success-modal')

  getNewsletterSuccessUrl: ->
    @getRows().first().data('success-url')

  getNewsletterSelects: ->
    @getRows().find(@getNewsletterSelectSelector())

  getNewsletterSelectSelector: ->
    '.newsletter_city_select'

  getNewsletterCityInputSelector: ->
    '#newsletter_subscription_city_id'
