window.Foodoo or= {}

class Foodoo.ICheckCheckbox
  constructor: (@element)->
    @assignHandler()

  assignHandler: ->
    self = this

    @label_elements().find('input').remove()

    $(@element).iCheck
      labelHover: false
      activeClass: 'active'
      cursor: true
      checkboxClass: 'icheckbox'
      radioClass: 'iradio'
      inheritClass: true
      insert: self.button_html()

    $(@element).on 'ifChecked', (event)->
      mainContainer = $(this).parent().parent().parent()
      self.button().addClass('active')

    $(@element).on 'ifUnchecked', (event)->
      self.button().removeClass('active')

  label_elements: ->
    @_label_elements or= $(@element).parent().clone()

  button: ->
    @_button or= $(@element).parent().find('button')

  button_html: ->
    if @is_bool()
      '<button class="' + @css_classes() + '"></button><p>' + @label_text() + '</p>'
    else
      '<button class="' + @css_classes() + '"><div class="left"></div><div class="content">' + @label_text() + '</div><div class="right"></div></button>'

  css_classes: ->
    classes = new Array()

    if $(@element).data('btn-type') == undefined
      classes.push('fbtn')
      classes.push('fbtn-default')
    else
      classes.push($(@element).data('btn-type'))

    if @is_checked()
      classes.push('active')

    classes.join(' ')

  label_text: ->
    $(@label_elements()).html()

  is_checked: ->
    $(@element).attr("checked") != "undefined" && $(@element).attr("checked") == "checked"

  is_bool: ->
    $(@element).hasClass('boolean')
