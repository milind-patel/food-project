# namespace
window.Foodoo or= {}
window.Foodoo.User or= {}

# order form handler
class Foodoo.User.LanguagesEdit

  constructor: (@form) ->
    @bindAddLanguageListener()
    @bindDeleteLanguageListeners()


  bindAddLanguageListener: ->
    _this = @

    @getAddLanguageButton().on 'click', (event) ->
      event.preventDefault()
      _this.updateLanguages()

      url = $(this).attr('href')

      $.ajax(
        type: 'GET'
        url: url
        dataType: 'html'
      ).done((response_data) ->
        _this.getModal().html(response_data)
        _this.getModal().modal(
          backdrop: true
          keyboard: true
          show: true
        )
        _this.bindModalButtons()
      )


  bindDeleteLanguageListeners: ->
    _this = @

    @getLanguagesRows().each ->
      delete_button = $(this).find('.button-delete')
      confirm_message = delete_button.data('confirm')
      url = delete_button.attr('href')

      delete_button.data('confirm', null)
      delete_button.data('method', null)

      delete_button.on 'click', (event) ->
        event.preventDefault()
        _this.updateLanguages()

        if confirm(confirm_message)
          $.ajax(
            type: 'DELETE'
            url: url
            dataType: 'json'
          ).done((response_data) ->
            _this.reloadLanguages()
          )

  bindModalButtons: ->
    _this = @

    @getModal().find('#create-btn').on 'click', (event) ->
      event.preventDefault()
      form = _this.getModal().find('form')
      url = form.attr('action')
      data = form.serialize()

      $.ajax(
        type: 'POST'
        url: url
        data: data
        dataType: 'json'
      ).done((response_data) ->
        _this.getModal().modal('hide')
        _this.reloadLanguages()
      )



  reloadLanguages: ->
    _this = @
    @showSpinner()

    $.ajax(
      type: 'GET'
      url: @getLanguagesIndexUrl()
      dataType: 'html'
    ).done((response_data) ->
      _this.getLanguagesContainer().html(response_data)
      _this.hideSpinner()
      _this.bindDeleteLanguageListeners()
    )

  updateLanguages: ->
    _this = @
    @showSpinner()
    data = $('#edit_user_form').serialize()

    $.ajax(
      type: 'PUT'
      url: '/profil/user/user_languages/update_all'
      data: data,
      dataType: 'json'
    ).done((response_data) ->
      _this.hideSpinner()
      console.log 'ready'
    )


  # --------------------------------------------
  # spinner

  showSpinner: ->
    @getLanguagesContainer().spin(@getSpinnerOptions())

  hideSpinner: ->
    @getLanguagesContainer().find('.spinner').hide()


  # --------------------------------------------
  # getter / setter

  getForm: ->
    @form

  getModal: ->
    @modal or= $('#add-language-modal')

  getLanguagesContainer: ->
    @languagesContainer or= @getForm().find('.languages')

  getAddLanguageButton: ->
    @addLanguageButton or= @getForm().find('#add-language-button')

  getLanguagesRows: ->
    @getLanguagesContainer().find('.language')

  getLanguagesIndexUrl: ->
    @languagesIndexUrl or= @getLanguagesContainer().data('url')

  getSpinnerOptions: ->
    {
      lines: 9     # The number of lines to draw
      length: 15    # The length of each line
      width: 2      # The line thickness
      radius: 9    # The radius of the inner circle
      corners: 1    # Corner roundness (0..1)
      rotate: 0     # The rotation offset
      direction: 1  # 1: clockwise, -1: counterclockwise
      color: '#000' # #rgb or #rrggbb or array of colors
      speed: 1      # Rounds per second
      trail: 31     # Afterglow percentage
      shadow: true  # Whether to render a shadow
      hwaccel: false    # Whether to use hardware acceleration
      className: 'spinner'    # The CSS class to assign to the spinner
      zIndex: 2e9   # The z-index (defaults to 2000000000)
      top: 'auto'   # Top position relative to parent in px
      left: 'auto'  # Left position relative to parent in px
    }
