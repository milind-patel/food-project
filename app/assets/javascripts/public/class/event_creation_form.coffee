# namespace
window.Foodoo or= {}

# order form handler
class Foodoo.EventCreationForm
  constructor: (@selector)->
    # @assignCustomButtons()
    # @assignHandler()
    @buildForm()

  buildForm: ->
    sexDirection = @activeSexDirection().val()

    @assigneSelectHandler()
    @assignSexDirectionHandler()
    @changeFormForSexDirection(sexDirection)
    @refreshForm()
    @assignDates()
    @assignDestroyHandler()
    @assignRepetitionHandler()
    @assigneMaxGuestCountHandler()

  assignDates:->
    today = new Date();
    mindate = "#{today.getMonth()+1}/#{today.getDate()}/#{today.getFullYear()}"

    $('input[data-behaviour="datepicker"]').datetimepicker
      pick12HourFormat: false
      pickTime: false
      language: 'de'
      format: 'DD.MM.YYYY'
      minDate: mindate,

    $('input[data-behaviour="timepicker"]').datetimepicker
      language: 'de'
      pick12HourFormat: false
      format: 'HH:mm'
      showToday: true
      pickDate: false
      minDate: mindate
      icons:
        time: "fa fa-clock-o"
        date: "fa fa-calendar"
        up: "fa fa-arrow-up"
        down: "fa fa-arrow-down"

    $('input[data-behaviour="timepicker"]').on 'dp.change', (e)->
      console.log "Time choosed: #{$(this).getDate()}"

  assigneSelectHandler: ->
    self = this

    $('input[type="radio"]').each ->
      input = this
      button = $(this).parent()

      if $(this).is(':checked')
        button.addClass('active')

      button.click (e)->
        self.uncheckAllRadiosByName("input[name=\"#{$(input).attr('name')}\"]")
        self.refreshForm()

      button.mouseleave (e)->
        e.preventDefault()

        if $(input).is(':checked')
          $(this).addClass('active')

    $('input[type="checkbox"]').each ->
      input = this
      button = $(this).parent()

      if $(this).is(':checked')
        button.addClass('active')
      else
        button.removeClass('active')


      if $(this).val() == 'true'
        button.addClass('active')
        $(this).attr('checked', true)

      button.click (e)->
        e.preventDefault()

        if button.hasClass('active')
          button.removeClass('active')
          $(input).attr('checked', false)
        else
          button.addClass('active')
          $(input).attr('checked', true)

  uncheckAllRadiosByName: (name)->
    $(name).each ->
      input = this
      $(this).parent().removeClass('active')

  assignSexDirectionHandler: ->
    self = this

    @sexDirection().each ->
      $(this).find('input[type="radio"]').click (e)->
        value = $(this).val()
        self.changeFormForSexDirection(value)

  assignDestroyHandler: ->
    @destroyButtons().show()
    @destroyButtons().first().hide()

    @destroyButtons().mouseenter (e)->
      $(this).removeClass('btn-default')
      $(this).addClass('btn-danger')

    @destroyButtons().mouseleave (e)->
      $(this).addClass('btn-default')
      $(this).removeClass('btn-danger')

    @destroyButtons().click (e)->
      e.preventDefault()
      $(this).parent().find('.hidden').val('true')
      $(this).parent().parent().fadeOut(400)

  assignRepetitionHandler: ->
    self = this

    @repetitionBtn().mouseenter (e)->
      $(this).removeClass('btn-default')
      $(this).addClass('btn-success')

    @repetitionBtn().mouseleave (e)->
      $(this).addClass('btn-default')
      $(this).removeClass('btn-success')

    @repetitionBtn().click (e)->
      e.preventDefault()

      index = $(this).data('index')
      time = new Date().getTime()
      htmlString = $(this).data('form')

      regex = new RegExp(index, 'g')
      htmlString = htmlString.replace(regex, time)

      html = self.htmlDecode htmlString
      $(this).parent().parent().parent().find('.repetitions').append(html)
      self.assignDestroyHandler()
      self.assignDates()

  assigneMaxGuestCountHandler: ->
    self = this

    @own_max_guest_count().each ->
      $(this).on 'change', ->
        self.maxGuestCount().find('.fbtn.active').removeClass('active')


    @maxGuestCount().find('.fbtn').each ->
      $(this).click (e)->
        self.own_max_guest_count().val('')

  own_max_guest_count: ->
    $('input[name="event[own_max_guest_count]"]')

  category: ->
    $('.event_category_id')

  repetitionBtn: ->
    $('.add-repetition')

  destroyButtons: ->
    $('.destroy')

  activeCategory: ->
    $(@category().find('input:checked')[0])

  sexDirection: ->
    $('.event_sex_direction')

  selfParticipation: ->
    $('.event_self_participation')

  activeSelfParticipation: ->
    $(@selfParticipation().find('input:checked')[0])

  activeSexDirection: ->
    $(@sexDirection().find('input:checked')[0])

  maxGuestCount: ->
    $('.event_max_guest_count')

  getOwnerGender: ->
    $('.event .eat').first().data('owner-gender')

  hideOddGuestCount: ->
    self = this
    @maxGuestCount().find('input[type="radio"]').each ->
      number = parseInt($(this).val())

      if (number % 2) == 0
        $(this).parent().parent().fadeOut(200)
      else
        $(this).parent().parent().fadeIn(200)

  hideEvenGuestCount: ->
    self = this

    @maxGuestCount().find('input[type="radio"]').each ->
      number = parseInt($(this).val())

      if (number % 2) == 1
        $(this).parent().parent().fadeOut(200)
      else
        $(this).parent().parent().fadeIn(200)

  showAllGuestCount: ->
    @maxGuestCount().find('input[type="radio"]').each ->
      unless $(this).parent().parent().is(':visible')
        $(this).parent().parent().fadeIn(200)

  htmlDecode: (value)->
    $('<div/>').html(value).text()

  isSelfParticipationActive: ->
    @activeSelfParticipation().val() == '1'

  isDatingDinerChecked: ->
    @activeCategory().val() == '2'

  isSexDirectionVisible: ->
    @sexDirection().is(':visible')

  isSelfParticipationVisible: ->
    @selfParticipation().is(':visible')

  showSexDirection: ->
    unless @isSexDirectionVisible()
      @sexDirection().fadeIn(200)

  hideSexDirection: ->
    if @isSexDirectionVisible()
      @sexDirection().fadeOut(200)

  showSelfParticipation: ->
    unless @isSelfParticipationVisible()
      @selfParticipation().fadeIn(200)

  hideSelfParticipation: ->
    if @isSelfParticipationVisible()
      @selfParticipation().fadeOut(200)

  deactivateSelfParticipation: ->
    @selfParticipation().find('input[value="0"]').attr('checked', true)
    @selfParticipation().find('input[value="0"]').parent().addClass('active')

  deactivateSelfParticipation: ->
    @selfParticipation().find('input[value="1"]').attr('checked', false)
    @selfParticipation().find('input[value="1"]').parent().removeClass('active')
    @selfParticipation().find('input[value="0"]').attr('checked', true)
    @selfParticipation().find('input[value="0"]').parent().addClass('active')

  activateSelfParticipation: ->
    @selfParticipation().find('input[value="1"]').attr('checked', false)
    @selfParticipation().find('input[value="1"]').parent().removeClass('active')
    @selfParticipation().find('input[value="0"]').attr('checked', true)
    @selfParticipation().find('input[value="0"]').parent().addClass('active')

  changeFormForSexDirection: (sexDirection)->
    if sexDirection == 'ff' && @getOwnerGender() == 'm'
      @deactivateSelfParticipation()
      @hideSelfParticipation()
    else if sexDirection == 'mm' && @getOwnerGender() == 'f'
      @deactivateSelfParticipation()
      @hideSelfParticipation()
    else
      if @isSexDirectionVisible()
        @showSelfParticipation()


  refreshForm: ->
    if @isDatingDinerChecked()
      @showSexDirection()

      if @isSelfParticipationActive()
        @hideOddGuestCount()
      else
        @hideEvenGuestCount()

      sexDirection = @activeSexDirection().val()
      @changeFormForSexDirection(sexDirection)
    else
      @hideSexDirection()
      @showAllGuestCount()
      @deactivateSelfParticipation()
      @hideSelfParticipation()

