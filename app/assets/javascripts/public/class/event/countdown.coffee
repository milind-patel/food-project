# namespace
window.Foodoo or= {}
window.Foodoo.Event or= {}

class Foodoo.Event.Countdown
  constructor: ->
    if @getMainContainer().length > 0
      @startInterval()

  showTimer: ->
    if @getTimeDifference() > 0
      days = @getTimeDifference() / (24 * 60 * 60)
      days = Math.floor(days)

      hours = (@getTimeDifference() / (60 * 60)) % 24
      hours = Math.floor(hours)

      if hours < 10
        hours = "0#{hours}"

      minutes = (@getTimeDifference() / 60) % 60
      minutes = Math.floor(minutes)

      if minutes < 10
        minutes = "0#{minutes}"

      seconds = @getTimeDifference() % 60
      seconds = Math.floor(seconds)

      if seconds < 10
        seconds = "0#{seconds}"

      text = "#{days} Tage #{hours} : #{minutes} : #{seconds}"
    else
      text = "0 Tage 00 : 00 : 00"

    @getMainContainer().text(text)

  getTimeDifference: ->
    (@getTimestamp() * 1000 - Date.now()) / 1000

  getTimestamp: ->
    @getMainContainer().data('time')

  getMainContainer: ->
    $('.public-eventappointments.show .timer-box .time')

  startInterval: ->
    self = @

    @inteval = window.setInterval ->
      self.showTimer()
    , 1000
