window.Foodoo or= {}

class Foodoo.ICheckRadio
  constructor: (@element)->
    @assignHandler()

  assignHandler: ->
    self = this
    $(@label_elements()).find('input').remove()

    $(@element).iCheck
      labelHover: false
      activeClass: 'active'
      cursor: true
      checkboxClass: 'icheckbox'
      radioClass: 'iradio'
      inheritClass: true
      insert: self.button_html()

    $(@element).on 'ifClicked', (event)->
      if !self.is_active()
        self.parents().removeClass('active')
        self.button().addClass('active')

  button: ->
    @_button or= $(@element).parent().find('button')

  parents: ->
    $(@element).parent().parent().parent().find('button')

  is_active: ->
    @button().hasClass('active')

  is_checked: ->
    $(@element).attr("checked") != "undefined" && $(@element).attr("checked") == "checked"

  label_elements: ->
    @_label_elements or= $(@element).parent().clone()

  button_html: ->
    '<button class="' + @css_classes() + '"><div class="left"></div><div class="content">' + @label_text() + '</div><div class="right"></div></button>'

  label_text: ->
    $(@label_elements()).html()

  css_classes: ->
    classes = new Array()

    if $(@element).data('btn-type') == undefined
      classes.push('fbtn')
      classes.push('fbtn-default')
    else
      classes.push($(@element).data('btn-type'))

    if @is_checked()
      classes.push('active')

    classes.join(' ')
