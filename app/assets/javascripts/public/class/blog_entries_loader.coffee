# namespace
window.Foodoo or= {}

# order form handler
class Foodoo.BlogEntriesLoader

  constructor: (mainContainer, @spinnerId)->
    @setLastLoadedPage(1)
    @setMainContainer($(mainContainer))

  loadNextPage: ->
    page = @getLastLoadedPage() + 1
    @loadPage(page)

  loadPage: (page)->
    @sendIndexPageRequest(page)

  sendIndexPageRequest: (page)->
    _this = @

    @showSpinner()
    @hideMoreButton()

    $.getJSON "#{@getReloadUrl()}?page=#{page}", null, (response) ->
      html = $(response['entries'])
      more_elements = response['more_entries']
      _this.hideSpinner()

      if more_elements == true
        _this.setLastLoadedPage(page)
        _this.showMoreButton()

      # add to dom
      _this.appendWithHtml(html)

  showSpinner: ->
    $(@getMoreButton()).before(
      "<div id='#{@getSpinnerId()}'></div>"
    )

    $("##{@getSpinnerId()}").spin(@getSpinnerOptions())

  hideSpinner: ->
    $("##{@getSpinnerId()}").remove()

  appendWithHtml: (html) ->
    # append
    @getMainContainer().append(html)

    # dotdotdot for the headline
    $('.blog-entry .teaser-text').dotdotdot(
      ellipsis: '... ',
      wrap: 'word',
      fallbackToLetter: true,
      after: null,
      watch: false
    )

  getMainContainer: ->
    @mainContainer

  setMainContainer: (container)->
    @mainContainer = container

  setSpinnerId: (spinnerId)->
    @spinnerId = spinnerId

  getSpinnerId: ->
    @spinnerId

  getLastLoadedPage: ->
    @lastLaodedPage

  setLastLoadedPage: (page)->
    @lastLaodedPage = page

  getSpinnerOptions: ->
    {
      lines: 9     # The number of lines to draw
      length: 15    # The length of each line
      width: 2      # The line thickness
      radius: 9    # The radius of the inner circle
      corners: 1    # Corner roundness (0..1)
      rotate: 0     # The rotation offset
      direction: 1  # 1: clockwise, -1: counterclockwise
      color: '#000' # #rgb or #rrggbb or array of colors
      speed: 1      # Rounds per second
      trail: 31     # Afterglow percentage
      shadow: true  # Whether to render a shadow
      hwaccel: false    # Whether to use hardware acceleration
      className: 'spinner'    # The CSS class to assign to the spinner
      zIndex: 2e9   # The z-index (defaults to 2000000000)
      top: '30px'   # Top position relative to parent in px
      left: 'auto'  # Left position relative to parent in px
    }

  getMoreButton: ->
    '.more_linking'

  getReloadUrl: ->
    @reloadUrl or= $(@getMoreButton()).data('url')

  hideMoreButton: ->
    $(@getMoreButton()).fadeOut(50)

  showMoreButton: ->
    $(@getMoreButton()).fadeIn(200)