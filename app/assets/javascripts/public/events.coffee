# ready function
$(document).on 'page:change', ->

  if $('body.public-eventappointments.show').length > 0
    gallery = new VerticleGallery(".event-box-detailed", "vertical")
    gallery.start()

  if $('body.internal-events.new, body.internal-events.edit').length > 0
    gallery = new VerticleGallery(".event-box-detailed", "vertical")
    gallery.start()

    new Foodoo.Upload ->
      gallery = new VerticleGallery("#gallery", "vertical")
      gallery.start()

      $('.internal-events.new #gallery').find('.btn.delete').click (e)->
        e.preventDefault()

        url = $(this).attr('href')
        image_container = $(this).parent()
        $.ajax url,
          type: 'DELETE'
          dataType:"json"
          contentType: 'application/json; charset=utf-8'
          success: (data, textStatus, jqXHR) ->
              image_container.fadeOut(400)
              large_image_url = image_container.find('img').data('url-large')
              main_image = $('.internal-events.new #gallery .main_image')
              if main_image.attr('src') == large_image_url
                main_image.attr('src', '')

              gallery = new VerticleGallery("#gallery", "vertical")
              gallery.start()

  new Foodoo.EventCreationForm('.internal-events form.standard-form')

  input_selector = $("input.price#input")
  output_selector = $("#output .wrapper > p")

  $('.internal-events.new #gallery').find('.btn.delete').click (e)->
    e.preventDefault()

    url = $(this).attr('href')
    image_container = $(this).parent()
    $.ajax url,
      type: 'DELETE'
      dataType:"json"
      contentType: 'application/json; charset=utf-8'
      success: (data, textStatus, jqXHR) ->
          image_container.fadeOut(400)
          large_image_url = image_container.find('img').data('url-large')
          main_image = $('.internal-events.new #gallery .main_image')

          gallery = new VerticleGallery("#gallery", "vertical")
          gallery.start()


  # --------------------------------------------
  # handler to manage description field
  # truncation in event ap. show
  new Foodoo.EventDescriptionHandler()


  # --------------------------------------------
  # Edit event
  if $('body.internal-eventappointments.edit, body.internal-eventappointments.update').length > 0

    bindImageDelete = ->
      $('#gallery').find('.btn.delete').click (e)->
        e.preventDefault()
        url = $(this).attr('href')
        image_container = $(this).parent()
        $.ajax url,
          type: 'DELETE'
          dataType:"json"
          contentType: 'application/json; charset=utf-8'
          success: (data, textStatus, jqXHR) ->
            image_container.fadeOut(400)
            large_image_url = image_container.find('img').data('url-large')
            main_image = $('.internal-events.new #gallery .main_image')
            if main_image.attr('src') == large_image_url
              main_image.attr('src', '')

            gallery = new VerticleGallery("#gallery", "vertical")
            gallery.start()

    bindGallery = ->
      gallery = new VerticleGallery("#gallery", "vertical")
      gallery.start()

    bindGallery()
    bindImageDelete()


    new Foodoo.Upload ->
      bindGallery()
      bindImageDelete()
