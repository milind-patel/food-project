# ready function
$(document).on 'page:change', ->

  $.fn.longclick = (callback, timeout) ->
    timer = undefined
    timeout = timeout or 100
    $(this).mousedown ->
      timer = setTimeout(->
        callback()
        return
      , timeout)
      false

    $(document).mouseup ->
      clearTimeout timer
      false

class window.Gallery
  constructor: (@gallery_id, @orientation) ->

    # time to invoke scroll methods at a longclick
    @long_click_repeat = 30
    # step to scroll at a long click
    @long_click_step = 5
    # step to scroll at a "normal click"
    @click_step = 25
    @timer_id = 0

    @arrow_hide(@arrow_next())

    if (@inside().height() - 5) <= @images_container().height()
      @arrow_hide(@arrow_previous())
      @arrow_hide(@arrow_next())

  images_container: ->
    $('.thumbnails')

  inside: ->
    @images_container().find('.inside')

  gallery: ->
    $(@gallery_id)

  main_image: ->
    $('.main_image')

  arrow_previous: ->
    @gallery().find('.prev')

  arrow_next: ->
    @gallery().find('.next')

  # fill in subclass
  previous: (offset, duration=200) ->

  # fill in subclass
  next: (offset, duration=200) ->

  abortTimer: ->
    clearInterval @timer_id

  arrow_show: (arrow) ->
    arrow.css(opacity:"1.0")
    arrow.css(cursor:"pointer")

  arrow_hide: (arrow) ->
    arrow.css(opacity:"0.2")
    arrow.css(cursor:"default")


  start: ->
    that = this
    # scroll up
    that.arrow_previous().mousedown (e) ->
      e.preventDefault()
      that.previous(that.click_step)

    that.arrow_previous().mouseup (e) ->
      e.preventDefault()
      that.abortTimer()

    that.arrow_previous().longclick ->
      that.timer_id = setInterval((->
        that.previous(that.long_click_step, that.long_click_repeat)
      ), that.long_click_repeat)

    # scroll down
    that.arrow_next().mousedown (e) ->
      e.preventDefault()
      that.next(that.click_step)

    that.arrow_next().mouseup (e) ->
      e.preventDefault()
      that.abortTimer()

    that.arrow_next().longclick ->
      that.timer_id = setInterval((->
        that.next(that.long_click_step, that.long_click_repeat))
      , that.long_click_repeat)

    # hover images
    @inside().find('img').hover ->
      image = $(that.main_image())
      data_url = $(this).data('url-large')
      image.attr "src", data_url
