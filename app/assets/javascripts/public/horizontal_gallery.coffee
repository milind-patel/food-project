class window.HorizontalGallery extends window.Gallery
  previous: (offset, duration=200) ->
    left = parseInt(@inside().css("left"))
    max_left_offset = @images_container().width() - @inside().width()

    if left >= max_left_offset
      if (left - offset) < max_left_offset
        @arrow_hide(@arrow_previous())
        @inside().stop().animate({left:"#{max_left_offset+1}px"}, duration)
      else
        @arrow_show(@arrow_next())
        @inside().stop().animate({left:"-=#{offset}"}, duration)

    else
      @arrow_hide(@arrow_next())
      @inside().stop().animate({left:"-1px"}, duration)

  next: (offset, duration=200) ->
    left = parseInt(@inside().css("left"))

    if left < -1
      @arrow_show(@arrow_previous())
      if (left + offset) >= -1
        @inside().stop().animate({left:"-1px"}, duration)
      else
        @inside().stop().animate({left:"+=#{offset}"}, duration)
    else
      @arrow_hide(@arrow_next())
      @inside().stop().animate({left:"-1px"}, duration)
