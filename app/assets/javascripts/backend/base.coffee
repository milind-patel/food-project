#
#= require turbolinks
#= require jquery
#= require jquery_ujs
#= require jquery-ui
#= require jquery.dotdotdot
#= require twitter/bootstrap
#= require tinymce-jquery
#= require bootstrap
#= require data-confirm-modal
#= require parameterize
#= require nprogress
#
#= require_self
#
#= require backend/blog
#

# nprogress hooks
$(document).on 'page:fetch', -> NProgress.start()
$(document).on 'page:change', -> NProgress.done()
$(document).on 'page:restore', -> NProgress.remove()

# ready function
$(document).on 'page:change', ->

  NProgress.done()

  $('form').on 'submit', (event) ->
    NProgress.start()

  dataConfirmModal.setDefaults
    title: 'Sind Sie sich wirklich sicher?',
    commit: 'löschen',
    cancel: "abbrechen"
