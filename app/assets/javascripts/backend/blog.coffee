# ready function
$(document).on 'page:change', ->

  dotdotdotForBlogTeaser = ->
    $('.blog-entry .teaser-text').dotdotdot(
      ellipsis: '... ',
      wrap: 'word',
      fallbackToLetter: true,
      after: null,
      watch: true
    )

  newContentToTeaser = (content) ->
    $('.blog-entry .text .teaser-text').html(content)
    dotdotdotForBlogTeaser()



  # ------------------------------------------
  if $('.backend-blogentries.new').length > 0 || $('.backend-blogentries.create').length > 0 ||
     $('.backend-blogentries.edit').length > 0 || $('.backend-blogentries.update').length > 0

    edit = if $('.backend-blogentries.new').length > 0 || $('.backend-blogentries.create').length > 0 then false else true
    dotdotdotForBlogTeaser()

    # init
    tinyMCE.init(
      selector: 'textarea[name="blog_entry[teaser_text]"]'
      language: 'de'
      skin: 'custom'
      convert_urls: false
      plugins: [
        "table link"
      ]
      extended_valid_elements: 'a[href|target=_blank]'
      menubar: false
      height: 120
      toolbar1: "removeformat | undo redo | link unlink image | bullist numlist outdent indent | blockquote | styleselect"
      toolbar2: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify "
      setup: (ed) ->
        ed.on( 'change', (e) ->
          newContentToTeaser(ed.getContent())
        )
        ed.on( 'keyup', (e) ->
          newContentToTeaser(ed.getContent())
        )
    )

    # events

    $('input[name="blog_entry[name]"]').on 'keyup', ->
      content = $(this).val()

      if edit == false
        $('input[name="blog_entry[alias]"]').val(URLify(content))
      $('.blog-entry .text .headline').html(content)

    $('textarea[name="blog_entry[teaser_text]"]').on 'keyup', ->
      content = $(this).val()
      newContentToTeaser(content)



  # ------------------------------------------
  if $('.backend-blogelements.new').length > 0 || $('.backend-blogelements.create').length > 0 ||
     $('.backend-blogelements.edit').length > 0 || $('.backend-blogelements.update').length > 0

    tinyMCE.init(
      selector: 'textarea[name="blog_element[text]"]'
      language: 'de'
      skin: 'custom'
      convert_urls: false
      plugins: [
        "table link"
      ]
      extended_valid_elements: 'a[href|target=_blank]'
      menubar: false
      height: 500
      toolbar1: "removeformat | undo redo | link unlink image | bullist numlist outdent indent | blockquote | styleselect"
      toolbar2: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify "
      setup: (ed) ->
        ed.on( 'change', (e) ->
          newContentToTeaser(ed.getContent())
        )
        ed.on( 'keyup', (e) ->
          newContentToTeaser(ed.getContent())
        )
    )




