class RatingNotificationCheckService

  def run
    distance = Settings.notfied_for_rating
    time = Time.now - distance.hours
    appointments = EventAppointment.where(notified_for_rating: 0)
                                   .where('start_time < ?', time)
    notify_appointments(appointments) if appointments.any?
  end

  private

  def notify_appointments(appointments)
    appointments.each do |appointment|
      appointment.participations.each do |participation|
        if participation.accepted?
          ParticipationMailingWorker.perform_async(participation.id, :rating)
        end
      end

      appointment.notified_for_rating += 1
      appointment.save(validate: false)
    end
  end

end