class NewsletterViewCheckService

  def initialize(user, newsletter_cookie_service)
    @user = user
    @newsletter_cookie_service = newsletter_cookie_service

    update_cookie_data
  end

  def cookie_service
    @newsletter_cookie_service
  end

  def show?
    return false if @user
    return false if cookie_service.active?
    true
  end


  private

  def update_cookie_data
    cookie_service.set_cookie(@user.email, @user.newsletter_subscription) if @user
  end

end