class CsvUserExportService

  def export
    to_csv(users)
  end

  private

  def users
    users = Invoice.all.map(&:user)

    users.uniq
  end

  def to_csv(users)
    CSV.generate(options) do |csv|
      csv << columns_titles

      users.each do |user|
        values = all_values(user)

        csv << values
      end
    end
  end

  def options
    {
      col_sep: "\t",
      quote_char: "'",
      force_quotes: true,
      skip_blanks: false
    }
  end

  def all_values(user)
    values = []
    payment_data = user.payment_data
    invoice_address = user.invoice_address

    user_columns.keys.each do |name|
      value = user.send(name)

      values << value || ''
    end

    address_columns.keys.each do |name|
      value = invoice_address && invoice_address.send(name)

      values << value || ''
    end

    payment_columns.keys.each do |name|
      value = payment_data && payment_data.send(name)

      values << value || ''
    end

    values
  end

  def columns_titles
    user_columns.values + address_columns.values + payment_columns.values
  end

  def columns
    user_columns.keys + address_columns.keys + payment_columns.keys
  end

  def user_columns
    {
      "email" => 'E-Mail',
      "phone" => 'Telefon',
    }
  end

  def address_columns
    {
      "addition" => 'Zusatz',
      "city" => 'Stadt',
      "country" => 'Land',
      "street" => 'Straße',
      "zip" => 'PLZ',
      "company" => 'Firma',
      "firstname" => 'Vorname',
      "lastname" => 'Nachname',
    }
  end

  def payment_columns
    {
      "owner" => 'Kontoinhaber',
      "bank" => 'Bank',
      "iban" => 'IBAN',
      "bic" => 'BIC',
    }
  end
end
