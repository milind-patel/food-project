class ViewConfig

  # plain attributes
  attr_accessor :controller_name, :action_name,
                :page_title, :page_meta_description,
                :page_meta_canonical,
                :page_meta_robots


  # returns classes string for body
  def body_classes
    "#{controller_name} #{action_name}"
  end

end