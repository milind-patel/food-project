class EventListViewArrayService

  def initialize(controller, event_appointments, page = 0, per = 0)
    @event_appointments = event_appointments
    @controller = controller
    @page = page
    @per = per
  end

  def build(with_newsletter = true)
    @view_array = []

    build_appointments
    inject_newsletter_rows if with_newsletter

    view_array
  end

  def page
    @page || 1
  end

  def per
    @per || @event_appointments.length
  end

  def view_array
    @view_array ||= []
  end

  def rows_before_newsletter
    Settings.view_events_newsletter_rows_offset
  end

  def request
    @controller.request
  end

  def config
    @controller.config
  end



  # ----------------------------------------
  # private

  private

  def build_appointments
    current_date = nil # headline detection
    row_elements = []

    @event_appointments.each_with_index do |appointment, index|
      # new headline?
      # if current_date != appointment.start_time.to_date
      #   view_array << generate_row(row_elements) if row_elements.any?
      #   view_array << generate_headline_row(appointment.start_time.to_date)
      #   current_date = appointment.start_time.to_date
      #   row_elements = []
      # end

      # insert element
      row_elements << appointment

      # insert row
      if row_elements.length >= 2 || index+1 >= @event_appointments.length
        view_array << generate_row(row_elements)
        row_elements = []
      end
    end
  end

  def inject_newsletter_rows
    newsletter_placed = false
    row_count = 0
    new_array = []

    view_array.each_with_index do |row|
      new_array << row
      row_count += 1 if row[:type] == 'events'
      if rows_before_newsletter == row_count && newsletter_placed == false
        new_array << generate_newsletter_row
        newsletter_placed = true
        row_count = 0
      end
    end

    new_array << generate_newsletter_row if newsletter_placed == false

    @view_array = new_array
  end


  # ----------------------------------------
  # generating methods (incl. rendering)

  def generate_headline_row(date)
    {
      type: 'headline',
      date: date.strftime('%Y-%m-%d'),
      html: ::Cell::Base.cell_for('index_events/headline', @controller, date).show
    }
  end

  def generate_row(row_elements)
    {
      type: 'events',
      html: ::Cell::Base.cell_for('index_events/events', @controller, row_elements).show
    }
  end

  def generate_newsletter_row
    {
      type: 'newsletter',
      html: ::Cell::Rails.render_cell_for('index_events/newsletter', 'show', @controller)
    }
  end

end