class LanguageService
  def load_and_create_languages
    file_path = Rails.root.join('lib', 'iso_languages.csv')

    if File.exists?(file_path)
      f = File.new(file_path)

      f.each_line do |content|
        language = create_language_hash_from_line(content)

        if language.present?
          ActiveRecord::Base.transaction do
            Language.find_or_create_by(language)
            puts "create language #{language[:title]}"
          end
        end
      end
    else
      raise "languages loading file in lib/iso_languages.csv not found."
    end
  end

  def create_language_hash_from_line(line)
    split = line.split(/[|]/)
    unless split[2].blank?
      {
        :iso_639_1 => split[2],
        :iso_639_2 => split[0],
        :iso_639_3 => split[1],
        :title     => split[3]
      }
    end
  end
end
