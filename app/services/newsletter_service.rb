class NewsletterService

  COOKIE_KEY = :foodoo_nelema

  def initialize(cookies, session, user = nil)
    @cookies = cookies
    @session = session
    @user = user

    subs = subscription
    cookify(subs) if subs && @cookies.present?
  end

  def subscription
    email = @cookies.signed[COOKIE_KEY]
    email ||= @session[COOKIE_KEY] if @session[COOKIE_KEY].present?
    email ||= @user.try(:email) if @user

    NewsletterSubscription.find_by(email: email) if email.present?
  end

  def cookify(subscription)
    cookies.permanent.signed[COOKIE_KEY] = subscription.email
    session[COOKIE_KEY] = subscription.email if session.present?
  end

end