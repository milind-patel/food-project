class NewsletterCookieService

  FOODOO_NEWSLETTER_COOKIE_KEY = :foodoo_newsletter_cookie

  def initialize(cookies)
    @cookies = cookies
    load_cookie
  end

  def cookies
    @cookies
  end

  def set_cookie(email, active)
    cookies.permanent.signed[FOODOO_NEWSLETTER_COOKIE_KEY] = {
        value: {
          email: email,
          active: active
        }.to_json,
        secure: Rails.env.production?
      }
  end

  def delete
    cookies.delete(FOODOO_NEWSLETTER_COOKIE_KEY)
  end

  def active?
    @data['active']
  rescue
    false
  end

  def email
    @data['email']
  rescue
    nil
  end

  private

  def load_cookie
    @data = JSON.parse(cookies.permanent.signed[FOODOO_NEWSLETTER_COOKIE_KEY])
  rescue
    @data = nil
  end

end