class SearchRequest
  include ActiveAttr::Model

  attribute :city
  attribute :category
  attribute :date

  def initialize(city, category, date)
    self.city = city
    self.category = category

    if date.present?
      date = Time.parse(date)
    end

    self.date = check_date(date)
    @page = 1
    @per = Settings.view_events_per_page
  end

  def page(page)
    @page = page if page.to_i > 0
  end

  def per(per)
    @per = per if per.to_i > 0
  end

  def filtered_appointments
    event_appointments = EventAppointment.approved.type_ordered.uncanceled
    event_appointments = filter_category(event_appointments)
    event_appointments = filter_city(event_appointments)
    event_appointments = paginate(event_appointments)
    event_appointments = event_appointments.includes(:event)
    event_appointments = event_appointments.includes(:participations)

    @more_appointments = @page.to_i < event_appointments.total_pages

    event_appointments
  end

  def more_appointments?
    @more_appointments
  end

  private

  def check_date(date)
    d = date.nil? ? Time.zone.now : date.to_time
    d.beginning_of_day
  end

  def filter_category(event_appointments)
    category = Category.find_by_name(self.category)

    category ? event_appointments.with_categories([category]) : event_appointments
  end

  def filter_city(event_appointments)
    if self.city.present?
      event_appointments.in_city(self.city.humanize)
    else
      event_appointments
    end
  end

  def paginate(event_appointments)
    event_appointments.page(@page).per(@per)
  end
end
