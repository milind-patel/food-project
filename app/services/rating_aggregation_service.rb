class RatingAggregationService

  def self.aggregate_all
    User.includes(events: [event_appointments: [:event_ratings]]).load.each{ |u| u.reaggregate_ratings }
  end

  def self.max_rating_value
    5
  end

  # ------------------------------------------

  def initialize(user)
    @user = user
  end

  def run
    return if @user.nil?
    return if @user.events.empty?
    aggreagate_with_events(@user.events)
    rating
  end

  def max_rating_value
    RatingAggregationService.max_rating_value
  end

  def rating
    rating_from_aggregations
  end

  # ------------------------------------------

  private

  def aggreagate_with_events(events)
    ratings = ratings_for_events(events)
    @ratings_count = ratings.count
    ratings.each do |rating|
      add_rating(rating.total.to_i, :total)
      add_rating(rating.meal.to_i, :meal)
      add_rating(rating.atmosphere.to_i, :atmosphere)
      add_rating(rating.location.to_i, :location)
      add_rating(rating.price_performance_ratio.to_i, :price_performance_ratio)
    end
  end

  def aggregation
    @aggregation ||= {
      total: OpenStruct.new(value: 0, count: 0),
      meal: OpenStruct.new(value: 0, count: 0),
      atmosphere: OpenStruct.new(value: 0, count: 0),
      location: OpenStruct.new(value: 0, count: 0),
      price_performance_ratio: OpenStruct.new(value: 0, count: 0),
      count: 0,
    }
  end

  def ratings_count
    @ratings_count ||= 0
  end

  def add_rating(value, identifier)
    return if value <= 0 || value > self.max_rating_value
    aggregation[identifier].count += 1
    aggregation[identifier].value += value
  end


  def rating_from_aggregations
    aggr = aggregation.inject({}) do |r,(k,v)|
      r[k] = rating_from_aggregation(v.value, v.count) if k != :count
      r
    end
    aggr[:count] = ratings_count
    aggr
  end

  def rating_from_aggregation(value, count)
    result = ((value.to_d / count) / self.max_rating_value).to_f
    result.nan? ? 0 : result
  end


  def ratings_for_events(events)
    events.inject([]) { |r,event| r += ratings_for_event(event); r }
  end

  def ratings_for_event(event)
    return [] if event.event_appointments.empty?
    event.event_appointments.inject([]) { |r,appointment| r += ratings_for_appointment(appointment); r }
  end

  def ratings_for_appointment(appointment)
    appointment.event_ratings
  end

end