class MailChimpConnectorService
  def synchronize
    create_not_exists_groups
    sync_unsubscriptions
    sync_subscriptions

    NewsletterLog.create
  end

  def sync_subscriptions
    subscriptions = NewsletterSubscription.where('updated_at > ? AND active = ?', last_sync, true)

    if subscriptions.present?
      api.lists.batch_subscribe(
        {
          id: list_id,
          batch: create_subscription_batch(subscriptions),
          double_optin: false,
          update_existing: true,
        }
      )
    end
  end

  def sync_unsubscriptions
    unsubscriptions = NewsletterSubscription.where('updated_at > ? AND active = ?', last_sync, false)

    if unsubscriptions.present?
      api.lists.batch_unsubscribe(
        {
          id: list_id,
          batch: create_unsubscription_batch(unsubscriptions),
          delete_member: true,
        }
      )
    end
  end

  def create_subscription_batch(subscriptions)
    subscriptions.inject([]) do |result, subscription|
      result << {
        email: { email: subscription.email },
        :merge_vars => {
          :FNAME => subscription.name,
          :groupings => [{
            name: NewsletterSubscription.humanized_group_name_city,
            groups: [subscription.city_name]
          },
          {
            name: NewsletterSubscription.humanized_group_name_registered,
            groups: [subscription.registered? ? 'YES' : 'NO']
          }]
        }
      }
    end
  end

  def create_unsubscription_batch(subscriptions)
    subscriptions.inject([]) do |result, subscription|
      result << {
        email: subscription.email,
      }
    end
  end

  def last_sync
    subscription = NewsletterLog.order('created_at').last

    if subscription.present?
      subscription.created_at
    else
      Time.now - 100.years #wtf
    end
  end

  def create_not_exists_groups
    names = available_group_names

    needed_names.each do |name|
      unless names.include?(name)
        create_group(name)
      end
    end
  end

  def create_group(name)
    options = group_options(name)

    api.lists.interest_grouping_add({
      id: list_id,
      name: name,
      type: 'hidden',
      groups: options
    })
  end

  def needed_names
    needed_names = [
      NewsletterSubscription.humanized_group_name_city,
      NewsletterSubscription.humanized_group_name_registered,
    ]

    needed_names.map(&:humanize)
  end

  def list_id
    Settings.mail_chimp_list_id
  end

  def api
    @api ||= Gibbon::API.new(Settings.mail_chimp_api_key)
  end

  private

  def group_options(name)
    @group_options ||= begin
      options = {}

      options[NewsletterSubscription.humanized_group_name_city] = City.all_names

      options[NewsletterSubscription.humanized_group_name_registered] = [
        'YES',
        'NO',
      ]

      options
    end

    @group_options[name.to_s]
  end

  def available_group_names
    response = api.lists.interest_groupings({:id => list_id})

    names = response.map do |hash|
      hash['name']
    end

    names || nil
  rescue Gibbon::MailChimpError
    []
  end
end
