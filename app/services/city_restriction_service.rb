class CityRestrictionService

  def address_accessable?(address)
    results = Geocoder.search(address)
    results.present?
  end

end
