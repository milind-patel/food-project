class EventAppointmentSerializer < ActiveModel::Serializer
  attributes :id, :date, :headline

  def date
    object.start_time.to_i
  end

  def headline
    @options['headline']
  end

  # def view
  #   raise self.inspect
  #   #render html: "<h1>TEST</h1>".html_safe
  #   ApplicationController.new.render_to_string(
  #     partial: 'public/event_appointments/index_event',
  #     locals: {
  #       event_appointment: object
  #     }
  #   )
  # end
end
