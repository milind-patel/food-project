class TimePickerInput < SimpleForm::Inputs::StringInput
  def input_html_options
    value = object.send(attribute_name) if object.present?
    value = value.strftime("%H:%M") if value.present?
    options = {
      data: { behaviour: 'timepicker' },  # for example
      class: 'form-control small inline',
      value: value,
    }

    # add all html option you need...
    super.merge options
  end
end
