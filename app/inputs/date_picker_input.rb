class DatePickerInput < SimpleForm::Inputs::StringInput
  def input_html_options
    value = object.send(attribute_name) if object.present?
    value = value.strftime("%d.%m.%Y") if value.present?
    options = {
      data: { behaviour: 'datepicker' },  # for example
      class: 'form-control small inline',
      value: value,
    }

    # add all html option you need...
    super.merge options
  end
end
